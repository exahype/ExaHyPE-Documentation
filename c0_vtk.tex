\chapter{ExaHyPE Output File format specifications}

This chapter contains a technical specification for the file formats
described in section \ref{sec:plotting-supported-output-formats}. These
are sophisticated complex file formats to hold the ExaHyPE cells/patches
and their subcell structure which is either composed by the ADERDG 
Legendre subgrid or the Finite Volume Cartesian subgrid. See figure
\ref{fig:fileformats-overview-discussion} for a quick overview of the
available file formats.

\begin{figure}
\makebox[\textwidth][c]{% % for overwidth
%\vspace{-1cm}%
% trim={<left> <lower> <right> <upper>}
\includegraphics[trim={1cm 3cm 1cm 1cm},clip,width=1.1\textwidth]{sketches/File-Formats-Sketch.pdf}
}% makebox
\caption[Infographic (Openoffice): file format overview]{A sketch on ExaHyPE volumetric file formats, their advantages and disadvantages as well as their internal structure}%
\label{fig:fileformats-overview-discussion}
\end{figure}

\section{ExaHyPEs VTK Unstructured Mesh output}
\label{section:VTK-appendix}

Our VTK support sticks to the VTK legacy format. 
This section is not supposed to replace the VTK file format description as it
can be found in The VTK User's Guide available from Kitware free of charge.
An excerpt of the relevant pages can be found at
\url{http://www.vtk.org/wp-content/uploads/2015/04/file-formats.pdf}, e.g.
This section complements the technical aspects with a user's point of view. 

\begin{itemize}
  \item {\bf File format.} As VTK has no real support for block-structured
  adaptive meshes, \exahype\ writes out all data as an unstructured mesh: the
  output file describes a graph. The graph is specified via three sections.
  \item {\bf Section 1: Point coordinates.} In the first section of the VTK
  file, all points used by the mesh are specified. There is no particular on these
  points. Please see the introductory remarks in Chapter \ref{chapter:plotting}
  on DG polynomials that clarify why some points can be found redundantly in the
  file. The only way the order plays a role is through the fact that it imposes
  an enumeration of the vertices starting with 0.
  \item {\bf Section 2: Cells.} In a second part of the file, we specify all
  cells. Again, there is no particular order but the sequence in which the cells
  are enlisted defines an order. Each line in the file specifies on cell. VTK
  requires each cell definition to start with the number of vertices used to
  span the cell. As \exahype\ restricts to block-Cartesian grids, this initial
  number always is either 4 or 8.
  \item {\bf Section 3: Cell types.} The VTK file format requires a block where
  one enlists the cell type per cell entry. As we stick to cubes/squares in
  \exahype, this section contains the same identifier (number) per cell and can
  be skipped if you want to postprocess the data yourself.
  \item {\bf Point time step data.} After the cell types, you find the time
  steps of the point data. Per vertex, we track at which time stamp its data is
  written if your plotter has to write data. Otherwise, the section is omitted. 
  Per vertex, we write one scalar.
  For global time stepping, all values are equal. For adaptive time stepping,
  they might differ as the data gives the real time stamp of the data, not the
  simulation time when the data has been written.
  \item {\bf Point data.} After the time stamp, you find per vertex one vector
  holding all the vertex data.
  \item {\bf Cell time step data.} Analogous to vertices.
  \item {\bf Cell data.} Analogous to vertices.
\end{itemize}

\noindent
If you have chosen a VTK binary format, all the section identifiers are
still written in plain text, i.e.~you can continue to open the output files as text
files. The real data, i.e.~the coordinates, the indices spanning a cell, the
unknowns the are written binary and this binary stream is embedded into the text
file.

Here we follow the display of cell and point data as defined in the VTK file format.

\subsection{VTK cell data}
Table \ref{vtkcell} describes the data present in a VTK file which
was created with the \texttt{vtk::*::cells::*} stanza. A note on
the ordering of the data rows: There is no obvious order one should assume.
Instead, it is helpful to sort the data on own criteria after reading in.

\begin{table}[h]
\footnotesize{
\begin{minipage}{.5\linewidth}
\begin{tabular}{l r r r r}
    \toprule
    \multirow{2}{*}{\bfseries Point ID} & 
%    \multirow{2}{*}{\bfseries Characteristics} & 
    \multicolumn{3}{c}{\bfseries Points} &
    \multirow{2}{*}{\bfseries Time}    
    \\ \cmidrule(lr){2-4}
    & x&y&z &
    \\ \cmidrule(lr){1-5}
    %------
    0 & 0.00 & 0.00 & 0.00 & 0.01 \\
    1 & 0.01 & 0.00 & 0.00 & 0.01 \\
    2 & 0.01 & 0.01 & 0.00 & 0.01 \\
    \ldots \\
    \bottomrule
\end{tabular}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\begin{tabular}{l l r r r}
    \toprule
    \multirow{2}{*}{\bfseries Cell ID} & 
    \multirow{2}{*}{\bfseries Cell Type} & 
    \multicolumn{3}{c}{\bfseries Q}
    \\ \cmidrule(lr){3-5}
    && $Q_0$&$Q_1$ & \ldots
    \\ \cmidrule(lr){1-5}
    %------
    0 & Pixel & 1.23 & 4.56 & \ldots \\
    1 & Pixel & 3.41 & 0.07 & \ldots \\
    2 & Pixel & 0.81 & 5.47 & \ldots \\
    \ldots \\
    \bottomrule
\end{tabular}
\end{minipage}
\caption{The point and cell data tables in ExaHyPE's VTK cell data format}\label{vtkcell}
}%footnotesize
\end{table}


\subsection{VTK point data}
Table \ref{vtkpoint} describes the data present in a VTK file which
was created with the \texttt{vtk::*::vertices::*} stanza. A note on
the ordering of the data rows: There is no obvious order one should assume.
Instead, it is helpful to sort the data on own criteria after reading in.

\begin{table}[h]
\footnotesize{
\begin{minipage}{.7\linewidth}
\begin{tabular}{l r r r r r r r}
    \toprule
    \multirow{2}{*}{\bfseries Point ID} & 
%    \multirow{2}{*}{\bfseries Characteristics} & 
    \multicolumn{3}{c}{\bfseries Points} &
    \multirow{2}{*}{\bfseries Time} &   
    \multicolumn{3}{c}{\bfseries Q} 
    \\ \cmidrule(lr){2-4}\cmidrule(lr){6-8}
    & x&y&z && $Q_0$ & $Q_1$ & \ldots
    \\ \cmidrule(lr){1-8}
    %------
    0 & 0.00 & 0.00 & 0.00 & 0.01 & 1.23 & 4.56 & \ldots \\
    1 & 0.01 & 0.00 & 0.00 & 0.01 & 3.14 & 0.07 & \ldots \\
    2 & 0.01 & 0.01 & 0.00 & 0.01 & 0.81 & 5.47 & \ldots \\
    \ldots \\
    \bottomrule
\end{tabular}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\begin{tabular}{l l}
    \toprule
    {\bfseries Cell ID} & 
    {\bfseries Cell Type}
    \\ \cmidrule(lr){1-2}
    %------
    0 & Pixel \\
    1 & Pixel \\
    2 & Pixel \\
    \ldots \\
    \bottomrule
\end{tabular}
\end{minipage}
\caption{The point and cell data tables in ExaHyPE's VTK point data format}\label{vtkpoint}
}% footnotesize
\end{table}


\section{HDF5 in ExaHyPE}

\subsection{Enable HDF5}\label{sec:enable-hdf5}
All proposed tailored volumetric output formats have the option or require the
HDF5 container format to properly work with. To do so, they rely on the HDF5
bindings.

If you want \exahype\ to run with HDF5, you have to compile with
\texttt{-DHDF5}. 
The simplest way to achieve this is to overload the environment variable
\texttt{EXAHYPE\_CC}:
\begin{code}
export EXAHYPE_CC="mpiCC -DHDF5"
\end{code}
Furthermore, you have to link against both the \texttt{hdf} library and
\texttt{hdf5\_cpp}:
\begin{code}
export EXAHYPE_CC="mpiCC -DHDF5 -lhdf5 -lhdf5_cpp"
\end{code}

\subsubsection{Warn or crash if plotter fails}
If you enable a plotter which
requires HDF5 during runtime on an ExaHyPE build without HDF5, the plotter will
not do anything but print warnings at every plotter step. This allows you to
get started or continue working with ExaHyPE without worrying about dependencies.

If you want to have \exahype\ crash in case of missing HDF5 support, for instance
when you run a large job and cannot afford a accidental wrong compile setting which
would result in wasting large amounts of CPU hours, you can set the environmental
variable \texttt{EXAHYPE\_STRICT} before invoking ExaHyPE built in with these
plotters, i.e.
\begin{code}
export EXAHYPE_STRICT="True"
\end{code}
