#!/bin/bash
# SvenK, 2018-05-11

## Compose together a latex report by the edits done by the people.

cd "$(dirname $0)"

## Get new numbers, if wanted:

# bash make-gitfame-csv.sh
# bash make-gitquick-csv.sh

## Join them

texfile="../x1_contributions.tex"
echo "Writing Latex to $texfile"

exec > $texfile

echo "Detailed report variant 1:"

python gitquick-to-latex.py

echo 
echo "Detailed report variant 2: coming"
