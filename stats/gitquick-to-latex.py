#!/usr/bin/python
# SvenK, 2018-05-11

from __future__ import print_function
import pandas as pd
import io # character safe

table = pd.read_csv("stats-gitquick.txt", sep="\t")

# cleanup authors trailing ":"
table["Author"] = table["Author"].apply(lambda x: x.replace(":",""))

# import dates as DateTimes
timelike_cols = ['first', 'last']
for timelike_col in timelike_cols:
	table[timelike_col] = pd.to_datetime(table[timelike_col])

# set the global first and last edit:
total_row = table["Author"] == "total"
table.loc[total_row , "first"] = table['first'].min()
table.loc[total_row , "last"]  = table['last'].max()

# correct sum
lines_changed = "lines changed"
table.loc[total_row, lines_changed] = table[lines_changed].sum()

# correct type
table[lines_changed] = table[lines_changed].astype(int)

# Correct sorting:
table = table.sort_values(by=lines_changed, ascending=False)

# Resolution of dates
for timelike_col in ['first', 'last']:
	table[timelike_col] = pd.to_datetime(table[timelike_col]).dt.strftime('%Y-%m-%d')

# print(table.to_latex())

with io.open("stats-gitquick.tex",'w',encoding='utf8') as f:
    f.write(table.to_latex(index=False)) # index: do not show the row numbers


