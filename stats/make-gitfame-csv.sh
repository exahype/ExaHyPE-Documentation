#!/bin/bash
# SvenK, 2018-05-11

# Purpose: Run another kind of statistics on the ExaHyPE documentation git repository

cd "$(dirname $0)"

# pip install gitfame
(cd .. && python -m gitfame ) > stats-gitfame.txt

# known issues:
#  1. Output is not parsable CSV but a ASCII art table
#  2. non-ascii characters are corrupted (at least nicely to '?', i.e. no binary garbarge)


