#!/bin/bash

# Taken from https://github.com/arzzen/git-quick-stats,
# the most usable output

# Does not need git-quick-stats installed, I just copied the
# standalone code from that bash code. It is pure awk.

# Executed in a git repository, this will print out a CSV
# table, seperated by tabs

# SvenK, 2018-05-11

exec &> >(tee stats-gitquick.txt)

cd "$(dirname $0)"
cd ..

echo -e "Author\tinsertions\tdeletions\tfiles\tcommits\tlines changed\tfirst\tlast"

# Keep author mail addresses: Author: %aN <%aE>
# Only names:                 Author: %aN

 git log --use-mailmap --no-merges --numstat --pretty="format:commit %H%nAuthor: %aN%nDate:   %ad%n%n%w(0,4,4)%B%n"  | LC_ALL=C awk '
    function printStats(author) {
      printf "%s:", author
      if( more["total"] > 0 ) {
        printf "\t%d", more[author]
      }
      if( less["total"] > 0 ) {
        printf "\t%d", less[author]
      }
      if( file["total"] > 0 ) {
        printf "\t%d", file[author]
      }
      if(commits["total"] > 0) {
      	printf "\t%d", commits[author]
      }
      if ( first[author] != "" ) {
        printf "\t%s", more[author] + less[author]
        printf "\t%s", first[author]
        printf "\t%s", last[author]
      }
      printf "\n"
    }
    /^Author:/ {
      author = substr($0, 9)
      commits[author] += 1
      commits["total"] += 1
    }
    /^Date:/ {
      $1="";
      first[author] = substr($0, 2)
      if(last[author] == "" ) { last[author] = first[author] }
    }
    /^[0-9]/ {
      more[author] += $1
      less[author] += $2
      file[author] += 1
      more["total"]  += $1
      less["total"]  += $2
      file["total"]  += 1
    }
    END {
      for (author in commits) {
        if (author != "total") {
          printStats(author)
        }
      }
      printStats("total")
    }'
