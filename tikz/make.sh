#!/bin/sh
#
# Make all standalone tikz figures
#

latex=pdflatex

for file in *.tex; do
	$latex ${file}
	$latex ${file}
done

rm -f *.aux *.log
