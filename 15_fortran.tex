\chapter{\exahype\ FORTRAN}
\label{section:fortran}

\exahype\ is a pure C++ application. However, it allows developers a
seamless integration of FORTRAN code. There are two places where
FORTRAN code may be used:

\begin{itemize}
\item Everywhere inside the users solver. Modern compilers support
   binding FORTRAN functions to C and thus passing actually \emph{all}
   PDE signatures, definition of boundary values, initial data, etc.
   to FORTRAN code managed by the user.

\item The 3D ADERDG FORTRAN kernels. They descend from the original
   ADER-DG formulation by M. Dumbser and may be used by user applications.
   They are identified with the token
   \texttt{kernels::aderdg::generic::fortran::3d} in contrast to the
   generic C kernels. However, there is in principal no advantage in
   using the fortran kernels over the C kernels.
   In any case, when using the Fortran kernels, applications \emph{must}
   implement their PDE functions in FORTRAN.
\end{itemize}

While the use of FORTRAN in ExaHyPEs kernels is more historical than
officially supported, users are encouraged to extensively use FORTRAN
in their application if needed. The ExaHyPE Makesystem detects,
compiles and links FORTRAN modules and files (\texttt{.f90} files).

\section{An example FORTRAN binding}

When passing arguments to FORTRAN, we stick here to the convenience
rules to
\begin{itemize}
\item pass all parameters by reference (ie. with pointers)
\item call Fortran functions named \texttt{FortranFunction} from C
as \texttt{fortranfunction\_}, ie. lowercased and with a trailing
underscore.
\end{itemize}

First, we present some signatures how to pass variables for an exemplary
\texttt{Demo} project with solver named \texttt{FortranSolver} to
FORTRAN:

\begin{code}
void Demo::FortranSolver::flux(const double* const Q, double** F) {
  pdeflux_(F[0], F[1], (DIMENSIONS==3) ? F[2] : nullptr, Q);
}

void Demo::FortranSolver::eigenvalues(const double* const Q,
    const int normalNonZeroIndex, double* lambda) {
  double nv[3] = {0.};
  nv[normalNonZeroIndex] = 1;
  pdeeigenvalues_(lambda, Q, nv);
}

void Demo::FortranSolver::adjustedSolutionValues(const double* const x,
    const double w,const double t,
    const double dt,double* Q) {
  adjustedsolutionvalues_(x, &w, &t, &dt, Q);
}

void Demo::FortranSolver::source(const double* const Q, double* S) {
  pdesource_(S, Q);
}

void Demo::FortranSolver::ncp(const double* const Q,
     const double* const gradQ, double* BgradQ) {
  pdencp_(BgradQ, Q, gradQ);
}

void Demo::FortranSolver::matrixb(const double* const Q,
    const int normalNonZero, double* Bn) {
  pdematrixb_(Bn, Q, nv);
}
\end{code}
%
It is evident how simple these function calls are. For the C++ side,
one might define the signatures now in an appropriate header file as
\begin{code}
extern "C" {
void adjustedsolutionvalues_(const double* const x,const double* w,
  const double* t,const double* dt,double* Q);
void pdeflux_(double* Fx, double* Fy, double* Fz, const double* const Q);
void pdesource_(double* S, const double* const Q);
void pdeeigenvalues_(double* lambda, const double* const Q, double* nv);
void pdencp_(double* BgradQ, const double* const Q, const double* const gradQ);
void pdematrixb_(double* Bn, const double* const Q, double* nv);
}/* extern "C" */
\end{code}

\begin{table}[h!]
\footnotesize{
\centering
\begin{tabular}{lll} \toprule
  \multicolumn{1}{c}{\textbf{Quantity}} & 
  \multicolumn{1}{c}{\textbf{Size}} &
  \multicolumn{1}{c}{\textbf{Meaning}}
  \\
\midrule \midrule
x  & DOUBLE(nDim) & spatial positions \\
Q  & DOUBLE(nVar) & Unknowns/Degrees of Freedom \\
Fi & DOUBLE(nVar) & Flux for each unkown in direction $i$ \\
S  & DOUBLE(nVar) & Source term for each unknown \\
lambda  & DOUBLE(nVar) & Eigenvalue for each unknown \\
nv  & DOUBLE(nDim) & Normal vector for computation \\
BgradQ  & DOUBLE(nVar) & Vector $(B^k\otimes(\nabla Q)_k)_i$ \\
gradQ    & DOUBLE(nVar, nDim)  & Matrix $(\nabla Q_i)_j$ \\
\bottomrule
\end{tabular}%
\caption{Parameters for the PDE solvers, in Fortran notation, cf. also appx.~\ref{section:datastructures}.}\label{table:fortran}
}%footnotesize
\end{table}
%
\section{The Fortran code}

In Fortran, one might now implement the functions anywhere, for instance
with
%
\begin{code}
SUBROUTINE PDEEigenvalues(Lambda,Q,nv) 
  USE Parameters, ONLY : nVar, nDim
  USE, INTRINSIC :: ISO_C_BINDING 
  IMPLICIT NONE 
  ! Argument list  
  REAL, INTENT(IN)  :: Q(nVar), nv(nDim)  
  REAL, INTENT(OUT) :: Lambda(nVar)  
  ! Local variables  
  REAL :: foo(nVar), bar
  
  Lambda = 1
END SUBROUTINE PDEEigenvalues
\end{code}
%
Two comments about this code: First, we specify the size of the individual
parameter arrays, in contrast to C. Table \ref{table:fortran} gives
an overview about the actual extends. Second, we make use of
a module \texttt{Parameters} which is introduced for convenience
in order to store the information about the number of variables in
the PDE system and the number of dimensions. There is no connection
at all to ExaHyPE, it's in the users duty to maintain this code:
\begin{code}
MODULE Parameters 
    IMPLICIT NONE  
    PUBLIC  

    INTEGER, PARAMETER             :: nDim = 2
    INTEGER, PARAMETER             :: nVar = 9    
    ! etc.
END MODULE Parameters
\end{code}
%

\section{Known limitations}
\begin{itemize}
\item
Note that \exahype\ currently provides no further support for Fortran
code generation in terms of providing consistency between the Fortran
\texttt{nDim}, \texttt{nVar} as introduced here and the C++ counterparts
(which can actually be found in the \texttt{Abstract*Solver.h} header).

\item
ExaHyPE's build system is quite rudimentary: It basically slurps all
C++ and f90 files it can find, compiles them in no well defined order
and links everything together. However, especially Fortran requires
modules to be compiled before files depending on them. Thus you might
have to invoke \texttt{make} several times if you run into this problem.
You also might want to replace the Makesystem of your application with
an more advanced one.

\item
In any case, we can compile Fortran code with both Intel and GCC compilers
within ExaHyPEs build system.

\end{itemize}

\section{Further reading}

\begin{itemize}
\item \url{https://computing.llnl.gov/tutorials/bgq/mixedProgramming1.pdf}
\end{itemize}