#!/bin/bash

# This script allows uploading the generated pdf to a FTP site.
# However, the FTP credentials (user:password) are note stored
# in this script but obtained from a remote server by passing
# a secret. Thus we keep more valuable secrets out of the (at one
# time public) repository. We can also control the upload target,
# etc. much more easily with this delegation. However, it is a
# hacky solution and also allows the "secret agent" to control
# the executing computer on a shell basis. Thus... use it with care.
#
# 2017, svenk.
#

asset="guidebook.pdf"
server="http://dev.exahype.eu"

secret_agent="$server/getsecrets.php?JrdWgLt7qVXDho1niMg0TyqK1PIlfs6cnlYXPRqz"

set -e

if [[ -e $asset ]]; then
	echo -e "Obtaining upload token"
	upload_token="$(curl -f $secret_agent)"

	echo "Uploading $asset to $server/$asset"

	# using $upload_token opens the door for shell exploiting
	# the executing system. "--proto ftp" tries to restrict this
	# but the correct solution would be to avoid bash at all
	# and use execl(3), ie. with pythons subprocess.
	exec curl -f -T $asset --proto ftp $upload_token/$asset
else
	echo "Could not find $asset, do not upload."
fi
