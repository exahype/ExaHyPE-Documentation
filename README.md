ExaHyPE Guidebook
================

The ExaHyPE guidebook is a documentation about the ExaHyPE project. It
is written in LaTeX and currently measures about 60 pages. The guidebook
is intended to be read by users who want to implement their own PDEs
in ExaHyPE or who want to understand and use ExaHyPE example or
demonstrator applications to do computational physics.

Since December 2015 the guidebook resides in it's own git repository.
The repository does not contain the compiled PDF. Instead, it is built
by our gitlab installation https://gitlab.lrz.de/exahype/ExaHyPE-Documentation/
with a continous integration service and published at
http://dev.exahype.eu/guidebook.pdf

In the past, the guidebook was built nightly and released at
http://www5.in.tum.de/exahype/guidebook.pdf . Apparently, this service
stopped in December 2016.


Compiling the guidebook
-----------------------

The Guidebook should be able to compile with any decent standard LaTeX
distribution, for instance TexLive (PDFLatex). To do so, just type
`pdflatex codebook.tex` or just use `make`.

Optional LaTeX-packages
-----------------------

The guidebook can make use of certain LaTeX packages *if* they are
installed. Otherwise, it gracefully ignores these options. The following
*upgrades* are turned on as soon as the packages are installed:

* Nicer fonts: Inconsolata for monospace, TGPanella for the main text.
  These fonts are available as LateX packages, ie in the Ubuntu package
  `texlive-fonts-extra`.

* Nicer code listings using the *minted* package. To install minted, you
  need Pygments (available via `pip install Pygments` or Ubuntu package
  `python-pygments`) and the `minted` package, available with the Ubuntu
  package `texlive-latex-extra`.

