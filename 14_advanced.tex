\chapter{Some advanced solver features}\label{sec:advanced-solver-features}

\section{Multiple solvers in one specification file}

\exahype\ specification files can host multiple solvers. In this case, multiple
solvers are simultanously hosted within one compute grid. This feature can be
advantageous for parameter studies, e.g., as all grid managements,
parallelisation, load balancing, \ldots overhead is amortised between the
various solvers ran simultaneously.


\section{Runtime constants/configuration parameters}

There are various ways to add application-specific constants to your code. 
While it is reasonable to extend/tailor the code to make it accept additional
runtime parameters on the command line, e.g., our original design philosophy was
to have everything in one file.
This means, that many application need application-specific settings in this
file.
Therefore, \exahype\ allows you to add
a \texttt{constants} section to your solver:

\begin{code}
  solver ADER-DG MyEulerSolverWithConstantsFromSpecFile
    variables const    = rho:1,j:3,E:1
    order              = 3
    maximum-mesh-size  = 0.5
    time-stepping      = global
    type const         = nonlinear
    terms const        = flux
    optimisation const = generic
    language const     = C
    constants          = rho:0.4567,gamma:-4,alpha:4.04e-5,file:output
  end solver
\end{code}


\noindent
If you rerun the toolkit now, you'll get a modified constructor that accepts a
\linebreak 
\texttt{ParserView} object which you can query for the constants.
\begin{code}
Euler::MyEulerSolverWithConstantsFromSpecFile::
  MyEulerSolverWithConstantsFromSpecFile(
  ...,
  exahype::Parser::ParserView constants):
  exahype::solvers::ADERDGSolver(...) {
  if (constants.isValueValidDouble( "rho" )) {
    double rho = constants.getValueAsDouble( "rho" );
    // do some magic with rho
  }
}
\end{code}


\begin{design}
\exahype\ is an engine that focuses on simplicity, capability and speed. 
We do not support the reuse of one solver type with different constants in one
file or the reuse of a solver between various projects (though both features
should be straightforward to implement within an application). However, you can create multiple specification files differing in
their constants section to feed them into one solver, i.e.~constants are not
built into the executable.


Lastly remember that you can always specify file paths in the \texttt{constants}
section and then use your own application-specific file parser.
\end{design}

\section{The init function}\label{sec:init-func}
The ExaHyPE toolkit equipps every solver with a function \texttt{init}
which is called immediately after the solver construction, ie. at startup
time. It is comparable to a \texttt{main} function in regular C programs.

The init method is  the right place to do any startup preparation,
for instance to prepare loading heavy initial data. There is no
parallelization done when calling \texttt{main}, so it might want to
make use of TBB and MPI to realize the preparation of initial data.
Note that the actual initial data are fed by the
\texttt{adjustedSolutionValues(x,Q)} function (cf. section 
\ref{section:setup}). Users may want to extend their solver class to
store prepared initial data, if neccessary.

ExaHyPE also supports passing command line parameters down to every
solver. This allows developers to quickly setup a workflow for passing
extensive own parameter files or command line parameters. To do so,
they can exploit the (exemplary) signature

\begin{code}
void Euler::MyEulerSlover::init(std::vector<std::string>& cmdlineargs) {
  std::cout << "Command line options: \n";
  for(auto arg& : cmdlineargs) {
     std::cout << "- " << arg << "\n";
  }
}
\end{code}


% 
% 
% \noindent
% It is now a convenient exercise to make your simulation code run several solvers
% simultanously in the code.
% For this, add multiple solver sections to the specification file.
% If all solvers solve the same PDE, it is a natural choice to make one solver
% invoke fluxes, initial conditions, \ldots from the other solvers.
% \exahype\ does not support (yet, i.e.~in this chapter) a feature to reuse one
% solver multiple times.
% As you have different solver sections in your specification file, you can make
% them tackle different constants.
% This way, you save grid construction overhead (the actual grid holding the
% solvers is maintained only once), and you typically increase the arithmetic
% intensity of your code. 
% It thus should scale better.




\section{Time stepping strategies}

\exahype\ provides various time stepping schemes for both the ADER-DG solvers
and the Finite Volume solvers.
You can freely combine them for the different solvers in your specification
file.
Please note that some schemes require you to recompile your code with the flag
\texttt{-DSpaceTimeDataStructures} as they need lots of additional memory for
their realisation.
At the same time, it would be stupid to invest that much memory, i.e.~to
increase the memory footprint, if these flags were defined all the time.

If you combine various time stepping schemes, please note that the most
restrictive scheme determines your performance.
\exahype\ has time stepping schemes that try to fuse multiple time steps, e.g.,
and thus is particulary fast on distributed memory machines where
synchronisation between time steps can be skippted.
However, if you also have a solver with a tight synchronisation in your spec
file, these optimisations automatically have to be switched off.

\begin{description}[style=nextline]

\item[global] The \texttt{global} time stepping scheme makes each cell
advance in time per iteration with a given time step $\Delta t$ irrespective of
its spatial resolution. If one cell in the domain finds out that $\Delta t$
harms its CFL condition, all cells in the domain are rolled back to their last
time step, $\Delta t$ is reduced, and the time step is ran again. If a time step
finds out that it has been too restrictive, the code carefully increases
$\Delta t$ for the next time step. So the scheme is a global scheme that does
not anticipate any adaptive pattern (small cells usually are subject to more
restrictive CFL conditions than coarse cells). However, the time step size is
adaptively chosen. This implies that all ranks have to communicate once per time
step, i.e.~the scheme induces a tight synchronisation.

\item[globalfixed] The \texttt{globalfixed} time stepping scheme runs one
time step of type \texttt{global} which determines a global $\Delta t$. From
hereon, it uses this $\Delta t$ for all subsequent time steps. If it turns out
that the CFL condition is harmed through $\Delta t$ later on throughout the
simulation, no action is taken. In return, the scheme allows \exahype\ to
desynchronise all the ranks. This is among the fastest schemes available in the
code. Furthermore, as the code knows the time step size and knows the time
intervals between two plots a priori, it typically runs multiple time steps in
one rush, i.e.~you will not receive one terminal plot per time step, but most
terminal outputs will summarise multiple time steps.

\end{description}


\section{Material parameters}
\label{section:advanced-features:material-parameters}


\exahype\ does not ``natively'' distinguish unknowns that are subject to
a PDE from parameters on the user side. 
It however provides a \texttt{parameter} keyword.
Syntax and semantics of this keyword equal the \texttt{variables}, i.e.~you may
either just add a parameter quantity or give the parameters symbolic names.

\begin{code}
  solver ADER-DG MyEulerSolverWithParameters
    variables const    = rho:1,j:3,E:1
    parameters const   = MaterialA:1,MaterialB:2
    order const        = 3
    maximum-mesh-size  = 0.5
    time-stepping      = global
    type const         = nonlinear
    terms const        = flux
    optimisation const = generic
    language const     = C
  end solver
\end{code}


Technically, parameters are simply appended to the actual unknows:
If you specify 4 unknowns and 3 (material)
parameters, all \exahype\ functions just seem to handle 4+3=7 variables. 
Notably, one set of parameters is associated to each integration point.
Parameters hence are directly accessed in \texttt{Q} via the indices 4,5 and 6
(in this example) or via the generated \texttt{Variables} array wrappers.

Obviously, material parameters may not change over time---unless an application
code manually resets them in \texttt{adjustSolution}.

\begin{design}
\exahype\ models material parameters as additional unknowns $Q_p$ in the PDE that
are subject to $\partial _t Q_p = 0$ on an analytical level (no dissipation).
\end{design}

\noindent
This paradigm is not revealed to the user code that always receives a native
flux and eigenvalue function with all variable plus parameter entries.
However, the \exahype\ internally sets all fluxes and eigenvalues in the
corresponding routines to zero, i.e.~they are a posteriori eliminated from any
equation system. This way, parameters do not diffuse and are not transported.

% We only distinguish the values in the script
% file as we obviously do not want to run any PDE kernel (unknown update) on top
% of the material parameters.
% For them, we restrict to a subset of all the variables---namely the first 4
% in the example above.
% To the user, we do not distinguish material parameters from unknowns. 
% When you set up the initial condition, e.g., you will be passed 7 unknowns.

\section{Alternative symbolic naming schemes}
\exahype\ allows you to specify an arbitrary number of additional symbolic
naming schemes other than \texttt{variables} and \texttt{parameters} per solver.
These naming schemes must be listed below the field
\texttt{variables} or below \texttt{parameters} if the latter is present.  

To give an example: We might prefer to compute the fluxes in primitive variables
instead of the conserved ones. 
We thus add a symbolic naming scheme ``\texttt{primitives}''
to our solver:
\begin{code}
  solver ADER-DG MyEulerSolverWithParametersAndPrimitives
    variables const   = rho:1,j:3,E:1
    parameters const  = MaterialA:1,MaterialB:2
    primitives const  = rho:1,u:3,E:1
    order const       = 3
    [..]
  end solver
\end{code}

The \exahype\ toolkit will then generate another array
wrapper named \texttt{Primitives} which can be accessed in similar ways as
\texttt{Variables} and \texttt{Parameters}.


\section{Non-conservative formulations}
\label{section:advanted:non-conservative-formulations}

Most of this document considered only how to implement strongly
hyperbolic conservation laws, ie. PDEs which can be casted to
the form
%
\begin{equation}
\frac{\partial}{\partial t} \textbf{Q}
+
\boldsymbol{\nabla}\cdot\textbf{F}(\textbf{Q})
= \boldsymbol S(\boldsymbol Q).
\end{equation}
%
However, the generic kernels in ExaHyPE also support hyperbolic
PDEs with non-conservative terms which can be written in a
quasi-linear form as
%
\begin{equation}
\frac{\partial}{\partial t} \textbf{Q}
+
\boldsymbol{\nabla}\cdot\textbf{F}(\textbf{Q})
+
\boldsymbol \sum_i \boldsymbol B_i(\boldsymbol Q )\,\frac{\partial \boldsymbol
Q}{\partial x_i}
+
\boldsymbol B_0(\boldsymbol Q )\,\boldsymbol Q
 = 
\boldsymbol S(\boldsymbol Q).
\end{equation}
%
with matrices $\boldsymbol B_i(\boldsymbol Q)$ per space dimension $i$.
The $\boldsymbol B_i$ are used in the ADER-DG and FV schemes in the Riemann
solver and in the space-time predictor. Pleae compare these
formulations also with the generic solver structure as proposed
in section \ref{sec:solver-configuration}.

\begin{design}
By default, we adopt a Riemann solver which anticipates the
nonconservative fluxes. Therefore, we do not allow source terms
$\boldsymbol S(\boldsymbol Q, \nabla \boldsymbol Q)$ but split
such a source term into the \emph{algebraic source}
$\boldsymbol S(\boldsymbol Q)$ and the \emph{non-conservative}
source
(or non-conservative product) $\boldsymbol B \cdot \nabla \boldsymbol Q$.
\end{design}

\subsection{Signatures for the non-conservative product}
The special treatment of the non-conservative product in the
ADER-DG scheme implemented in ExaHyPE requires to specify the
non-conservative product explicitely. We experimented so far
with two signatures:
%
\begin{code}
   NCP(BgradQ, Q, gradQ): BgradQ = B(Q)*gradQ
   matrixB(Bn, Q, nv):    B[n] = B(Q)
\end{code}
%
The symbol \texttt{BgradQ} has different meanings in the linear
and nonlinear kernels:

\begin{itemize}
  \item \texttt{BgradQ} is a vector of size
  \texttt{NumberOfVariables} if you use \exahype's ADER-DG kernels for
  nonlinear PDEs.
  \item \texttt{BgradQ} is a tensor of size
  \texttt{Dimensions} $\times$ \texttt{NumberOfVariables}  
  if you use ADER-DG kernels for linear PDEs,
\end{itemize}
%
However, note that in the generic nonlinear kernels, we decided
to use only the \texttt{NCP} variant. Users therefore are asked
to provide the result vector
$\boldsymbol B \cdot \nabla \boldsymbol Q$.

\subsection{The fused Source}

In many PDE systems, the computation of the non-conservative
product as well as the algebraic source is expensive. In the scheme,
at one point both quantities are just added to a
\emph{right hand side} source term $\boldsymbol S - \boldsymbol B \cdot \nabla \boldsymbol Q$ which we refer to as the \emph{fused source}.
%
\begin{design}
In ExaHyPE, the user PDE application interface shall be
\emph{minimalistic}. We just do not support to implement a fused
source with the generic kernels. It is subject to optimised kernels
to exploit the PDE structure and skip unneccessary PDE calls.
\end{design} 


\section{Point sources}\label{sec:point-sources}

While we support a generic way of expressing Source terms, this approach
is not ideal in the vicinity of Dirac shaped point sources. Such sources
typically occur in Seismology applications. In order to exploit the high
order features of the ADER-DG scheme, we let users instead specify the
actual positions of such point sources.

\todo[inline]{This section is still under construction.
Kduru or the Seismic people might add some text to it.}

\section{Custom Riemann Solvers}

ExaHyPE recognizes the fact that there's not a single numerical scheme to
rule all kind of problems. Therefore, the user API is quite flexible and allows
users to engage parts of the generic kernels while implementing other parts on
their own. For instance, in both the ADERDG and Finite Volume schemes, users can
overwrite the Riemann Solver with their own implementation.

Start with a spec file that asks for a standard Riemann solver, such as \texttt{type=godunov} and then
overwrite the Riemann solvers implementation in the abstract solver interface in the user solver.

In the ADERDG scheme, the Riemann Solver is called once per patch/cell. In the
Finite Volume schemes, the Riemann Solver is called on each point.

As an example we provide here an implementation of the Rusanov flux (as it is currently
implemented in \exahype) for a simple
system without non-conservative products or source terms.

\begin{code}
 double riemannSolver(double* fL, double *fR,
                      const double* qL, const double* qR,
                      int direction) {
  constexpr int numberOfVariables  = SolverType::NumberOfVariables;
  constexpr int numberOfParameters = SolverType::NumberOfParameters;
  constexpr int numberOfData       = numberOfVariables + numberOfParameters;

  double sL[numberOfVariables];
  double sR[numberOfVariables];
  solver.eigenvalues(qL, normalNonZero, sL);
  solver.eigenvalues(qR, normalNonZero, sR);

  double s_max = -1.0;
  for (int i = 0; i < numberOfVariables; i++) {
    const double abs_sL_i = std::abs(sL[i]);
    s_max = std::max( abs_sL_i, s_max );
  }
  for (int i = 0; i < numberOfVariables; i++) {
    const double abs_sR_i = std::abs(sR[i]);
    s_max = std::max( abs_sR_i, s_max );
  }
  
  double FL2[DIMENSIONS][numberOfVariables] = {0.0};
  double FR2[DIMENSIONS][numberOfVariables] = {0.0};
  double* FL[DIMENSIONS]={FL2[0], FL2[1]};
  double* FR[DIMENSIONS]={FR2[0], FR2[1]};
  solver.flux(qL, FL);
  solver.flux(qR, FR);
  
  for (int i = 0; i < numberOfVariables; i++) {
    fL[i] = 0.5 * s_max * (qL[i] - qR[i]) 
                  + 0.5 * (FL2[normalNonZero][i] + FR2[normalNonZero][i]);
    fR[i] = fL[i];
  }
  return s_max;  
}
\end{code}

\todo[inline]{Provide
a better example, for instance an Osher-type Riemann Solver.}







