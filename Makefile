.IGNORE:

TARGET = guidebook

pdflatex = pdflatex -interaction=nonstopmode

all: pdf

pdf: 
	$(pdflatex)     "$(TARGET)"
	#bibtex         "$(TARGET)"
	$(pdflatex)     "$(TARGET)"
	$(pdflatex)     "$(TARGET)"

.PHONY: clean vclean

clean:
	/bin/rm -f *.dvi *.log *.aux *.toc *.bbl *blg *.end *.out *.4ct *.4tc *.idv *.lg *.tmp *.xref *.idx *.ptc *.synctex.gz *.lof *.lot *.tdo

vclean: clean
	/bin/rm -f $(TARGET).pdf  *~
	/bin/rm -rf build/
	# this is when make4ht generated stuff in the wrong directory
	/bin/rm -f *.html *.png *.css

deploy:
	./upload.sh

compress: 
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.7 -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$(TARGET)-compressed.pdf $(TARGET).pdf

release:
	make pdf TARGET="\def\releasemode{1} \input{guidebook.tex}"
