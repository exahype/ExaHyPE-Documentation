\section{Frequently asked user questions}\label{sec:user-faq}

\begin{description}
  \item [ How do I modify the computational domain?]
    Open the specification file and alter the \texttt{width}. The entry
    specifies the dimensions of the computational domain. Please note that
    \exahype\ only supports square/cube domains as it works on cube/square
    cells only.
    You might have to extend your problem's domain accordingly.  \texttt{width}
    is read at startup time, i.e.~there's no need to rerun any script or
    recompile once you have changed the entry. Just relaunch your simulation.
    
  \item [ How do I alter the spatial resolution $\Delta x$?]
    ExaHyPE asks you to specify the \texttt{maximum-mesh} \texttt{-size} which
    determines the coarsest resolution in your grid. The number of cells in each direction
    on a unigrid layout however is always $3^i$ where $i$ is the depth.
    While these statements hold for serial codes, parallel codes even might have 
    other, prescribed, subdivision ratios.  
    That is, \exahype\ is tied to some resolutions and always chooses the
    coarsest resolution that still is finer or equal to a prescribed mesh size. 
    Examples for possible mesh values for a serial code are given by table
    \ref{table:faqdx} with depth $d$, number of cells in one spatial direction $N=3^d$, real grid
    spacing $\Delta x = 3^{-d}$ and \texttt{maximum-mesh-size}
    $\Delta x_\text{max} > \Delta x$. It may help to quickly choose the right
    value for $\Delta x_\text{max}$.
    
    \begin{table}[h]
    \centering
    \begin{tabular}{l r r r}
        \toprule
        $d$ &
        $N$ &
        $\Delta x$ &
        $\Delta x_\text{max}$
        \\ \midrule
        1 &   3 & 0.333 & 0.50 \\
        2 &   9 & 0.111 & 0.20 \\
        3 &  27 & 0.037 & 0.10 \\
        4 &  81 & 0.012 & 0.02 \\
        5 & 243 & 0.004 & 0.01 \\
        \ldots & & &\\ 
        \bottomrule
     \end{tabular}
     \caption[Possible unigrids in ExaHyPE]{Possible unigrid (regular grid) layouts with
     \exahype. All quantities refer to the unit cube/square.}\label{table:faqdx}
     \end{table}
     
    In practice, all your postprocessing has to be able to cope with the mesh
    size \exahype\ gives you. In return, you can be sure that \exahype\ always
    chooses mesh sizes that are at least as fine as required and that it chooses
    mesh sizes that are efficient w.r.t.~runtime.
    
    If the exact mesh size/character is important to you and you don't want to
    extract it from plots (which is the recommended way for AMR production runs), you might
    want to retranslate your code with \texttt{-DTrackGridStatistics} which
    plots the grid resolution after each step. Typically, you this option to the
    makefile in your application's folder:
    \begin{code}
PROJECT_CFLAGS+=-DTrackGridStatistics
    \end{code}
    
    
  \item [ How do I alter the time step size?]
    \exahype\ implements two time stepping schemes (global time stepping and
    globally fixed time stepping).
    Per solver, you can choose a strategy.
    All schemes run a trial time step in the first iteration to evaluate the CFL
    condition and determine a reasonable time step size.
    All schemes besides  \texttt{globalfixed} then continue to study
    the CFL constraints and adopt the time step size automatically.
    Only  \texttt{globalfixed} fixes the time step size for the whole simulation
    period. 
    All schemes use a security factor on the CFL condition, i.e.~the admissible
    time step size is scaled before it is applied.
    This scaling is set by \texttt{fuse-algorithmic-steps-factor} in the
    \texttt{global-optimisation} block. 
    Its value has to be small or equal to one.
    Besides this security factor, nothing has to be
    done by the user.
  \item [ How do I alter the time step size (advanced)?]
    You may however choose the time step size yourself if you do not rely on the
    generated kernels but write a whole solver yourself (see solver variant
    \texttt{user::defined} in Section \ref{chapter:aderdg-user-defined-fluxes}) from
    scratch.
  \item [ Why do the time stamps in the plots differ from the ones I
     set in the specification?]
     Plotting at specific times would require \exahype\ to perform shorter time
     steps than prescribed by the CFL condition. This
     leads to long-term pollution of the numerical solution due to artificial
     diffusion.
     We thus track when the next plot would be due and plot as soon as the
     solver time (minimum over all cells) is greater or equal to the next plotting time stamp.

  \item [ ExaHyPE did not create all the plots I requested. Why?]
     It is very likely that more than one of the plotting times you specified
     did fall into a single time step interval of the corresponding solver.
     In this case, see bullet point {\bf Why do the time stamps in the plot
     output differ from the ones I have set in the specification file?}.

  \item [ How do I change the resolution dynamically?]
    The dynamic AMR is realised along the same lines as the static refinement.
    You have to implement \texttt{refinementCriterion} in your solver which
    tells \exahype\ where you want to refine. How this is realised---notably how
    all the ranks and cores synchronise---is hidden away from the user code and
    decided in the kernel. As such, it is more of a developer question than a
    user question.

  \item [ To be continued \ldots]
\end{description}
