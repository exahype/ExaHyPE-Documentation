\section{Datastructures in the Optimised Kernels}
\label{section:datastructures}

\normalsize
This section provides an up-to-date documentation
of the memory layout employed in the optimised
solver kernels.
\paragraph{System Matrices}
The optimised kernels use padded structures and thereby
differ from the generic code base. Padding, of course,
depends on the target microarchitecture. It guarantees
that the height of the matrices is always a multiple of
the SIMD width.
\begin{center}
  \includegraphics[scale=1]{sketches/paddedMatrix.pdf}
\end{center}

All matrices are stored in column major order.
Table~\ref{table:optimised-matrices} lists the memory layout.
The corresponding CodeGen files are \texttt{DGMatrixGenerator.py}
and \texttt{WeightsGenerator.py}. The matrix \texttt{dudx} is
only generated in the case of a linear PDE.


\begin{table}[h!]
\footnotesize{
\centering
\begin{tabular}{lll} \toprule
  \multicolumn{1}{c}{\textbf{Matrix}} & 
  \multicolumn{1}{c}{\textbf{Memory Layout}} &
  \multicolumn{1}{c}{\textbf{Size}}
  \\
\midrule \midrule
Kxi        & [[Kxi];0] & (nDOF+Pad) $\times$ nDOF \\
iK1        & analogous to Kxi             & \\
dudx       & analogous to Kxi             & \\
FRCoeff    & [FRCoeff]                    & 1 $\times$ nDOF \\
FLCoeff    & analogous to FRCoeff         & \\
F0         & [F0;0]                       & 1 $\times$ (nDOF+Pad) \\
\midrule
equidistantGridProjector1d & analogous to generic version & \\
fineGridProjector1d        & analogous to generic version & \\
\bottomrule
\end{tabular}
\caption{Structure of the system matrices in the optimised kernels.}\label{table:optimised-matrices}
}
\end{table}

\newpage
\paragraph{Quadrature Weights}
The solver kernels exhibit three distinct patterns of weights.
Either occurrence is mapped onto a separate data structure.
\begin{itemize}
\item \texttt{wGPN}. Quadrature weights.
\item \texttt{aux = (/ 1.0, wGPN(j), wGPN(k) /)}. Combination of
two weights, stored in weights2.
\item \texttt{aux = (/ wGPN(i), wGPN(j), wGPN(k) /)}. Combination
of three weights, stored in weights3.
\end{itemize}
Table~\ref{table:optimised-weights} illustrates the memory layout
for a 2D and 3D setup, respectively. In any case, the weights
vectors are interpreted as linear, padded arrays. The pad width depends
on the microarchitecture and ensures that the total length of the vectors
is a multiple of the SIMD width. In the 2D case, the vectors
\texttt{weights1} and \texttt{weights2} coincide because their
computational pattern coincides.

\begin{table}[h!]
\footnotesize{
\centering
\begin{tabular}{lll} \toprule
  \multicolumn{1}{c}{\textbf{Name}} & 
  \multicolumn{1}{c}{\textbf{Memory Layout}} & 
  \multicolumn{1}{c}{\textbf{Meaning}} \\
\midrule \midrule
\multicolumn{3}{c}{2D} \\
\midrule
weights1        & $[w_i,0]$  	  & quadrature weights + Pad\\
weights2        & $[w_i,0]$  	  & quadrature weights + Pad\\
weights3        & $[w_iw_j,0]$    & outer product of quadrature weights + Pad\\
\midrule
\multicolumn{3}{c}{3D} \\
\midrule
weights1        & $[w_i,0]$  	  & quadrature weights + Pad\\
weights2        & $[w_iw_j,0]$ 	  & outer product of quadrature weights + Pad\\
weights3        & $[w_iw_jw_k,0]$ & all combinations of quadrature weights + Pad\\
\bottomrule
\end{tabular}
\caption{Variables holding combinations of quadrature weights employed in the optimised kernels.}\label{table:optimised-weights}
}
\end{table}


\paragraph{Parameters and Local Variables}
Table~\ref{table:optimised-datastructures} lists the structure of the
input/output parameters of the solver kernels and local variables used
within the solver kernels. The parameters nDOFx, nDOFy and nDOFz denote
the number of degrees of freedom in x-, y-, and z-direction, respectively.
Analogously, nDOFt gives the number of degrees of freedom in time. All
variables are linearised, i.e. one-dimensional arrays.



\begin{table}
\scriptsize{
\centering
\begin{tabular}{lll} \toprule
  \multicolumn{1}{c}{\textbf{Name}} & 
  \multicolumn{1}{c}{\textbf{Ordering}} &
  \multicolumn{1}{c}{\textbf{Size}}
  \\
\midrule \midrule
rhs, rhs0       & (nVar,nDOFx,nDOFy,nDOFz,nDOFt) 		& nVar $\cdot$ nDOF${}^4$ \\
lFhbnd           & (nVar, nDOFx, nDOFy, nFace) 				& (nVar+Pad) $\cdot$ nDOF${}^2\cdot$ 6 \\
lQhbnd           & (nVar, nDOFx, nDOFy, nFace) 				& (nVar+Pad) $\cdot$ nDOF${}^2\cdot$ 6 \\
lqh (nonlinear) & (nVar, nDOFt, nDOFx, nDOFy, nDOFz)		& (nVar+Pad)$\cdot$nDOF${}^4$ \\
lqh (linear)    & (nVar, nDOFx, nDOFy, nDOFz,nDOFt+1)		& (nVar+Pad)$\cdot$nDOF${}^3\cdot$(nDOF+1) \\
lFh             & (nVar, nDOFx, nDOFy, nDOFz,nDOFt,d)		& (nVar+Pad)$\cdot$nDOF${}^4 \cdot$d \\
lFhi            & irregular					& (nVar+Pad)$\cdot$nDOF${}^3 \cdot$d \\
luh, lduh       & (nVar, nDOFx, nDOFy, nDOFz)			& nVar $\cdot$ nDOF${}^3$ \\
\midrule
\multicolumn{3}{c}{temporary variables} \\
\midrule
QavL, QavR      & (nVar)                                        & (nVar+Pad) \\
lambdaL, lambdaR& (nVar)                                        & (nVar+Pad) \\
\midrule
\multicolumn{3}{c}{auxiliary variables} \\
\midrule
tmp\_bnd        & (nVar, nDOFx, nDOFy)                          & (nVar+Pad) $\cdot$ nDOF${}^2$ \\
s\_m (``\textit{s}caled \textit{m}atrix'')  & analogous to Kxi  & \\
s\_v (``\textit{s}caled \textit{v}ector'')  & analogous to F0   & \\
\bottomrule
\end{tabular}
\caption{Data layout (Fortran notation) used in the optimised kernels.}\label{table:optimised-datastructures}
}
\end{table}

The parameter lFhi is decomposed into three tensors lFhi\_x, lFhi\_y, lFhi\_z, all sized (nVar+Pad)$\cdot$nDOF${}^3$
\begin{center}
\includegraphics{sketches/lFhi.pdf}
\end{center}
Their internal ordering of the degrees of freedom depends on the direction.
\begin{itemize}
\item \texttt{lFhi\_x}. (nVar, nDOFx, nDOFy, nDOFz)
\item \texttt{lFhi\_y}. (nVar, nDOFy, nDOFx, nDOFz)
\item \texttt{lFhi\_z}. (nVar, nDOFz, nDOFx, nDOFy)
\end{itemize}

Note that the structure of luh respectively lduh is chosen for compatibility with the generic kernels. 
