\chapter{Upscaling rationale}
\label{chapter:upscaling-rationale}


\exahype's and Peano's domain decomposition is sometimes somehow non-orthodox. 
People tend to classify it as space-filling curve-based (SFC) parallelism.
Yet, that falls short. 
In this chapter, we quickly sketch how it works.
The sketches themselves are done in 1d to keep the explanations simple.
All the real-world data presented stem from an Euler flow run as described
before in this document.
The output is analysed through Peano's Python script
\texttt{domaindecomposition.py} throughout the first part of the section.
The later text then recompiles \exahype\ with \texttt{MODE=PeanoProfile} and
uses the script \texttt{performanceanalysis.py} to squeeze out more data from
the runs.



\section{Greedy spacetree decomposition}

We start off with thee greedy decomposition scheme, i.e.~we use the flags
\begin{code}
configure = {greedy-naive,FCFS,virtually-expand-domain}
\end{code}

\noindent
Please note that we found the flag \texttt{virtually-expand-domain} be
absolutely mandatory as it rescales the domain to MPI's needs. 
We therefore use it in all of our experiments. 
The flag is detailed in Section \ref{section:mpi:grid-modifications} and
notably the Peano handbook.


\exahype\ inherits Peano's tree decomposition concept, i.e.~its MPI variant cuts
the global spacetree into subtrees recursively as long as there are ranks
available that can then take control of these cut-out subtrees. 
In this first version, the cutting-out is done greedy in a first come first
served (FCFS) manner:
whichever rank decomposes into more ranks further is successful with its
further decomposition while other ranks that want to split their tree with
someone else run into a lack of free ranks.

\begin{figure}
\begin{center}
  \includegraphics[width=0.5\textwidth]{sketches/mpi-rationale-greedy.pdf}
\end{center}
\caption{Spacetree encoding MPI ranks by color.}\label{fig:mpi-rationale-greedy}
\end{figure}

\noindent
Such an approach can yield a situation as depicted by the sketch above: a white
rank being responsible for the root of the spacetree forks into a blue rank.
Though the white rank continues to traverse the tree, the blue rank is faster
and is served quicker with the two other remaining ranks (red and green). We
obtain a very ill-balanced outcome.
Once we pass the output into Peano's domain decomposition script, it clearly
highlights the ill-balancing both through its logical topology tree (which is
unreasonable deep) and the histogram of the work distribution.

\begin{figure}[h]
\begin{center}
\subfloat[One splitting of a regular grid covering the unit square by a greedy
  tree decomposition with 24 ranks running on four nodes.]{\includegraphics[width=0.35\textwidth]{screenshots/73_dd-fcfs.pdf}}
\hspace{1.2cm}
\subfloat[The corresponding topology. Rank 1 is the most successful rank with
  booking additional workers. All other ranks find no more idle ranks left when
  they try to split their domain further.]{%
  \includegraphics[width=0.45\textwidth]{screenshots/73_topology-fcfs.pdf}}
\end{center}
\caption{Details on the pictured MPI balancing as shown in figure \ref{fig:mpi-rationale-greedy}.}
\end{figure}

\noindent
With this situation, we cannot scale up in terms of performance as we are
radically ill-balanced---though adding more ranks might pseudo-randomly bring
along a performance improvements---and notably soon run into memory footprint
constraints.



\section{A fairer load distribution}

We change to the
\begin{code}
configure = {hotspot,fair,virtually-expand-domain,ranks-per-node:6}
\end{code}

\noindent
configuration next. It introduces two new/alternative concepts:
\begin{enumerate}
  \item The tree is not greedily cut into pieces but always the rank with the
  largest load (the hotspot) is assigned additional ranks next. That is, the
  rank responsible for the largest tree cuts out subtrees from its tree. In the
  example below, where the x cell denotes an outer cell---cells outside the
  computational domain do not carry workload---the white rank (the global
  master) first splits up the workload between blue, red and green. Red and
  green thus become hotspots and are each given another rank as worker. We
  assume in this sketch that we have six ranks in total. As one rank is reserved
  for load balancing and algorithm control (white), the load balancing stops
  now.
  \begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.5\textwidth]{sketches/mpi-rationale-fair.pdf}
  \end{center}
  \caption{MPI Rationale: Fair}
  \end{figure}
  \item Fairness is built into the system by taking \texttt{ranks-per-node} into
  account. Per spacetree level, the load balancing tries to distribute the work
  among all compute nodes. Let two ranks (white/blue, red/yellow and
  green/light blue) be ran per compute node. As white is rank 0, the load
  balancing next employs ranks green and red from other nodes while distributing
  before it returns to blue.
\end{enumerate}


\noindent
The effect of the improved load balancing is a slightly better load balancing.
It notably succeeds if the computational load can be evenly distributed among
all compute nodes:
If we run a totally regular grid (without the recommended virtual expansion) on
$\geq 3^d+1$ ranks, we should observe almost perfect scaling.
The +1 results from the fact that \exahype\ reserves one MPI rank for a global
master always.


Yet, the scheme tends to yield a step pattern in the scalability curve: 
While it scales for $3^{d\ell }+1$ ranks, adding a few more ranks
does not improve the scaling further.
It is only when we meet $3^{d(\ell +1)}+1$ ranks that we have the next
significant improvement in performance.


\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.35\textwidth]{screenshots/73_dd-fair.pdf}
  \hspace{1.2cm}
  \includegraphics[width=0.45\textwidth]{screenshots/73_topology-fair.pdf}
  \\
\end{center}
\caption{%
  FCFS tree decompositions are replaced by a fair strategy. This yields better
  balanced trees (shallow compared to FCFS) and the subdomains obviously are
  better distributed.
}
\end{figure}


\begin{figure}[h]
\begin{center}
\subfloat[Workload distribution among the four compute nodes for FCFS.]{%
  \includegraphics[width=0.45\textwidth]{screenshots/73_workload-per-node-fcfs.pdf}}
  \hspace{0.8cm}
\subfloat[A fair workload distribution.]{%
  \includegraphics[width=0.45\textwidth]{screenshots/73_workload-per-node-fair.pdf}}
  \\
\end{center}
\caption{Workload per Node: FCFS and FAIR}
\end{figure}


\section{Per-node workload distribution and diffusion along an SFC}

For the subsequent idea, we weaken our notion of a fixed number of MPI ranks per
node and instead assume that we have a number of MPI ranks per compute node that
we want to use at least.
These are called primary nodes.
And then we have some more ranks as resiliency.
\begin{code}
configure = {sfc-diffusion,hotspot,virtually-expand-domain,
  ranks-per-node:2,primary-ranks-per-node:1}
\end{code}

\noindent
In the example above, such a setup means that we start our job with three MPI
ranks per node, but we do accept/assume that usually only two of them are used.
We stick to our hotspot cutting of the spacetree, but this new rank assignment
strategy is an extension of the fair strategy.
As long as some primary ranks remain available, it distributes those guys. 
Once the next hotspot decomposition strategy would use more than the primary
ranks, the SFC diffusion allows one more load balancing step and then terminates
the balancing.
In this final wrap-up step, it tries to serve all those spacetree cut requests
that can be fit into a SFC cut scheme, and it serves only those that improve the
balancing per node slightly.
It is one load diffusion step.

\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.5\textwidth]{sketches/mpi-rationale-sfc.pdf}
\end{center} 
\caption{MPI rationale SFC}
\end{figure}

\noindent
Let there be three compute nodes.
For the example above our idea means: 
The global master splits up into blue, red, green. 
The partitioning is ill-balanced, yet there are no more primary nodes left.
The load balancing thus starts to employ the remaining ranks.
From all the ranks that might be able to decompose their partition further, it
first allows green to deploy work to further nodes (not ranks).
It books one additional rank from the node hosting the red primary rank.
Next, the red rank is allowed to book one additional rank from the node that
hosts the blueish ranks.


\begin{itemize}
  \item With these concepts at hands, we have balanced the workload reasonably among the
  nodes and the subdomains deployed to one node are reasonably connected.
  \item We have not balanced the workload per rank.
  \item We have not balanced the ranks among the nodes.
\end{itemize}


\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.5\textwidth]{screenshots/73_dd-sfc.pdf}
\end{center} 
\caption{%
  SFC-based decomposition. We observe a better connectivity of the subdomains.
}
\end{figure}


\noindent
The configuration so far thus lets us upscale in terms of memory.
We do not run into out-of-memory problems that fast anymore.
It does not yet allow us to upscale in terms of performance.


\paragraph{Some working setups:}
\begin{itemize}
  \item On Durham's 24-core Broadwells, we made good experience with six MPI
  ranks per node with four out of those acting as primary ranks.
\end{itemize}

% \section{Batching}
% 
