\chapter{Optimisation}



\section{High-level tuning}

\exahype\ realises few high level optimisations that you can switch on and off
at code startup through the specification file. To gain access to these
optimisations, add a paragraph 

\begin{code}
  global-optimisation
    ...
  end global-optimisation  
\end{code}

\noindent
at the end of your specification file. It requires a sequence of parameters in a
fixed order as they are detailed below.

\paragraph{Step fusion.} 
Each time step of an \exahype\ solver consists of three phases: computation
of local ADER-DG predictor (and projection of the prediction onto the cell
faces), solve of the Riemann problems at the cell faces, 
and update of the predicted solution. We may speed up the code
if we fuse these four steps into one grid traversal. To do so, we set 
\texttt{fuse-algorithmic-steps}.
Permitted values are \texttt{on} and \texttt{off}.

The fusion is using a moving average kind time step size
estimate. If we detect a-posteriori that this estimate
has violated the CFL condition, we have
to rerun the predictor phase of the ADER-DG scheme
with a time step size that does not violate the 
CFL condition. In our implementation, 
such a CFL-stable time step size is
available after the ADER-DG time step has been performed.

We offer to rerun the predictor phase of the ADER-DG
scheme with this CFL-stable time step size weighted by a factor\\
\texttt{fuse-algorithmic-steps-factor}. This problem-dependent factor must be
chosen greater zero and smaller to/equal to one.  A value close to one might
lead to more predictor reruns during a simulation 
but to less additional numerical diffusion.      
\begin{code}
  global-optimisation
    fuse-algorithmic-steps   = off
    fuse-algorithmic-steps-factor = 0.99
    ...
  end global-optimisation  
\end{code}


\paragraph{Restrict adaptivity during time stepping iterations}
For many setups, it makes
sense to disable the dynamic AMR from time to time, e.g. 
when multiple iterations are batched and the code
runs a fixed number of iterations or when the previous grid iteration did not
identify any refinement and thus we have to expect that the probability of a
refinement in the subsequent iteration is low, e.g.

Often, the CFL condition in local time stepping requires the code to perform $N$
grid sweeps and it is sufficient if the grid is adopted afterwards.
This can speed up the code in many cases.
To tell \exahype\ to throttle the dynamic adaptivity, set the flag to \texttt{on}.

Switching dynamic AMR off during time steps removes some
additional neighbour communication which might have an impact on
the overall performance.

\begin{code}
  global-optimisation
    ...
    disable-vertex-exchange-in-time-steps = on
  end global-optimisation  
\end{code}

\exahype\ will additionally turn the exchange of metadata completely
off in all algorithmic phases where no mesh refinement or
limiter status spreading is performed.

\paragraph{Batching of time steps.}
If you use fixed, adaptive or anarchic time stepping, Peano can guess from an
evaluation of the CFL condition how many grid sweeps (global time steps) are
required to advanced all patches such that the next plotter becomes active or
we meet the simulation's termination time.
Each grid sweep/global time step might update only a few patches and their
permitted time step size again might change throughout the simulation run.
So we can only predict.
For MPI's speed is it advantageous if multiple time steps are triggered in one
rush---this way, ranks that have already finished its traversal know whether
they can immediately continue with the subsequent time step.
To allow so, you have to set \texttt{time-step-batch-factor} to a value
from $(0,1($.
Setting it to zero switches off this feature. 
The semantics is as follows:
The code computes how many time steps it expects to be required to reach the
next plotter or final simulation time, respectively.
This value is scaled with  \texttt{time-step-batch-factor} and the
resulting number of time steps then are ran.
A factor of around \texttt{0.5} has to be proven to be good starting point.

If you allow the code to run multiple iterations in one batch,
\exahype\ assumes that there is no need to broadcast and restrict any data (such
as minimal time steps) during intermediate batch iterations.
A broadcast and reduction is still performed in the first and last batch
iteration, respectively.

\begin{code}
  global-optimisation
    ...
    time-step-batch-factor = 0.5
  end global-optimisation  
\end{code}

\paragraph{Turning metadata sends off (in intermediate batch iterations)}
If you allow the code to run multiple iterations in one batch and
you do not employ dynamic limiting, it makes sense to turn off the
exchange of \exahype's solver metadata during intermediate batch
iterations.

\begin{code}
  global-optimisation
    ...
    disable-metadata-exchange-in-batched-time-steps = 0.5
  end global-optimisation  
\end{code}

\paragraph{Spawn prediction and/or fused time step as background jobs}
Certain space-time-predictor kernel computations can be spawned
as a background job. In particular, this is the case for cells where we do not
need to prolongate boundary fluxes to finer grids or where we do not 
need to restrict boundary fluxes up to more than one parent cell.

Additionally, we do not spawn a predictor background job if a cell
is adjacent to the boundary of an MPI rank's mesh partition as
we need to send out boundary fluxes to neighbours
directly in the current iteration. This cannot be delayed
to the beginning of the next iteration.

This feature can thus establishes an overlap of MPI communication and
computation as it prioritises cells at the boundary and postpones work
in the interior of an MPI rank's partition.

If we fuse the algorithmic phases of our solver and batching is switched on, we
can further spawn a whole fused time step instead of the predictor kernel 
as a background job during intermediate batch iterations. Of course, the above
constraints apply here as well.

To enable the feature, 
set the flag \texttt{spawn-predictor-as-background-thread} to \texttt{on}.
\begin{code}
  global-optimisation
    ...
    spawn-predictor-as-background-thread = on
  end global-optimisation  
\end{code}

\paragraph{Spawn AMR operations as background job}
Costly AMR operations such as the imposition of initial conditions and
evaluation of refinement criteria can also be performed as a background
job. This usually increases the concurrency of the mesh refinement
iterations however might add a few exta iterations as 
work is done in the next iteration.

To enable the feature, 
set the flag \texttt{spawn-amr-background-threads} to \texttt{on}.
\begin{code}
  global-optimisation
    ...
    spawn-amr-background-threads = on
  end global-optimisation  
\end{code}

\paragraph{Modify storage precision.}
\exahype\ internally can store data in less-than-IEEE precision. 
This can reduce the memory footprint of the code signficantly.
To allow \exahype\ to do so, the user has to specify through
\texttt{double-compression} which accuracy actually is required.
If the field is set to zero, \exahype\ works with full double precision and no
compression is activated.


While the data is compressed, \exahype\ nevertheless computes with double
precision always.
Consequently, data has to be converted forth and back: 
Any compressed data is converted into IEEE precision before any arithmetic
operation and later on compressed before it is written back to the main memory. 
This process is costly, though we can spawn it into background threads.
This is particularly convenient if you have idle cores `left' and controlled via
the flag \linebreak
\texttt{spawn-double-compression-as-background-thread}.


\begin{code}
  global-optimisation
    ...
    double-compression = 0.0
    spawn-double-compression-as-background-thread = off
  end global-optimisation  
\end{code}

\begin{remark}
 If you compile with assertions, you can ask \exahype\ explicitly to validate
 that all of your compressed data still is close (up to prescribed accuracy) to
 the actual data. For this, you compile with
 \texttt{-DValidateCompressedVsUncompressedData}.
\end{remark}


\section{Optimised Kernels}\label{sec:optimized-kernels}
\exahype\ offers optimised compute kernels. Given a specification file, the toolkit 
triggers the kernel generator to output optimised compute kernels for this specific setup. 

\paragraph{Prerequisites}
The code generator exhibits two dependencies. First, the kernel generator requires Python 3 with jinja2.
Second, it relies on Intel's libxsmm generator driver. 
Both are handled as \exahype\ submodule and should be downloaded locally when setting up \exahype.

\paragraph{Microarchitecture}
The optimised kernels explicitly use the instruction set available on the
target processor. You therefore have to define its microarchitecture in the
specification file. The specification file terms the processor architecture
just architecture. The supported options for this flag are given
in table \ref{table:flags1}.
\begin{table}
\begin{center}
\begin{tabular}{lp{6cm}} \toprule
  \multicolumn{1}{c}{\textbf{Architecture}} &
  \multicolumn{1}{c}{\textbf{Meaning}}  \\
  \midrule \midrule
  wsm & Westmere \\
  snb & Sandy Bridge \\
  hsw & Haswell \\
  knc & Knights Corner (Xeon Phi) \\
  knl & Knights Landing (Xeon Phi) \\
  skx & Skylake \\
  \bottomrule
\end{tabular}
\end{center}
\caption{Supported flags}\label{table:flags1}
\end{table}
You can identify the microarchitecture of your processor through
amplxe-gui, an analysis tool part of Intel VTune Amplifier.
Alternatively, you can obtain the `model name' via
\begin{code}
cat /proc/cpuinfo
\end{code}
and search for it on \url{http://ark.intel.com}.

The optimised kernels require an architecture to be set.

\section{An Example Optimised Specification File}

This section collects together the various optimisations discussed so far into a single production ready specification file.

\begin{code} 

exahype-project Euler

  peano-kernel-path const  = ./Peano
  exahype-path const       = ./ExaHyPE
  output-directory const   = ApplicationExamples/Euler
  // See Optimised Kernels/microarchitecture
  architecture const       = knl

  computational-domain
    dimension const  = 3
    width            = 1.0, 1.0, 1.0
    offset           = 0.0, 0.0, 0.0
    end-time         = 1.0
  end computational-domain

  shared-memory
    identifier       = dummy
    // Set background-tasks and cores to max hardware threads
    configure        = {background-tasks:32,manual-pinning}
    cores            = 32
    properties-file  = sharedmemory.properties
  end shared-memory  
  
  distributed-memory
    identifier               = static_load_balancing
    configure                = {hotspot,fair,ranks-per-node:1}
    buffer-size              = 64000
    timeout                  = 600
  end distributed-memory

  global-optimisation
    fuse-algorithmic-steps                            = on
    fuse-algorithmic-steps-factor                     = 0.99
    spawn-predictor-as-background-thread              = on
    spawn-amr-background-threads                      = on
    disable-vertex-exchange-in-time-steps             = on
    time-step-batch-factor                            = 1.0
    // Turn on if you do not use dynamic limiting
    disable-metadata-exchange-in-batched-time-steps   = on
    double-compression                                = 0.0
    spawn-double-compression-as-background-thread     = on
  end global-optimisation

  solver ADER-DG EulerSolver_ADERDG
    variables const    = rho:1,j:3,E:1
    order const        = 3
    maximum-mesh-size  = 0.05
    time-stepping      = globalfixed
    type const         = nonlinear
    terms const        = flux
    // Use the optimised kernels
    optimisation const = optimised,usestack
    language const     = C
    constants          = reference:entropywave
    
  end solver
end exahype-project
\end{code}