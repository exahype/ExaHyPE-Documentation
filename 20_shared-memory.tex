\chapter{Shared memory parallelisation}
\label{chapter:shared-memory-parallelisation}

\exahype\ currently supports shared memory parallelisation through Intel's
Threading Building Blocks (TBB) and OpenMP. 
We recommend using the TBB variant that is typically one step ahead of the
OpenMP support.
To make an \exahype\ project use multi- and manycore architectures, please add a
\texttt{shared-memory} configuration block to your specification file before
the solvers:

\begin{code}
  shared-memory 
    identifier               = dummy
    cores                    = 4
    configure                = {}
    properties-file          = sharedmemory.properties
  end shared-memory  
\end{code}

\noindent
Rerun the \exahype\ toolkit afterwards, and recompile your code. 
Whenever you configure your project with shared memory support, the
\emph{default} build variant becomes a shared memory build.
Whether it is TBB or OpenMP depends on the environment variable
\texttt{SHAREDMEM}.
Invoke \texttt{make} without arguments for details or study the toolkit's
output. 
You always are able to rebuild without shared memory support or a
different shared memory model just by manually redefining environment variables
and by rerunning the makefile.
There is no need to rerun the toolkit.


Note that all arguments within the \texttt{shared-memory} environment are
read at startup time as none of them is marked with \texttt{const}, i.e.~you can
change them without rerunning the toolkit, too.

\paragraph{TBB} For the compilation, we assume that the environment variables
\texttt{TBB\_INC} and \texttt{TBB\_SHLIB} are set\footnote{
\texttt{TBB\_INC=-I/mypath/include} and
\texttt{TBB\_SHLIB="-L/mypath/lib64/intel64/gcc4.4 -ltbb"} are typical
environment values.
}.
%If you use OpenMP, your compiler has to support OpenMP.
The toolkit checks whether the environment variables are properly set and gives
advise if this is not the case.
\exahype\ sets an appropriate core number internally reading in the
value of \texttt{cores}.
Consult Table \ref{03_shared-memory:parameters} for further details.


\paragraph{OpenMP} For users with an OpenMP background, please note that we do
set the thread count manually within the code.
OpenMP environment variables, notably \texttt{OMP\_NUM\_THREADS}, are neglected.
Instead, \exahype\ sets the core count internally reading in the
value of \texttt{cores}.
Consult Table \ref{03_shared-memory:parameters} for further details.

\vspace{0.4cm}

Using the \texttt{identifier} \texttt{dummy} as displayed above is a reasonable
starting point to assess whether your code is correctly translated and started.
The properties file is neglected for the dummy identifier.
If you use a more sophisticated shared memory strategy through another
identifier, \texttt{properties-file} is important.
More sophisticated strategies are subject of discussion next.
Values within \texttt{configure} are specific to the \texttt{identifier}
selected.
The only generic value you can specify is the number of background tasks to be
used by \exahype: 
Our engine deploys tasks to the background from time to time. 
These are typically low-priority ones.
Any idle thread then can automatically process these tasks.
The number of the background tasks is set via \texttt{background-tasks:x}.
Please consult Table \ref{03_shared-memory:parameters:configure} for further
details.



\exahype's shared memory parallelisation yields reasonable speedups if
and only if your problem is sufficiently compute intense and regular.
If you work with low polynomial order, very coarse meshes or meshes that are
extremely (dynamically) adaptive, the shared memory parallelisation will not
yield a massive speedup.
You might decide to use MPI to exploit cores instead.

\begin{table}[htb]
  \caption{
    Parameter choices for the multicore configuration.
    \label{03_shared-memory:parameters}
  }
\begin{center}
 \begin{tabular}{p{2cm}p{2cm}p{8cm}}
   \toprule
  {\bf Parameter} & {\bf Options}& {\bf Description} \\ 
   \midrule\midrule
    identifier
    &
	 \texttt{dummy} \texttt{autotuning} \texttt{sampling} & 
    Three shared memory parallelisation strategies are
    provided by \exahype\ at the moment. 
    \texttt{dummy} uses some default values that have proven to be reasonably
    robust and yield acceptable speed.
    \texttt{sampling} tests different choices and plots information on
    well-suited variants to the terminal.
    \texttt{autotuning} tries to find the best machine parameter choices
    on-the-fly via a machine learning algorithm. Variants of the autotuning
    strategy are discussed in Table \ref{03_shared-memory:parameters:autotuning}.
    \\
  \midrule
  cores
    &
    $>0$ 
    &
    Number of cores that shall be used. \exahype\ ignores thread/core counts set
    externally (via environment variables, e.g.) and tries to book an
    appropriate number of cores itself. Please consult Table
    \ref{section:20:hybrid} if you combine shared memory runs with MPI.
    \\
  \midrule
  configure
    &
    &
    Some specialised shared memory setups require additional configuration data.
    For plain TBB and OpenMP, an empty clause does the job.
    \\
  \midrule
    properties-file
    & 
    filename
    & 
    The \texttt{sampling} and the \texttt{autotuning} strategy can write their
    results into files and reuse these results in follow-up runs. If no
    (valid) file is provided, they both start from scratch and
    without any history. If the file name is empty or invalid, no output data is dumped.
    The entry is ignored if you use the \texttt{dummy}
    strategy. 
    \\
   \bottomrule
 \end{tabular}
\end{center}
\end{table}



\begin{table}[htb]
  \caption{
    Variants of shared memory autotuning.
    \label{03_shared-memory:parameters:autotuning}
  }
\begin{center}
 \begin{tabular}{p{1.5cm}p{11cm}}
   \toprule
  {\bf Options}& {\bf Description} \\ 
  \midrule\midrule
	 \multicolumn{2}{l}{\texttt{autotuning}} \\ &
    The basic variant
    \texttt{autotuning} tries to find the best machine parameter choices
    on-the-fly. When your code terminates, the found configuration is piped into
    the properties file. The next time you run your code, the autotuning does
    not start its search from scratch. Instead, it uses the state documented by
    the file. If no file exists, it starts from scratch.
    
    Part of the autotuning strategy is the opportunity to restart the search
    from time to time, i.e.~if the strategy thinks it has found a reasonable
    configuation for some parameter over quite some time, it randomly decides to
    try out a few other options nevertheless. This way, we try to avoid that the
    machine learning runs into local minima. 
    \\
  \midrule
	 \multicolumn{2}{l}{\texttt{autotuning\_without\_learning}} \\ &
	This variant reads in the properties file of the autotuning, but it does not
	continue to learn. The variant is reasonable for production runs where you
	already have found a valid shared memory configuration. If no file is found,
	the strategy however has to switch off multicore parallelisation.
	\\
  \midrule
	 \multicolumn{2}{l}{\texttt{autotuning\_without\_restarts}} \\ &
    Variant of the autotuning that does never restart any search, i.e.,~once a
    working parameter configuration is found it is kept. As all searches
    successively are shut down, the machine learning switches itself gradually
    off. As the timings underlying the machine learning are expensive OS calls
    themselves, this might be reasonable but bear in mind that you might find
    good parameter combinations that represent a local minimum and are not the
    globally optimal parameter choice.
    \\
   \bottomrule
 \end{tabular}
\end{center}
\end{table}


\begin{table}[htb]
  \caption{
    Supported flags that can be used in the \texttt{configure} section.
    \label{03_shared-memory:parameters:configure}
  }
\begin{center}
 \begin{tabular}{p{1.5cm}p{11cm}}
   \toprule
  {\bf Options}& {\bf Description} \\ 
  \midrule\midrule
	 \multicolumn{2}{l}{\texttt{background-tasks:x}} \\ &
	 \exahype\ uses background tasks in several places to deploy long-lasting, low
	 priority jobs to free cores. By default, the number of these background tasks
	 is restricted. This avoids that the system is swamped with too many of them
	 and the ``main'' code starves. You can alter this value. By default it is 1.
	 We notably encourage users to play around with this quantity if they have many
	 cores.
    \\
  \midrule
	 \multicolumn{2}{l}{
	  \texttt{no-invade}
	  |
	  \texttt{invade-between-time-steps}
	  |
	  \texttt{invade-throughout-computation}
	 } \\
	 & |
	  \texttt{invade-at-time-step-startup-plus-throughout-computation} \\
	 &
	 Can be used if you compile with \texttt{TBBInvade} to control the invasion
	level. 
	Only taken into account if you compile with the invasive TBB extension
	(cmp.~Section \ref{section:invasive-tbb}).
	\\
  \midrule
  \multicolumn{2}{l}{\texttt{manual-pinning}} \\ &
  Can be used if you compiled with \texttt{TBB}. Enforces a strict one to one thread to cpu affinity map.
  This setting can decrease the applications run-time as cache-efficiency is
  increased. If the flag is present, \exahype\ also reports on the exact
  pinning via terminal outputs. This is useful to validate whether the job
  scheduler assigns the right cores---even if you rely on the scheduler to
  take care of all pinning. \\
	\\
  \midrule
  \multicolumn{2}{l}{\texttt{thread-stack-size:X}} \\ &
  Can be used if you compiled with \texttt{TBB}.  Ensures that each thread at
  least uses a certain stack size. Consult
  \url{https://www.threadingbuildingblocks.org/docs/help/reference/appendices/preview_features/tbb_global_control.html}
  for details.\\
   \bottomrule
 \end{tabular}
\end{center}
\end{table}



\section{Tailoring the shared memory configuration}

Shared memory performance can be very sensitive to machine-specific tuning
parameters:
Peano, \exahype's AMR code base, relies on static problem subpartitioning and
needs precise instructions which problem sizes for particular parts of the
code are convenient.
We thus encourage high performance applications to tailor
\exahype's shared memory parallelisation to get the most out of their machine.

Key ingredient to do so is the usage of a different parallelisation strategy
than our \texttt{dummy} plus the configuration/properties files read by these
strategies.
Parallelisation strategies are selected through the \texttt{identifier} option.

\begin{design}
All non-dummy strategies load tuning parameters from the
properties file. We keep this file separate from the \exahype\ specification 
file as some shared memory strategies automatically update/autotune the settings therein, i.e.~the file is 
altered by the code.
\end{design}

\noindent
Through a dump of information, knowledge can be reused in the next run,
runtime characteristics can be analysed, or a knowledge base can be built up
over multiple simulation runs.


It is obvious that proper property files depend on the machine you are using.
They reflect your computer's properties.
Yet, note that very good property files depend on the 
\begin{itemize}
  \item choice of cores to be used,
  \item MPI usage and balancing,
  \item application type, and even
  \item input data/simulation scenario.
\end{itemize}
It thus might make sense to work with various property files for your
experiments.


\subsection{Autotuning}
Our most convenient shared-memory tailoring relies on the \texttt{autotuning}
strategy.
Autotuning starts with serial configurations for all program parts.
Once it has obtained a reasonable accurate estimate of how long each program
part runs, it tries to deploy various parts of the code among multiple cores.
We typically start with two cores, four cores, and so forth unless subproblems
consist of large arrays that immediately can be broken up into more chunks.
If a problem decomposition improves the runtime, the oracle continues to break
it up into even smaller chunks to exploit more cores until the decomposition
either does not improve the runtime anymore or even makes the performance worse.
In this case, we roll back to the last reasonable configuration.
This means that first runs might be really slow, but the runtime improves
throughout the development.


The autotuning switches off per code segment automatically as soon as it has to
believe that best-case parameters are found.
As a result, timing overheads are reduced.
As any configuration might correspond to a local runtime minimum, the
autotuning restarts the search from time to time.
This restart is randomised.
The whole learning process is documented via terminal outputs. 
It might be reasonable to disable them via a log filter entry (cmp.~Chapter
\ref{chapter:output:logging}).


Peano provides a Python script to translate all measurements into a big HTML
table.
The script is located within \texttt{Peano/sharedmemoryoracles}.
If you invoke it without parameters, you obtain a detailed usage message.


If a properties file does exist at startup already, \exahype\ loads this propery
file, i.e.,~the autotuning starts from the previous properties dump.
This way, long-term learning can be split among various program
invocations.



\begin{itemize}
  \item The autotuning requires your OS to offer real-time timers. We have
  encountered various situations, notably on Intel KNL, where timers seem not to
  work properly. In this case, the autotuning works if and only if a properties
  file from another machine is handed in, i.e.,~a configuration from a different
  machine is used. Please note that disfunctional autotuning might mess up your
  performance as \exahype\ is forced to switch off shared memory
  parallelisation if the properties file is malformed.
  \item Large simulation runs seem to yield runtime data that varies strongly.
  It thus requires the autotuning to measure quite some time before code regions
  are identified that scale. We recommend to run autotuning first on small
  problem setups. The dumped properties files then can be reused by larger runs.
  If \exahype\ is passed an autotuning configuration from a small run, it
  extrapolates measurements therein as initial data for the autotuning and then
  continues to optimise further.
  \item As the autotuning reads text configuration files,
  it is possible to configure the autotuning manually.
  The format of the properties file is documented as comment within the source
  code.
\end{itemize}


\subsection{Configuration sampling and manual tuning}

To get the whole picture which code fragments perform in which way on your
machine, you might want to run the \texttt{sampling} strategy.
However, the parameter space is huge, i.e.~getting a valid picture might require
several days.
The output of the sampling is written into the specified properties file and
then allows you to identify global best case parameters.


Peano provides a Python script to translate all measurements into graphs. 
The script is located within \texttt{Peano/sharedmemoryoracles}.
If you invoke it without parameters, you obtain a detailed usage message.


If a properties file does exist at startup already, \exahype\ loads this propery
file, i.e.,~the statistics are incremently improved.
This way, a long-term statistical analysis can be split among various program
invocations.



\section{Hybrid parallelisation}
\label{section:20:hybrid}

If you want to combine shared memory parallelisation with MPI, nothing has to be
done in most cases.
However, the tailoring of a reasonable number of ranks per node plus a efficient
number of threads per rank can be tedious. 
You cannot expect that a hybrid code shows the same shared memory speedup
as a code that is parallelised with shared memory only.


As a rule of thumb, it makes sense first to choose a core count that anticipates
the number of MPI ranks you deploy per node. 
That is, if you have $c$ cores per node and you launch $p<c$ MPI ranks per node,
it makes sense to set \texttt{cores} to $c/p$.
Please note that some scheduling systems require you to tell the scheduler
(Slurm, e.g.) as well if you wanna use more than one thread per MPI rank.


\exahype\ supports multithreaded MPI and most MPI implementations nowadays
support multithreaded calls.
However, we found that most applications do not benefit from hyperthreaded MPI
or even are slowed down.
It thus is disabled by default in \exahype. 
If you want to use hyperthreaded MPI in your code, please comment the line

\begin{code}
PROJECT_CFLAGS+=-DnoMultipleThreadsMayTriggerMPICalls
\end{code}

\noindent
in your autogenerated makefile out.


If you use autotuning with MPI on $p$ ranks, please note that \exahype\ disables
the learning on all ranks besides rank $p-1$.
While rank $p-1$ reads in the properties file and tries to improve parameters
specified therein, all remaining ranks do read the properties file, too.
They however do not alter the file's settings.


Once your code terminates, rank $p-1$ dumps any (improved) parameter choice.
If you use grain size sampling, rank $p-1$ dumps its statistics. 
The statistics from all other ranks are not dumped persistently.
If your rank $p-1$ successively improves the setting in the properties file,
better parameter findings automatically will be available to the other ranks in
the next program run. 



\section{Invasive TBB}
\label{section:invasive-tbb}

Invasive TBB is a concept to weaken the fixed association of cores to MPI ranks:
The ranks with most of the load per node grab more cores than the other ranks.
To enable this feature, \exahype\ requires you to rebuild the code and to
re-configure your scheduler scripts. 
The standard settings of SLURM, e.g., do not allow MPI ranks to shuffle around
cores---each rank has a fixed number of cores to administer over the whole
runtime, and typically this is the same number of cores per rank.


\subsection{Use InvasiveTBB version shipped with ExaHyPE}

We ship a version of our invasive TBB component with \exahype\ as the majority
of the development on the second generation of the invasive TBB routines has been
done within \exahype.
To use this extension, first change \exahype's build mode into 
\begin{code}
export MODE=TBBInvade
\end{code}

\noindent
Furthermore, please extend you pathes for the shared memory oracles \linebreak
(\texttt{PEANO\_TOOLBOX\_SHAREDMEMORY\_ORACLES\_PATH}) to include the
\texttt{shminvade} directory, too. 

\begin{code}
PEANO_TOOLBOX_SHAREDMEMORY_ORACLES_PATH=mypath/ExaHyPE-Engine/Peano/\
  sharedmemoryoracles:mypath/ExaHyPE-Engine/Peano/shminvade
\end{code}


 
\subsection{Use SHMInvade 1.0}

\begin{remark}
  This section refers to the first version of SHMInvade and thus might not be
  up-to-date anymore.
\end{remark}

Get the Invasive TBB from \url{bitbucket.org} through git.
It comes along as a header-only distribution.
Change \exahype's build mode into 
\begin{code}
export MODE=TBBInvade
\end{code}

\noindent
A build now probably fails due to lacking header search directories. We recommend to extend \texttt{TBB\_INC} 
such that they link to the correct invasive libraries, too. Furthermore, we
found that most Linux distributions require us to explicitly include
\texttt{pthread}:
\begin{code}
export TBB_INC=$TBB_INC" -I/directory-holding-shminvade"
export TBB_SHLIB=$TBB_SHLIB" -lpthread"
\end{code}


\subsection{Configure SLURM}



