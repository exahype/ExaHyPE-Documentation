\chapter{Shallow Water Equations with ADER-DG}
\label{chapter:swe}

In this chapter, we quickly detail how to write a Shallow Water solver with
ADER-DG. It is very hands on, application-focused and brief, so it might be
reasonable to study Chapter \ref{chapter:aderdg-user-defined-fluxes} first.
Different to Chapter \ref{chapter:aderdg-user-defined-fluxes} we focus on a few
particular challenges tied to the shallow water equations:

\begin{enumerate}
  \item How we quickly write down plotter to visualise the water height though
  we are given the bathymetry and water depth in the solver only.
  \item How to handle bathymetry, i.e.~material parameters, elegantly within
  \exahype's ADER-DG framework.
\end{enumerate}

\noindent
Our shallow water equations are written down as

\begin{equation}
\frac{\partial}{\partial t} 
\begin{pmatrix}
h\\hu\\hv\\ b
\end{pmatrix}
+
\frac{\partial}{\partial x}
\begin{pmatrix}
hu\\
hu^2 + 0.5gh^2\\
huv\\
0\\
\end{pmatrix}
+
\frac{\partial}{\partial y}
\begin{pmatrix}
hv\\
huv\\
hv^2 + 0.5gh^2\\
0\\
\end{pmatrix}
+
\begin{pmatrix}
0\\
hg \cdot b_x\\
hg \cdot b_y\\
0\\
\end{pmatrix}
= 0.
\label{eq:swe_plain}
\end{equation}


\noindent
$h$ denotes the height of the water column, $(u,v)$ the horizontal flow velocity, $g$ the gravity and $b$ the bathymetry. 
The subscripts $x,y$ symbolize partial differentiation. 
Following \exahype's philosophy, distinguish our unknown vector $(h,hu,hv)$ from
the material parameter $b$, but we summarise them where appropriate as one
vector or quantities of interest $\mathbf{Q} = (h,hu,hv,b)$.

In the first part of this discussion, we neglect the bathymetry. 
A separate subsection is dedicated to this material parameter.
As the bathymetry does not directly have an impact on the solution---it is the
gradient that has an impact---this means we assume a constant sub-ocean profile. 


\section{Setting up the experiment}

Fluxes and non-conservative products will be required and therefore the solver's kernel is \texttt{generic::fluxes::ncp::nonlinear}.
The specification for this example is close to trivial

\begin{code}
exahype-project  SWE

  peano-kernel-path const = ./Peano
  exahype-path const      = ./ExaHyPE
  output-directory const  = ./ApplicationExamples/SWE_ADERDG
  architecture const      = noarch
  log-file                = mylogfile.log

  computational-domain
    dimension const          = 2
    width                    = 10.0, 10.0
    offset                   = 0.0, 0.0
    end-time                 = 5.0
  end computational-domain

  solver ADER-DG MySWESolver
    variables const   = h:1,hu:1,hv:1
    parameters const  = b:1
    order const       = 3
    maximum-mesh-size = 0.15
    time-stepping     = global
    type const         = nonlinear
    terms const        = flux,ncp
    optimisation const = generic
    language const    = C
  end solver
end exahype-project
\end{code}

\noindent
and we set some initial data as we adjust our solution manually and point-wisely
if the simulation time equals $t=0$:
\begin{code}
bool SWE::MySWESolver::useAdjustSolution(...,const double t) const { 
 return tarch::la::equals(t,0.0); 
}

void SWE::MySWESolver::adjustSolution(const double* const x,...,
  const double t,...,double* Q) { 
  assertion(tarch::la::equals(t, 0.0); 
  initialData(x,Q);
}
\end{code}

\noindent
We may add this \texttt{initialData} as function to \texttt{MySWESolver.h} or we
may have it separately; it does not really matter.
Before we continue, we open \texttt{MySWESolver.h} and ensure that
\begin{code}
  bool useAdjustSolution(...) const override;
\end{code}

\noindent
is already marked there as \texttt{override}. By default, \exahype\ does not
adjust any solution and the \texttt{AbstractSWESolver} indeed will by default
return \texttt{false}. 
We explicitly have to switch this ``feature'' on. 
This is a pattern that will arise later for the bathymetry again.


\section{Writing out the data}

While we could plot out $\mathbf{Q}$ as it is, it is convenient, for the shallow
water equations, to see the water height rather than the entries in
$\mathbf{Q}$. 
We also want to get the bathymetry (though this is kind of an overhead as it
does not change in time).
The water height is the sum of the bathymetry plus $h$.
We therefore make the plotter give us two more unknowns than we actually have in
the specification file:

\begin{code}
plot vtu::Cartesian::cells::ascii ConservedWriter
  variables const = 5
  time            = 0.0
  repeat          = 0.1
  output          = ./conserved
  select          = x:0.0,y:0.0
end plot
\end{code}

\noindent
A rerun of the \exahype\ toolkit yields a writer class, and we modify one
function therein:
\begin{code}
void SWE::ConservedWriter::mapQuantities(...) {
  for (int i=0; i<4; i++){
    outputQuantities[i] = Q[i];
  }
  outputQuantities[4] = Q[3] + Q[0];
}
\end{code}

\noindent
This snippet uses two properties of \exahype:
\begin{enumerate}
  \item The material parameters in terms of data are just modelled as additional
  quantities attached to the unknown vector (this is one of the reasons why we
  have defined $\mathbf{Q}$ as above). We exploit this factor here by accessing
  \texttt{Q[3]} which otherwise would not make any sense.
  \item We have told the toolkit that we want to write out five quantities, so
  it already prepares for us a reasonably large array \texttt{outputQuantities}.
\end{enumerate}

\section{The (non-bathymetric) fluxes}

The realisation of the eigenvales and the fluxes here is straightforward once we
note that we basically just set all contributions from the bathymetry to zero:
\begin{code}
void SWE::MySWESolver::eigenvalues(const double* const Q,
  const int normalNonZeroIndex,double* lambda) {
  // Dimensions             = 2
  // Number of variables    = 3 (#unknowns + #parameters)
  ReadOnlyVariables vars(Q);
  Variables eigs(lambda);  

  const double c= std::sqrt(grav*vars.h());
  const double ih = 1./vars.h();

  double u_n = Q[normalNonZeroIndex + 1] * ih;

  eigs.h() = u_n + c ;
  eigs.hu()= u_n -c;
  eigs.hv()= u_n ;
}


void SWE::MySWESolver::flux(const double* const Q,double** F) {
  ReadOnlyVariables vars(Q);

  const double ih = 1./vars.h();

  double* f = F[0];
  double* g = F[1];

  f[0] = vars.hu();
  f[1] = vars.hu()*vars.hu()*ih + 0.5*grav*vars.h()*vars.h();
  f[2] = vars.hu()*vars.hv()*ih;

  g[0] = vars.hv();
  g[1] = vars.hu()*vars.hv()*ih;
  g[2] = vars.hv()*vars.hv()*ih + 0.5*grav*vars.h()*vars.h();
}
\end{code}


\section{Injecting the bathymetry}

So far, we did assume $\nabla b=0$ and thus neglected any subocean 
variations.
To get in the bathymetry, we follow the following \exahype\ conventions:
\begin{itemize}
  \item The bathymetry $b$ is subject to the trivial PDE $\partial _t b = 0$.
  \item We can thus rewrite (\ref{eq:swe_plain}) as a PDE over four unknowns.
  \item Following exactly the notation from (\ref{eq:swe_plain}), the bathymetry
  gradient then enters the PDE as additional derivative entry. This
  follows the notion of path-conservative integration, but yields
  non-conservative terms. We thus have to switch on \exahype's non-conservative
  formulation explicitly.
\end{itemize}

\begin{design}
  Material parameters in \exahype\ are simply appended to the unknowns. From a
  data structure point of view, it does not make a difference whether we write
  \begin{code}
variables const   = h:1,hu:1,hv:1
parameters const  = b:1
  \end{code}
  or
  \begin{code}
variables const   = h:1,hu:1,hv:1,b:1
  \end{code}
  in the present application. \exahype's implementation however ensures that
  parameters are never updated even though some Riemann schemes might come up
  with updates to the ``parameter PDE'' $\partial _t b = 0$.
  If you require time-dependent material parameters, we recommend to plug into
  \texttt{adjustSolution} to set the corresponding ``solution'' entries for
  particular time steps.
\end{design}

\noindent
We formalise our approach by rewriting (\ref{eq:swe_plain}) into
\begin{equation}
\frac{\partial}{\partial t} \textbf{Q}
+
\frac{\partial}{\partial x}
\textbf{F}(\textbf{Q})
+
\frac{\partial}{\partial y}
\textbf{G}(\textbf{Q})
+
\boldsymbol B_1(\boldsymbol Q ) \frac{\partial \boldsymbol Q}{\partial x}
+
\boldsymbol B_2(\boldsymbol Q ) \frac{\partial \boldsymbol Q}{\partial y}
= 0
\label{eq:nc_form}
\end{equation}
where 
\begin{equation*}
\textbf{Q} = \begin{pmatrix}
h\\hu\\hv\\ b
\end{pmatrix}
\quad \mbox{and} \quad
\textbf{F} = 
\begin{pmatrix}
hu\\
hu^2 + 0.5gh^2\\
huv\\
0\\
\end{pmatrix}
\quad \mbox{and} \quad
\textbf{G} = 
\begin{pmatrix}
hv\\
huv\\
hv^2 + 0.5gh^2\\
0\\
\end{pmatrix}.
\end{equation*}

\noindent
$h$ denotes the height of the water column, $(u,v)$ the horizontal flow velocity, $g$ the gravity and $b$ the bathymetry. The subscripts $x,y$ symbolize partial differentiation. By defining
\begin{equation*}
\textbf{B}_1:= \begin{pmatrix}
0 & 0 & 0 & 0 \\\
0 & 0 & 0 & hg \\\
0 & 0 & 0 & 0 \\\
0 & 0 & 0 & 0 
\end{pmatrix}
\qquad
\mbox{and}
\qquad
\textbf{B}_2:= \begin{pmatrix}
0 & 0 & 0 & 0 \\\
0 & 0 & 0 & 0 \\\
0 & 0 & 0 & hg \\\
0 & 0 & 0 & 0
\end{pmatrix},
\end{equation*}
the bathymetry gradient is taken into account through the non-conservative products 
$\textbf{B}_1 \frac{\partial \textbf{Q}}{\partial x}$ and $\textbf{B}_2
\frac{\partial \textbf{Q}}{\partial y}$.
This way of mapping the equations into \exahype\ allows for a well-balanced numerical solver and automatic differentiation of $b$. 
%In theory, \exahype\ allows us to redefine the scalar product in the 
%non-conservative part of (\ref{eq:nc_form}) with any scalar product we like. 
%The text therefore later uses the symbol $\otimes  $ there. 
% \todo[inline]{The product 
% $(B_1,B_2,\ldots,B^d)\otimes\nabla Q := \sum_i B_i\,\frac{\partial Q}{\partial
% x_i}$ is not a scalar product since the result has the same cardinality as Q.}
%In the present example, we stick to standard scalar products and realise the 

We realise the non-conservative product:

\begin{enumerate}
  \item As we informed \exahype\ that we wanted to use the non-conservative
    products, the header should already offer a realisation of those routines:
    \begin{code}
void nonConservativeProduct(const double* const Q,
  const double* const gradQ,double* BgradQ) override; 
  
void coefficientMatrix(const double* const Q,
  const int d,double* Bn) override;
    \end{code} 
  \item We realise the non conservative product:
\begin{code}
void SWE::MySWESolver::nonConservativeProduct(const double* const Q,
  const double* const gradQ,double* BgradQ) {
     idx2 idx_gradQ(DIMENSIONS, NumberOfVariables+NumberOfParameters);
     BgradQ[0]=0.0;
     BgradQ[1]=g*Q[0]*gradQ[idx_gradQ(0,3)];
     BgradQ[2]=g*Q[0]*gradQ[idx_gradQ(1,3)];
     BgradQ[3]=0.0;
}
\end{code}
     In the above code snippet, we used the \texttt{idx2} struct in
     \texttt{kernels/KernelUtils.h} to access the derivatives. 
   \item The matrices $\textbf{B}_1$ and $\textbf{B}_2$ are finally
   parameterised over the normal direction and can be implemented as follows:
\begin{code}
void SWE::MySWESolver::coefficientMatrix(const double* const Q, 
  const int d, double* Bn) {
  idx2 idx_Bn(NumberOfVariables+NumberOfParameters,
    NumberOfVariables+NumberOfParameters);

  Bn[0] = 0.0;
  Bn[1] = 0.0;
   ...
  Bn[15]= 0.0;    

  Bn[idx_Bn(3,d+1)]=g*Q[0]; //g*h
}
\end{code}

     \noindent
     It is important to note that the solver expects the matrix to be in Fortran
     storage order. To abstract from this fact, we use \exahype's
     \texttt{idx\_Bn} array. Alternatively, you can access the data directly.
     Again, the above snippet exploits that material parameter physically are
     treated as additional unknowns attached to the original unknown vector.
\end{enumerate}


\begin{center}
  \includegraphics[width=0.45\textwidth]{screenshots/11_swe00.png}
  \includegraphics[width=0.45\textwidth]{screenshots/11_swe01.png}
  \\
  \includegraphics[width=0.45\textwidth]{screenshots/11_swe03.png}
  \includegraphics[width=0.45\textwidth]{screenshots/11_swe05.png}
  {
  \\
  \footnotesize
    Left: A ocean with a plain subsurface hosting a circular plateau is covered
    by constant constant water height, i.e.~the water mirrors the subocean
    topology. To the right: Once we ``release'' this initial condition, we see
    waves spreading.
  }
\end{center}

\section{Transcribing the algorithm into Finite Volumes}

The finite volumes realisation of the shallow water equations follows 
the workflow sketched in this chapter. 
There are subtle differences:
\begin{itemize}
  \item The non-conservative product is not required for our 1st order Godunov
  schemes. The corresponding routine has to be removed.
  \item In the routine \texttt{boundaryValues} are no fluxes, so we can
  eliminate any flux sets along the boundary. 
\end{itemize}
\begin{info}
The current Rusanov solver is not well-balanced which results in spurious waves for some scenarios. A simple fix to make it well-balanced for the SWE adds $0.5 * s_{max} *(b_L - b_R)$ to the first component of the Rusanov flux.
\end{info}
