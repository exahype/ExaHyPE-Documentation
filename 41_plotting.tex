\chapter{Plotting}
\label{chapter:plotting}

In this Chapter, we start with an overview over various output formats supported
by \exahype.
In a second part, we discuss how to pass selections over to the simulation
engine.
Often, you are not interested in the whole domain or all variables, so notifying
the code base about the regions of interests allows the code to stream out only
the data actually required which makes it faster.
We wrap up with some remarks how to modify/postprocess variables on-the-fly
before they are written.
An all time classic of such an in-situ postprocessing is the conversion of
conservative into primitive variables of the elimination of helper variables
from the output.

\begin{design}
\exahype's IO is per se very simplistic as we do ackwnowledge that most
applications have very application-specific plotting demands. We thus decided on
purpose that we support only very few output formats, do not support
sophisticated plotting (such as symbolic names onto output fields) or complex
filtering.
However, we do offer the infrastructure to realise sophisticated plots on the
application side that do scale up.
\end{design}


Most output formats of \exahype\ are standardised output formats such as VTK.
There are a number of Open-Source tools available to interactively watch,
inspect and render these files. 
Popular choices are \emph{ParaView} and
\emph{Visit}.
These programs are available for many operating systems and also typically present on vizualization nodes in scientific clusters.

\section{A short introduction of plotting paradigms}\label{sec:plot:cell-structure}

Due to the complex subcell structure in ExaHyPE (for
details, see chapter \ref{chapter:cell-structure}), there
are many design decisions when it comes to plotting the
degrees of freedom of a simulation. Some of these decisions
are classical for any fluid dynamics code and shall reviewed
here briefly.

\subsection{Exact representations of degrees of freedom}
\begin{description}
\item [ADERDGSolver]
 The subgrid are the polynomial supporting
points living on the Legrende basis points. In general, an $p$ order ADERDG
method has $p+1$ supporting points (vertices) on one axis. For instance, for
a second order DG approximation in 1D, an exact output (in terms of a dump of
the simulation) contains three data points per cell and thus holds three times
more data than with a cell-averaging output method. As long as these $p+1$
points are represented in an output file format, one is able to exactly
reconstruct the polynomials in a postprocessing step.
%
\item [FVSolver] 
In case of the Finite Volume kernels, in ExaHyPE they are always realized with
regular block based grids embedded into cells/patches. The size of these blocks
can be controlled via the specification file (parameter \texttt{basis-size}).
For a basis size $N$, there are $N$ (cell centered) points on one axis. As a
matter of principle, the number of points is independent from the
(convergence/computational) order of the finite volume scheme working on this
grid. Anyway the basis size is one of the primarily visible things in any
ExaHyPE output format. Note that in the Finite Volume schemes the plotters we
provide in ExaHyPE \emph{never} print ghost zones.
%
\item [LimitingADERDGSolver]
The same as ADERDGSolver.
\end{description}

\subsection{Cell-centered and nodal values: Native solver representation}
Many file formats support the representation of physical
fields both cell-centered and vertex-centered (or nodal).
The concept is briefly sketched in figure
\ref{fig:node-and-vertex-centered}. The following overview
lists the way how this concept goes along with specific
solvers:

\begin{figure}
\includegraphics[width=\textwidth]{sketches/node-and-vertex-centered.pdf}
\caption[Sketch (Inkscape): Node and vertex centered in 1D, 2D, 3D.]{Storage location of data in node- vs cell-centered formats in 1D, 2D and 3D. The cell-centered location is similarly determined for non-cubic cells. Adopted from \cite{TecplotFileFormat}.}\label{fig:node-and-vertex-centered}
\end{figure}

\begin{description}
\item [ADERDGSolver] This is the most versatile solver in terms of plotting as it
  allows an easy evaluation (sampling) of polynomials on any basis. The internal
  representation (``where the data live'') is the \emph{nodal Gauss-Legendre} basis.
  Thus, the \texttt{Legendre::vertices} plotters display the degrees of freedom as
  they are used in the simulation without any change. In any other representation,
  there is at least an intrinsic error of the evaluation ($\sim$ polynomial order).
  When plotting DG solutions cell-centered, it is only a matter of a small position
  shift but potentially a different meaning for the output data.
%
\item [FVSolver] The internal representation of the Finite Volume solver data
  is the regular \emph{cell-centered Cartesian} basis. Thus, you only obtain the
  real unchanged data when using a \texttt{Cartesian::cells} combination. Indeed,
  for the VTK  plotters, we (currently) don't even implement a different combination,
  ie. we don't do interpolation\footnote{Note that the CarpetHDF5 plotter allows you
  to interpolate  Finite Volume Solver data also vertex-centered. This could be easily
  extended to cover VTK plotters, too}.

\item [LimitingADERDGSolver] The limiting ADER-DG scheme (as described in chapter
  \ref{chapter:coupling-limiter}) always holds the DG degrees of freedom when it
  comes to plotting. Therefore any statement about the ADERDGSolver also applies for
  the LimitingADERDGSolver.
  \\
  However, it is also possible to export the further information about a cell hold
  by the LimitingADERDGSolver. For instance, one can think of plotting the subcell
  structure in a clearly troubled cell. This operation is not neccessarily
  straightforward as one could with a similar argument plot also the subcell
  structure in the neighboured and next-to-neighboured cells. Thus, as a limitation,
  we currently don't have any (VTK) plotter in ExaHyPE plotting the subcell structure.
  \\
  In contrast, we do have the \texttt{vtk::Cartesian::vertices::limited::...}
  plotters which are supported only for the LimitingADERDGSolver.
\end{description}

\subsection{Subcell structure: Discontinuities and ghost cells}
%
\begin{figure}
\includegraphics[width=0.50\textwidth]{figures/fileformat-sketch-cont.pdf}
\includegraphics[width=0.50\textwidth]{figures/fileformat-sketch-discont.pdf}
\caption[1D plotting output sketch, highlighting DG jumps]{A sketch of four 1D cells holding a polynomial of degree $p=2$, supported by $N=3$ nodal points in an equidistand basis (which could be a Gauss-Lobotto basis). Since the nodal points are on the cell boundary, there are data doublings (multiplicities) which only stand out visually when there are discontinuities (right panel). The output is a double-valued function. The gray bars indicate the cell average.}\label{fig:fileformat-sketch}
\end{figure}
%
\begin{design}\label{testA}
Plotting shall represent the degrees of freedom in a
simulation. In this spirit, we \emph{never} plot ghost zones in
a FV scheme. However, we \emph{always} plot double values in
an DG scheme.
\end{design}

All kernels in ExaHyPE have their own cell description where there is a certain
overlap of information with neighbouring cells. When it comes to plotting, one
has to choose how redundancy is handled. Principle \ref{testA} applies. In
detail, the following rules apply:

\begin{description}
\item [ADERDGSolver]
In a nodal representation, in particular bases nodes lay on the cell boundary and
thus the discontinous nature of the DG scheme is exposed by the plotter. In
continous (``shock-free'') fields, this introduces redundancy, i.e. the
same values $\Phi(\vec x)$ seem to occur multiple times in the plot. However, as
soon as the field values are discontinous along multiple cells, these points
provide additional information, cf. figure \ref{fig:fileformat-sketch}. In any
case, in a recovarable plot, the discontinuity can be recovered by computing the
polynomial from the plot data.
%
\item [FVSolver] 
Finite volume schemes always have \emph{ghost layers} which are however usually
never plotted in ExaHyPE. If the DOF are plotted cell-centered, they never touch
the cell boundary. Note that any nodal presentation either offsets the data
(staggering), introduces data doubling or interpolates into the ghost layer.
In such a nodal plot, figure \ref{fig:fileformat-sketch} applies again.
%
\item [LimitingADERDGSolver]
The same comments as for the ADERDGSolver apply.
\end{description}

While a visualization software might use available ADER-DG data (in combination
with knowledge of the polynomial order) in order to reconstruct the polynomial
we don't expect this to happen in every-day visualisations. Common visualization
software (such as ParaView, Visit, Amira) only does linear interpolation between
data points at all and users prefer even to turn off this in favour of an
neirest-neighbour visualization. Depending on the file formats, it may be also
difficult to extract the neccessary information for high order visualization,
for instance in the VTK file formats it had to be reconstructed.

\section{Overview of supported data output formats}
\label{sec:plotting-supported-output-formats}

This section lists all available plotter types. They are
distinguished by their identifier in the specification file.
Note that due to limitations of the glue code builder, the number and order
of the plotters have to be fixed at toolkit time. You can read them off by calling your compiled application with the
argument \texttt{--version} and you should obtain an
output similar to
\begin{code}
> ./ExaHyPE-YourApplication --version
....
Toolkit static registry info
============================
projectName: YourApplication
.....
Kernel[0].Plotter[0]: YourApplication::ConservedWriter(variables=19)
Kernel[0].Plotter[1]: YourApplication::ConservedWriter(variables=19)
Kernel[0].Plotter[2]: YourApplication::SphereIntegrals(variables=0)
Kernel[0].Plotter[3]: YourApplication::IntegralsWriter(variables=0)
...
\end{code}
This example corresponds to a specification file where a solver is equipped with
at least four plotters where the first two use the same mapping class (``ConservedWriter'')
while the third and fourth use a different one, cf. section \ref{sec:parameter-selection-and-conversion}
for further details.

If you want to change the number of plotters or their type, ie. if you want
to introduce a new plotter, you have to rerun the  \exahype\ toolkit and rebuild
your code---all other parameters are read at runtime. 
A plotter always has to specify how many variables are written through the
\texttt{unknowns} statement. 
By default, these are the first \texttt{unknowns} unknowns from your solver. 
If \texttt{unknowns} is greater than the unknowns of the solver, you also obtain
the material parameters if there are material parameters. Otherwise you do not
obtain useful data but could map your unknowns to locally derived quantities.

\begin{info}
Plotters in ExaHyPE have an identifier which reads like
\texttt{featureA::featureB::featureC::}\dots. The identifier
is not case sensitive.
\end{info}

\subsection{Volumetric plotters with generic file formats (VTK)}
All volumetric plotters are by construction able to plot the full
simulation domain. The VTK file format is adopted as one of the
most widespread file formats. For details about the file format,
we refer to appendix \ref{section:VTK-appendix}.

As a first rule of thumb, ExaHyPE currently supports the following combinations
of plotter parameters:
 \begin{equation*}
 \left\{
 \begin{matrix}
 \texttt{vtk} \\
 \texttt{vtu}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Legendre} \\
 \texttt{Cartesian}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Vertices} \\
 \texttt{Cells}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Ascii} \\
 \texttt{Binary}
 \end{matrix}
 \right\}
 \end{equation*}
 
\begin{figure}
\begin{center}
\includegraphics[width=0.40\textwidth]{screenshots/cartesian-box.png}%
\hspace{3em}%
\includegraphics[width=0.40\textwidth]{screenshots/legendre-box.png}
\end{center}
\caption[Screenshot (Paraview): 3D Cartesian vs. Legendre plotting]{Cartesian vs. Legendre volumetric plotters: Both cubes show a one-time refined cube with $p=$3 ADERDG subgrid, left equally spaced interpolated, right on the Legendre points.}\label{fig:volumetric-boxes}
\end{figure}

%\noindent
Figures \ref{fig:volumetric-boxes} and \ref{fig:volumetric-meshref} visualize the difference between Cartesian and Legendre plotters. Figure \ref{fig:cells-vs-certices} visualizes the difference
between Vertices and Cells sampling.
The variant \texttt{vtk} is a legacy format supported by most postprocessing
tools.
\texttt{vtu} is a newer format wich can represent the same data, but
encoded as XML and with substantially improved handling in case MPI is
used.
If you use \texttt{vtu}, \exahype\ does generate meta data formats such as
\texttt{pvtu} (only with MPI) or \texttt{pvd} for the time series that allow you
to load a whole set of \texttt{vtk} files in one rush.
The following sections refer only to the prefix \texttt{vtk}. 
You may always replace \texttt{vtk} with \texttt{vtu}. As a
restriction, in the moment we only support \texttt{vtu} in combination
with \texttt{ascii}, i.e. binary vtu is not (yet) implemented. 
Note that the \texttt{vtu} plotters create substantially more files.
However, as in general with the VTK family, these files are well
compressible (for instance with \textit{zip}).

\begin{design}
ExaHyPE uses Peano routines for block structured output. Peano
draws a line between the semantic file format (\emph{what} is
represented in a file) and the file format syntax (\emph{how}
information is encoded in a file). This seperation reflects in the
possibility to dump either human-readable text files (ASCII) or
special binary files (for instance VTK's binary representation or
the HDF5 binary file format).
\end{design}

\subsubsection{Basic VTK plotter parameter combinations}
The basic VTK plotters are given by the combination
%
 \begin{equation*}
 \left\{
 \begin{matrix}
 \texttt{vtk} \\
 \texttt{vtu}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Legendre} \\
 \texttt{Cartesian}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Vertices} \\
 \texttt{Cells}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Ascii} \\
 \texttt{Binary}
 \end{matrix}
 \right\}
 \end{equation*}
%
Note that you cannot choose \texttt{Legendre} plotters on FV solvers,
while you can choose \texttt{Cartesian} plotters on DG solvers.

\begin{description}

\item[\texttt{vtk::Cartesian::vertices::ascii}]

Dumps the whole simulation domain into one VTK file. If you use multiple MPI
ranks, each ranks writes into a file of its own. 
The plotter uses ASCII, i.e.~text format, so these files can be
large. 
\texttt{output} has to be a valid file name where a rank and time step
identifier can be appended.
The plotter always adds a \texttt{.vtk} postfix.
Through the \texttt{select} statement, you can use spatial filters. if you add values 
such as \texttt{select =
left:0.4,right:0.6,bottom:0.2, top:0.8,front:0.2,back:0.5}, you get only data overlapping
into this box.
As VTK does not natively support higher order polynomials, the solution is
projected onto a grid, and the solution values are sampled on this grid. 
In this case, we use a Cartesian grid and the values are sampled at the grid
vertices.

\item[\texttt{vtk::Cartesian::vertices::binary}]

Same as \texttt{vtk::Cartesian::vertices}  \texttt{::ascii}, but the
files internally hold binary data rather than ASCII data. 
All written files should thus be slightly smaller.

\item[\texttt{vtk::Cartesian::cells::ascii}]
See \texttt{vtk::Cartesian::vertices::ascii}.
Solution values are sampled as cell values instead of vertex values.

\item[\texttt{vtk::Cartesian::cells::binary}]
Binary variant of plotter with identifier 
\texttt{vtk::Cartesian::cells::ascii}.

\item[\texttt{vtk::Legendre::vertices::ascii}]
See counterpart with \texttt{Cartesian} instead of \texttt{Legendre}. Mesh
values are not sampled at Cartesian mesh points but at Legendre points.

\begin{figure}
\begin{center}
\begin{minipage}{1.0\textwidth}
\includegraphics[width=0.50\textwidth]{screenshots/cartesian-meshref-subgrid.png}%
\hspace{1em}%
\includegraphics[width=0.50\textwidth]{screenshots/legendre-meshref-subgrid.png}
\end{minipage}
\end{center}
\caption[Screenshot (Paraview): Mesh refinement in 3D Cartesian vs. Legendre plotting]{Cartesian vs. Legendre volumetric plotters, with focus on mesh refinement, in this example around the center. The plot shows a color encoded scalar field.}\label{fig:volumetric-meshref}
\end{figure}

\begin{figure}
\begin{center}
\begin{minipage}{1.0\textwidth}
\centering
\includegraphics[width=0.46\textwidth]{sketches/paraview-legendre-cells.png}%
\hspace{1em}%
\includegraphics[width=0.46\textwidth]{sketches/paraview-legendre-vertices.png}
\end{minipage}
\end{center}
\caption[Screenshot (Paraview): 2D cell- vs. vertex-centered plotting]{Cell-centered plotting (left) vs. plotting at vertices (right) at the example of the Legendre plotter. For the cell centered plotting, values are sampled in the centers of each plotting cell, in the case of the Legendre plotter this is in each subcell center. In contrast, plotting on the Legendre vertices shows exactly the degrees of freedom as in the computation. In order to highlight the values at the vertices, they are represented by coloured balls. Typically one only sees a linearly interpolated cell colouring in such a display.}\label{fig:cells-vs-certices}
\end{figure}

\item[\texttt{vtk::Legendre::vertices::binary}]
See counterpart with \texttt{Cartesian} instead of \texttt{Legendre}. Mesh
values are not sampled at Cartesian mesh points but at Legendre points.

\item[\texttt{vtk::Legendre::cells::ascii}]
See counterpart with \texttt{Cartesian} instead of  \texttt{Legendre}.
Mesh values are not sampled at Cartesian mesh points but at Legendre points.

\item[\texttt{vtk::Legendre::cells::binary}]
See counterpart with \texttt{Cartesian} instead of  \texttt{Legendre}.
Mesh values are not sampled at Cartesian mesh points but at Legendre points.
\end{description}

\subsubsection{Plotters for limited solvers}
For the limited solvers, additionally the following family of
VTK-based plotters may be used:
%
 \begin{equation*}
 \left\{
 \begin{matrix}
 \texttt{vtk} \\
 \texttt{vtu}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Legendre} \\
 \texttt{Cartesian}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Vertices} \\
 \texttt{subcells}
 \end{matrix}
 \right\}
 ~\times~
 \texttt{limited}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Ascii} \\
 \texttt{Binary}
 \end{matrix}
 \right\}
 \end{equation*}
%
\begin{description}
\item[\texttt{vtk::Cartesian::vertices::limited::ascii}]
This plotter is for the LimitingADERDG solver and not suitable for any other
solver/scheme. It does everything the ordinary plotter (without "limited" in the
name) also does. On top of that, it includes another field ``LimiterStatus'' which
encodes the
limiting status of cells in discrete numbers $n\in\{0,1,2,3\}$ where 3 indicates
a troubled (limited) cell, 2 indicates a next-to-troubled (NT) cell, 1 indicates
a next-to-next-to-troubled cell (NNT) and 0 indicates an ordinary untroubled cell.
The limiting status of all points inside a cell/patch is similiar, so this method
is not efficient at all but mainly suitable for debugging.

\item[\texttt{vtk::Cartesian::vertices::limited::binary}] as above.

\item[\texttt{vtk::Cartesian::subcells::limited::ascii}] 
  This plotter generates a VTK unstructured mesh which
  shows the real position of the DOF (degrees of freedom)
  for a limited ADERDG solver. That is, for any unlimited
  cell it is equal to the \texttt{vtk::Legendre::cells}
  plotter while for any limited cell it is basically equal
  to a \texttt{vtk::Cartesian::cells} plotter which shows
  the real $2N+1$ subcells.

\item[\texttt{vtk::Cartesian::subcells::limited::binary}] as above.
\end{description}

\subsubsection{Plotters for cell averages}
In addition, there is a family for plotting only \emph{one}
point per cell (or patch), i.e. disregarding the complete subcell
structure. It follows the naming
%
\begin{equation*}
 \left\{
 \begin{matrix}
 \texttt{vtk} \\
 \texttt{vtu}
 \end{matrix}
 \right\}
 ~\times~
 \texttt{patches}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{gaps} \\
 \texttt{boxes}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Ascii} \\
 \texttt{Binary}
 \end{matrix}
 \right\}
\end{equation*}
where data are always stored cell-centered in the patches. 
Users can map values of a complete patch (for instance cell averages
or some flags) to a single number.
Data is enriched in any case with ExaHyPE-internal information. For
instance, in the vicinity of a limiting solver, limiting information
is written out. In the vicinity of MPI, rank information is written
out.

\begin{description}
\item[\texttt{vtk::patches::boxes::ascii}] Displays the cells as touching boxes (resulting in a regular Cartesian grid).
\item[\texttt{vtk::patches::gaps::ascii}] Mimics the look of
the \texttt{Legendre} plotter, i.e. an artificial gap between each
cell is inserted. 
\end{description}

\subsection{ASCII CSV human-readable file plotters}

Certainly even more straightforward then VTK is the usage of
CSV plotters. CSV stands for \emph{Comma Seperated Values} and is
an expensive but widespread and accepted file format for tables
of numbers. Virtually any postprocessing tool will be able to read
in such ASCII tables (ASCII is used to indicate a \emph{plain text}
file), such as \emph{gnuplot}, \emph{numpy} or \emph{Microsoft Excel}.

ExaHyPE already ships a basic CSV writer which is useful in practice
when combined with lower-dimensional slicing (cf. section
\ref{sec:filtering}), for instance to quickly write out a 1D axis
of data in a commonly understood fileformat.

In ExaHyPE, we currently have these CSV file formats:

\begin{description}

\item[ \texttt{csv::Legendre::vertices::ascii} ]
  Writes out the original Legendre nodes (vertices) in ASCII.
  Suitable for the Legendre basis.

\end{description}

\subsection{Tailored volumetric output formats: Peano and Carpet}
The VTK file formats from the previous chapters are a generic output format for
2D and 3D data which is supported straightforwardly by many visualisation tools.
However, there is a large overhead of metadata describing the position and size
of cells tied to these formats as they basically project \exahype's
block-structured mesh onto an unstructed mesh.

\subsubsection{Peano block regular file format}
The following combination of the peano block regular files is supported:
%
\begin{equation*}
 \texttt{Peano}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{Legendre} \\
 \texttt{Cartesian}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{vertices} \\
 \texttt{cells}
 \end{matrix}
 \right\}
 ~\times~
 \left\{
 \begin{matrix}
 \texttt{ascii} \\
 \texttt{hdf5}
 \end{matrix}
 \right\}
\end{equation*}
%
\begin{description}
\item [\texttt{Peano::Legendre::cells::hdf5}] Dumps data in the
   Peano block-regular file format, as proposed in section
   \ref{section:exahype-file-format}. It resembles closely the array-of-structure
   in-memory storage layout of ExaHyPE and saves the majority of the metadata
   overhead by essentially storing $\vec x, \mathrm{d}\vec x$ of each cell
   instead the positions of each data point in the subcell structure. 
   This plotter is not supported for Finite Volume solvers.
%
\item [\texttt{Peano::Legendre::vertices::hdf5}] Vertex-based variant of the
   above plotter.
   This plotter is not supported for Finite Volume solvers.
\item [\texttt{Peano::Legendre::cells::ascii}] ASCII, i.e.~text, output of
   Peano's block-regular file format.
   This plotter is not supported for Finite Volume solvers.
\item [\texttt{Peano::Legendre::vertices::ascii}] Vertex-based variant of the
   above plotter.
   This plotter is not supported for Finite Volume solvers.
\item [\texttt{Peano::Cartesian::vertices::ascii}] Similar to Legendre variant.
   The ADER-DG polynomials are mapped onto 
   Cartesian coordinates, i.e.~onto a block-regular Cartesian mesh per cell. 
   If you use $p$th order polynomials, the plotter dumps a $p \times p \times p$
   grid. The plotter projects the unknowns onto the vertices of this image
   grid. 
   This plotter is not supported for Finite Volume solvers.
\item [\texttt{Peano::Cartesian::vertices::hdf5}] Similar to variant above with
   HDF5 output file.
\item [\texttt{Peano::Cartesian::cells::ascii}] 
   If this plotter is used with an ADER-DG solver, the integration points are
   mapped onto Cartesian coordinates, i.e.~onto a block-regular Cartesian mesh per cell. 
   See remarks above. The unknowns are mapped onto the cell centres. 
   If the plotter is used with Finite Volumes, the Finite Volume submesh per
   cell is dumped.
\item [\texttt{Peano::Cartesian::cells::hdf5}] 
   HDF5 variant of the plotter as described above.
\end{description}

\subsubsection{The Carpet output file format}
The following combinations for the Carpet output file format are supported:
%
\begin{equation*}
\texttt{Carpet}
~\times~
\texttt{Cartesian}
~\times~
\texttt{vertices}
~\times~
\left\{
\begin{matrix}
\texttt{ascii} \\
\texttt{hdf5}
\end{matrix}
\right\}
\end{equation*}
%
That is, by design, the Carpet file format always stores vertex data,
sampled on a Cartesian grid. The Carpet plotter can be used in ASCII mode
(creating CSV tables) and in HDF5 mode (creating compact H5 files). Furthermore,
the plotter can be used in a Finite Volume, Discontinous Galerkin or 
Hybrid (Limiting) solver application. The two options in detail are:

\begin{description}
\item [\texttt{Carpet::Cartesian::Vertices::HDF5}]
   The CarpetHDF5 block regular fileformat is the file format
   used by the Cactus/Carpet/EinsteinToolkit codes. It has the advantage
   of wide support in common tools (such as a reader included in Visit and
   Amira as well as desktop players for 1D and 2D movies). The CarpetHDF5 file
   format supports real 1D/2D/3D slicing, ie. when taking a lower-dimensional
   slice in an ADERDG solver, we evaluate the polynomials on the submanifold.
   \\
   The CarpetHDF5 file format is not suitable for large ExaHyPE runs due to the
   enormous meta data overhead, mainly coming from the structure-of-array
   data layout (each written unknown goes into an own table) while ExaHyPE uses
   an array-of-structures approach (all written unknowns at one physical point
   are written together in one table). Also note that the CarpetHDF5 file format
   can only represent cartesian blocks, ie. each block must have a cartesian
   subgrid with fixed point/lattice spacing d$\vec x$. The data points in the
   CarpetHDF5 file format live on the vertices.
   \\
   In ExaHyPE, we currently support plotting the CarpetHDF5 file format straightforward
   from an ADERDG scheme by interpolating on a Cartesian subgrid. We also support
   dumping data in the CarpetHDF5 file format from the Finite Volume scheme, however
   this yields in quality loss in the outputted files as the interpolation from
   cell to vertex data is not ideal, also given the limited amount of ghost
   cell information available. The output will look blurred
   on coarse grids.
   \\
   For details about the CarpetHDF5 files as well as an overview about the many
   postprocessing tools, see section \ref{section:carpethdf5-file-format} in the
   appendix. In order to use the CarpetHDF5 file format, you have to build
   \exahype\ with HDF5 support.
   
\item [\texttt{Carpet::Cartesian::Vertices::ASCII}]
   This flavour of the Carpet file format does \emph{not} represent exactly the
   same data structure just in another container (as with the Peano format) but
   instead really writes very lengthy CSV tables. They can be directly read in
   by Gnuplot or similiar tools. However, in practice they are only useful for 
   lower dimensional data because they quickly grow big.
\end{description}

The Carpet writer is especially known for some distinct features:

\begin{itemize}
	\item Actual dimension reducing slicing of the data (i.e. writing a 1D cut,
	or a 2D plane from 3D data). As the file format is so far in use only in
	highly symmetric astrophysical simulations, we concentrated on easy 
	Cartesian slicing only (i.e. planes and lines aligned with the Cartesian
	coordinate system).
	\item The option to write all the written output vector to one file or to
	multiple files, as well as to open one file per timestep or to write
	several timesteps in a single file.
	\item So far, every MPI rank writes their own file. This is similar to all
	other ExaHyPE plotters.
\end{itemize}

\todo{Add some screenshots of how Carpet files look like}

\subsubsection{The Flash output file format}
\begin{description}
\item [\texttt{Flash::hdf5}] The experimental FlashHDF5 block regular 
  file format tries to
  resemble the file format used by the FLASH code. At the current stage, a
  compatibility is not yet given. Once this is finished, the file format will have
  a wide support in common tools such as Visit and standalone postprocessing and
  visualization tools. For more details see section
  \ref{section:flashhdf5-file-format}.
\end{description}

\begin{design}
As users shall be able to use ExaHyPE without any external libraries, we make the
HDF5 support optional for all provided plotters. If you enable a plotter which
requires HDF5 during runtime on an ExaHyPE build without HDF5, the plotter will
not do anything but print warnings at every plotter step. This allows you to
get started or continue working with ExaHyPE without worrying about dependencies.
\end{design}
\begin{info}
Please refer to section \ref{sec:enable-hdf5} to learn how to enable HDF5 in
ExaHyPE's build system.
\end{info}

\subsection{Non-volumetric plotters}

\begin{description}

\item [\texttt{probe::ascii} ]
This option probes the solution over time, i.e.~you end up with files
specified by the field \texttt{output} that hold a series of samples in one
particular point of the solution. 
The code adds a \texttt{.probe} postfix to the file name.
This option should be used to plot seismograms, e.g.
For this data format is \texttt{ascii}, the file holds one floating point
value per line per snapshot in text format.
To make the plotter know where you want to probe, please add a line alike
\begin{code}
select   = x:0.3,y:0.3,z:0.3 
\end{code}
to your code.
If you run your code with MPI, the probe output files get a postfix
\texttt{-rank-x} that indicates which rank has written the probe.
Usually, only one rank writes a probe at a time. However, you might have placed
a probe directly at the boundary of two ranks. 
In this case, both ranks do write data.
If dynamic load balancing is activated, the responsibility for a probe can
change over time. 
If this happens, you get multiple output files (one per rank) and you have to
merge them manually.
Please note that output files are created lazily on-demand, i.e.~as long
as no probe data is available, no output file is written. 
The probe file contains data from all variables specified in the plotter. 
It furthermore gives you the time when the snapshot had started and the actual
simulation time at the snapshot location when data had been written.
As \exahype\ typically runs adaptive or local time stepping, a snapshot is
triggered as soon as all patches in a simulation domain have reached the next
snapshot time.
At this point, some patches might already have advanced in time. 
This is why we record both snapshot trigger time and real time at the data
point.

\item [n.a.]

We integrated the possiblity to plot only integrals/sums of values into the
ExaHyPE core. This technique is proposed in section \ref{section:global-integrals-computing}.
However, currently we did not expose this yet with a plotter device.

\end{description}


\section{Filtering and Slicing}\label{sec:filtering}

Filtering allows you to reduce the dimensionality of your output or
to plot only certain regions of the computational domain. Note that by
default and without filtering, ExaHyPEs plotting is always full 3D\footnote{in case of a 2D simulation, the plotting is of course always full 2D}.
For big simulations, this is certainly not always useful.

\exahype\ offers two types of built-in filtering: You can use the
\texttt{select} statement to impose spatial constraints on the output data, and
you can constrain the number of variables written. 

The spatial selection mechanism is described above for the individual plotters.
We built it into the \exahype\ kernel as IO has a severe performance impact.
It synchronises the involved MPI ranks.
If we know that only variables of a certain region are of interest, only ranks
responsible for these regions have to be synchronised, i.e.~the resulting code
is faster.

With a restriction on only a few output parameters, you can reduce the output
data file and thus reduce the pressure on the memory system.
By default, the first $k$ unknowns are streamed if $k$ unknows are to be
written.
However, you can use the postprocessing techniques discussed below to plot other 
unknowns from your unknown set.

\section{Parameter selection and conversion}
\label{sec:parameter-selection-and-conversion}

If you want to plot only a few parameters of your overall simulation, you have
to invest a little bit of extra work. 
First, adopt your plotter statements such that the \texttt{unknowns} statement
tells the toolkit exactly how many variables you want to write.
Typically, you plot fewer variables than the unknowns of your actual PDE.
However, there might be situations where you determine additional postprocessing
data besides your actual data and you then might plot more quantities than the
original unknowns.

Once you have tailored the \texttt{unknowns} quantity, you rerun the toolkit and
you obtain classes alike \texttt{MySolver\_Plotter0}.
These classes materialise the individual solvers as written down in your
specification file and they are enumerated starting from 0.
Open your plotter's implementation file and adopt the function there that maps
quantities from the PDE onto your output quantities.
By default, this mapping is the identity (that's what the toolkit generates).
However, you might prefer to do some conversations such as a conversion of
conservative to primitive variables.
Or you might want to select the few variables from the PDE solver that you are
actually interested in.

Below is an example:
\begin{code}
  solver ADER-DG MyEulerSolver
    variables const    = 5
    order const        = 3
    maximum-mesh-size  = 0.1
    time-stepping      = global
    type const         = nonlinear
    terms const        = flux,ncp
    optimisation const = generic
    language const     = C

    plot vtk::Cartesian::ascii MySolutionPlotter
      variables const = 2
      time            = 0.0
      repeat          = 0.05
      output          = ./solution
    end plot
 
  ...
\end{code}

\noindent
Originally, we did plot all five variales. Lets assume we are only interested in
$Q_0$ and $Q_4$. We therefore set \texttt{variables} in the plotter to 2 and
modify the corresponding generated plotter:
\begin{code}
void MyEulerSolver_Plotter0::mapQuantities(
    const tarch::la::Vector<DIMENSIONS, double>& offsetOfPatch,
    const tarch::la::Vector<DIMENSIONS, double>& sizeOfPatch,
    const tarch::la::Vector<DIMENSIONS, double>& x,
    double* Q,
    double* outputQuantities,
    double timeStamp
) {
  outputQuantities[0] = Q[0];
  outputQuantities[1] = Q[4];
}
\end{code}

Alternatively, with all the information like the space $x$ and time $t$
at hand, you can easily inject other quantities into \texttt{outputQuantities},
for instance exact initial conditions. You can also use the information
to compute \emph{locally} a derived quantity from $Q$.

\section{User-defined plotters: Understanding the Plotting API}

ExaHyPE already proposes a number of plotters, but certainly this will not cover
all users needs. Thus, our plotting API (C++ class API) is written in a way that
should allow users to come up with their own plotters.

Users can start to implement their own plotters when providing the string
\texttt{user::defined} as plotter description. The toolkit then generates another
method in the user plotter. Instead of allowing to map quantities, users now can
decide what to do when an individual patch is visited by Peano. One can then access
all unknowns on a patch, or, more generic, the cell description.

\todo[inline]{Describe better the plotting classes and where to start to implement
an own plotter.}

