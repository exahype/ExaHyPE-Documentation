\section{Minimalistic Finite Volumes for the Euler equations}


We provide a complete Finite Volume implementation of a plain Euler solver
realised with \exahype. 
This solver relies on a Rusanov flux and can, with only a few lines, be
changed into an ADER-DG scheme later.
Indeed, it can be used by an ADER-DG scheme as a limiter.
You find the \texttt{EulerFV} in the
\texttt{Demonstrators} subdirectory.

\subsection{Preparation}

The demonstrator folder contains solely files modified to realise the solver.
All glue code are to be generated with the toolkit.
Before we do so, we open the file \texttt{EulerFV.exahype}.
This simple text file is the centerpiece of our solver.
It specifies the solver properties, the numerical schemes, and it also holds all
the mandatory paths:
\begin{code}
exahype-project EulerFV

  peano-kernel-path const    = ./Peano
  exahype-path const         = ./ExaHyPE
  output-directory const     = ./Demonstrators/EulerFV

  computational-domain
    dimension const          = 2
    width                    = 1.0, 1.0
    offset                   = 0.0, 0.0
    end-time                 = 1.0
  end computational-domain
 
  solver Finite-Volumes MyEulerSolver
    variables const    = rho:1,j:3,E:1
    patch-size const   = 10
    maximum-mesh-size  = 5e-2
    time-stepping      = global
    type const         = godunov
    terms const        = flux
    optimisation const = generic
    language const     = C
    plot vtu::Cartesian::cells::ascii EulerWriter
      variables const = 5
      time            = 0.0
      repeat          = 0.5E-1
      output          = ./variables
    end plot
  end solver
end exahype-project
\end{code}


\noindent
To prepare this example for the simulation, we have to generate all glue code
\begin{code}
> ./Toolkit/toolkit.sh \ 
    Demonstrators/EulerFV/EulerFV.exahype
\end{code}


\noindent
and afterwards change into the application's directory
(\texttt{Demonstrators/EulerFV}) and type in \texttt{make}.
Depending on your system, you might have to set/change some environment
variables or adopt pathes, but both the toolkit and the makefile are very
chatty.
For the present demonstrator, it is usually sufficient to set either
\begin{code}
> export COMPILER=Intel
\end{code}
or 
\begin{code}
> export COMPILER=GNU
\end{code}

\noindent
In both modes, the makefile defaults to \texttt{icpc} or \texttt{g++},
respectively.
To change the compiler used, export the variable \texttt{CC} explicitly.


\begin{design}
Most modifications to the specification file do not require you
to rerun the {\exahype} toolkit.
A rerun is only required for changes to parameters 
with a trailing \texttt{const}, if you change the number or type
of solvers, or if you change the number of plotters in the specification
file.
\end{design}

\subsection{Run}

Once the compile has been successful, you obtain an executable
\texttt{ExaHyPE-EulerFV} in your application-specific directory.
\exahype's specification files always act as both specification and
configuration file, so you start up the code by handling it over the spec file
again:
\begin{code}
> ./ExaHyPE-EulerFV ./EulerFV.exahype
\end{code}

\noindent
A successful run yields a \texttt{*.pvd} file that you can open with
Paraview\footnote{\url{www.paraview.org}} or
VisIt\footnote{\url{wci.llnl.gov/simulation/computer-codes/visit}},
e.g.
There are two quantites plotted: 
The time encodes per vertex and quantity at which time step the data has been
written.
For these primitive tests with global time stepping, this information is of
limited value.
It however later makes a big difference if parts of the grid are allowed to
advance in time asynchronous to other grid parts.
The second quantity $Q$ is a vector of the real unknowns.
The underlying semantics of the unknowns is detailed in Section
\ref{chapter:new-development-project}.
For the time being, you may want to select first entry which is the density or
the last quantity representing the energy. The plot should look similar
to Figure \ref{fig:02fvm-snapshots}.


Please note that this minimalistic \exahype\ application will yield some
errors/warnings that no \texttt{exahype.log-filter} file has been found and thus
no filter is applied on the output messages.
You will obtain tons of messages.
The tailoring of the log outputs towards your needs is subject of discussion in
Chapter \ref{chapter:output}.
For the present experiment, you should be fine without diving into these
details.

\newpage

\begin{figure}
\begin{center}
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_00.png}
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_01.png}
  \\[0.1cm]
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_02.png}
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_04.png}
  \\[0.1cm]
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_06.png}
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_08.png}
  \\[0.1cm]
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_16.png}
  \includegraphics[width=0.36\textwidth]{screenshots/02_FVM_32.png}
\end{center}
\caption[Paraview snapshots: Euler Demonstrator Logo flow]{Snapshots of the time evolution of $Q_4$, ie. the energy distribution
in the EulerFlow Finite Volume demonstrator code.}%
\label{fig:02fvm-snapshots}
\end{figure}