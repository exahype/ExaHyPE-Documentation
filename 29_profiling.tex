\chapter{Profiling}
\label{chapter:profiling}


While we use \exahype\ extensively with profiling and analysis tools such as
Intel's VTune Amplifier and Likwid, we equip the engine with a couple of profile
options and features.


\section{Profiling scope}

Profiling can target the whole code or only particular algorithmic phases.
The profiling target can be selected via a block

\begin{verbatim}
begin profiling
  profiling-target = <enum>
end profiling
\end{verbatim}

where we can choose between \texttt{whole\_code} and \texttt{neighbour\_merge}, \texttt{predictor}, and \texttt{update}.
The default is \texttt{whole\_code}.
The block must be placed within the \texttt{exahype-project} environment.
Note that it is important to run the code for a certain
number of \texttt{time-steps} instead of specifying a simulation
\texttt{end-time} if a profiling target other than \texttt{whole\_code}
is specified.

Alternatively or additionally, we can measure the processing time of cell-wise operations
such as (fused) cell update or space-time predictor computation via:

\begin{verbatim}
begin profiling
  measure_cell_processing_times = true
  measure_cell_processing_times_iter = <integer>
end profiling
\end{verbatim}

The second parameter specifies the number of measurements to perform when measuring cell processing times. 
The results are averaged over the number of measurements. The default is one measurement.

Note that these real-time measurements can be performed in release mode.
No special profiling build target must be specified.

\section{Build targets}

We currently support two build targets for profiling:


\begin{itemize}
  \item \texttt{MODE=Profile} Creates a release executable which is augmented
  with symbol information and instrumented to output gprof performance data.
  While the gprof output can be used directly, we also recommend that you use
  this mode with 3rd party performance analysis tools such as VTune.
  \item \texttt{MODE=PeanoProfile} Creates a release executable which outputs
  tailored Peano performance data. See section below on further information.
\end{itemize}


\section{Plain performance analysis with Peano's scripts}
\label{section:profiling:Peano-plain}

\exahype\ is based upon Peano and thus offers a few lightweight analysis tools
that can be used {\em without} any particular compiles. Please ensure
however that your log filters enable outputs from
\texttt{peano::performancenalaysis}, i.e.~set these entries as whitelist
entries.
Pipe the executable output into a file and pass it over to the Python scripts
\texttt{domaindecompositionanalysis.py} which you find in the Peano's directory
\texttt{peano/}\linebreak \texttt{performanceanalysis}.
Invoking the script without parameters displays a usage message.
If the script terminates successfully, you obtain a HTML page which you can
study with any browser.



\section{Advanced Peano performance analysis}
\label{section:profiling:Peano-advanced}

Once you translate your code with \texttt{MODE=PeanoProfile} and you allow log
outputs from \linebreak \texttt{peano::performancenalaysis}, you obtain lots of
data that you can postprocess with the Python script
\texttt{performanceanalysis.py} from the directory 
\texttt{peano/}\texttt{performanceanalysis}.

If you use log files, each MPI rank pipes its performance analysis data into a
file of its own.
These files have to be merged before you hand them over to the performance
analysis.
There is a Python script \texttt{merge-log-files.py} coming along with Peano
that allows you to do this.


\section{Scalasca}
\label{section:profiling:Scalasca}

\exahype\ supports profiling through
Scalasca\footnote{\url{http://www.scalasca.org}.} via a variety of Score-P 
macro annotations in the Peano kernel and within the \exahype\ core routines.
To translate the code with Scalasca, change the compiler to something alike

\begin{code}
export EXAHYPE_CC="scorep --noonline-access --nocompiler --mpp=none \
  --thread=none --user icpc -DUseScoreP"
export EXAHYPE_CC="scorep --noonline-access --nocompiler --mpp=none \
  --thread=none --user g++ -DUseScoreP"
export EXAHYPE_CC="scorep --noonline-access --nocompiler --mpp=mpi \
  --thread=none --user mpicxx -DUseScoreP"
\end{code}

\noindent
All the arguments instruct Score-P, the instrumenter used by Scalsca. 
The very last argument is passed through to \exahype/Peano and makes it use the
Score-P macros. 
Please note that this compiler variant works with any build variant, but we
strongly encourage users to use solely \texttt{MODE=Release} and 
\texttt{MODE=PeanoProfile}.
