\chapter{\exahype\ workflow}
\label{chapter:workflow}

\section{Toolkit usage}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{sketches/workflow.pdf}
  \caption[Cartoon (Inkscape): Specfile workflow]{A typical \exahype\ workflow.}
  \label{fig:workflow}
\end{figure}

The typical workflow when working with the \exahype\ engine
is graphically displayed in Figure \ref{fig:workflow} and described
in the following:

\begin{enumerate}
  \item The user writes a specification file (text format) that holds all
  required data ranging from paths, which parallelisation to use, computational
  domain up to a specification which kind of solvers will exist and which
  numerical schemes they realise.
  \item This specification file is handed over to the \exahype\ toolkit which is
  a Python tool. It internally relies on Python scripts and invokes the libxsmm
  generator driver as well. A local build of the libxsmm's code generation driver
  is therefore a prerequisite for using optimised kernels.
  \item The toolkit yields a couple of files (Makefile, glue
  code, helper files, \ldots). Among them is also one C++ implementation class
  per solver that was specified in the specification file. The output directory
  in the specification file defines where all these generated files go to.
  \item \begin{enumerate}
    \item Within each C++ implementation file, the user can code solver
    behaviour such as initial conditions, mesh refinement control, and so
    forth.
    \item The whole build environment is generated. A simple \texttt{make} thus
    at any time should create the \exahype\ executable.
  \end{enumerate}
  \item If you run your \exahype\ executable, you have to hand over the
  specification file again. Obviously, many entries in there (simulation time
  or number of threads to be used) are not evaluated at compile time but at
  startup. You don't have to recompile your whole application if you change the
  number of threads to be used.
\end{enumerate}

\noindent
To summarise, the blueish text files in Figure \ref{fig:arch} are the only files you typically have to
change yourself. All the remainder is generated and assembled according to the
specification file.

% \section{Switching between Generic and Optimised Kernels}
Driven by the choice in the specification file, \exahype\ chooses either generic kernels
or optimised kernels. If you switch between the generic and the optimised kernels,
you have to use the toolkit to regenerate the glue code.

\section{ExaHyPEs build system}

The build system of ExaHyPE is driven by environment variables. The major
variables to drive a build are given in the following:

% todo: make this nicer.

\begin{small}
\begin{verbatim}
The ExaHyPE build cheat sheet
=============================

  export COMPILER=GNU                   Select GNU compiler
  export COMPILER=Intel                 Select Intel compiler (default)

  export MODE=Debug                     Build debug version of code
  export MODE=Asserts                   Build release version of code that is augmented with assertions
  export MODE=Profile                   Build release version of code that produces profiling information
  export MODE=Release                   Build release version of code (default)

  export SHAREDMEM=TBB                  Use Intels Threading Building Blocks (TBB) for
                                        shared memory parallelisation
  export SHAREDMEM=OMP                  Use OpenMP for shared memory parallelisation
  export SHAREDMEM=None                 Do not use shared memory (default if not indicated
                                        otherwise by "shared memory ..." message above)

  export DISTRIBUTEDMEM=MPI             Use MPI
  export DISTRIBUTEDMEM=None            Do not use MPI (default)
\end{verbatim}
\end{small}

\begin{design}
ExaHyPEs build system is minimal, it just compiles all C++/Fortran files
it can find. Since there are no external dependencies in ExaHyPE, it is easy
to exchange the build system with a tool suitable to your code and libraries.
\end{design}

\section{Build managament}

In a high performance production system, typically a number of different
combinations of builds (in terms of optimizations, included features, etc.)
arises. In order to organize the compilation process in a structured manner,
big software packages often come up with words like \emph{configurations} or
\emph{builds} which refer to a way how an executable (a binary) can be built
reproducably, fast and in parallel. Typically, these systems are out-of-tree
build systems. \emph{Out-of-tree} means that the source code files (C, Fortran,
Header files) and generated build files (object files and modules) are kept in
seperate directory trees.

In ExaHyPE, there is a lot of contributed code which results from endavours to
master the ExaHyPE production workflow in a quick and cross plattform
portable manner. All of this is optional code which you may want to use of but
don't have to in order to use ExaHyPE.
The main outcome of these products can be found in the
repository directory \texttt{Miscellaneous/BuildScripts/}. It is:

\subsection{The Exa toolbox}
The \emph{exa} command line utility collects all kind of standard tasks which
frequently arise in the every day life of ExaHyPE simulations. Most of these
tasks are accomplished by individual standalone command line executables. Such
tasks are

\begin{itemize}
\item Manage the repository: Updating external dependencies and providing
  a quick way to upgrade an ExaHyPE developer installation (Updating Peano
  and recompiling the toolkit).
\item Location and identification of the vast amount of installed applications
\item Running the toolkit from any directory
\item Compiling an application after running the toolkit on it, also taking
 into account computer-specific settings, paths, files overwritten by the
 toolkit (which can be restored from git), cleaning before the build.
\item Quickly running an application in a dedicated directory
\item Compiling an application for several polynomial orders serially
\item Managing out of tree builds (setup, synchronization, compiliation, execution)
\item Checking and looking up of build-specific environment variables
\item Simulation managament
\item Starting postprocessing tools from any directory
\item Starting analysis tools (such as for Peano) from any directory
\item ... expandable: Users can add their own commands   
\end{itemize}

\subsection{Out-of-Tree compilation and code generation}
\label{sec:oot}
In principle, one can understand the Build process of ExaHyPE as a chain
of individual processing units:

\begin{enumerate}
\item A code generator (ie. the toolkit or the Python3 optimized kernel
   generator) \emph{compiles} C++ code from some abstract instructions.
   That is, the code generator is a classical compiler compiling some high-level
   language to C++.

\item The C compiler, creating machine code from the C code. This includes
   the turing complete machinery of C++ templating which is heavily used in
   Peano as well as in the kernels to create optimized code with the minimal
   amount of instructions on a certain path.

\item Actually running ExaHyPE, which is without doubt the most challenging
   task of the chain.
\end{enumerate}

The fact that code generation is widely employed in ExaHyPE makes it difficult
to apply an ordinary out-of-tree build concept which solely relies on the idea
to put the build files at some other place. This approach fails as soon one
wants to build in parallel different parts of the program which rely on
different generated code which basically would go at the same place.

Thus, the out-of-tree concept logically starts even before the very first
step of the compilation chain outlined above: Literally all the crucial
code directories are mirrored (ie. copied) to an out-of-tree location
and executed there, including the toolkit and all code generation.

\section{Simulation managament}

Following the logic of section \ref{sec:oot}, running a simulation is the
pretty same thing as compiling a binary. In real-world applications users want
to do a lot of them, typically in parallel, executed on a fast parallel
file system in a computer cluster. Performing large three-dimensional
time-dependent simulations is a complex numerical task\footnote{as
can be read at the website of \url{http://simfactory.org/}.}. Thus there
is an urgent need for tools managing the directory layout and queue managament.
In principle, this is beyond the scope of ExaHyPE. However, similar to the
build tools, there has been some contributed code to for a basic managament
of generic simulations.

\subsection{Templated specification files}

As the ExaHyPE specification files are described by an unique grammar which
can in principle only be \emph{exactly} parsed by the Python tookit, it
is useful to understand specification files as common text files when it comes
to batch processing. Typical attemps are therefore replacement of striking
patterns such as \texttt{@VARIABLENAME@} in a key-value manner. Such a mechanism
is broadly employed in all kind of runscripts available at
\texttt{Miscellaneous/RunScripts}, ie. the templated runners for ExaHyPE.
Following the logic of the build system, they are all driven by environment
variables.


