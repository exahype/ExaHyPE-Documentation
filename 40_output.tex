\chapter{\exahype\ program output}
\label{chapter:output}

In this Chapter, we discuss how to tailor the \exahype\ program output to your
needs. 
We focus on direct output such as terminal messages.
Information on supported output file formats can be found in Chapter
\ref{chapter:plotting} while real on-the-fly postprocessing such as the
computation of global integrals is picked up in Chapter
\ref{chapter:postprocessing}.


\section{Logging}
\label{chapter:output:logging}

\exahype\ creates a large number of log files and log messages; notably if you
build in debug or assert mode. 
Which data actually is written to the terminal can be controlled via a file
\texttt{exahype.log-filter}.
This file has to be held in your executable's directory.
It specifies exactly which class of ExaHyPE is allowed to plot or not. 
The specification works hierarchically, i.e.~we start from the most specific
class identifier and then work backwards to find an appropriate policy.

\begin{code}
# Level            Trace    Rank                    Black or white list entry
# (info or debug)           (-1 means all ranks)

debug              tarch    -1                      black
debug              peano    -1                      black

info               tarch    -1                      black
info               peano    -1                      black

info               peano::utils::UserInterface -1   white
info               exahype  -1                      white
\end{code}

If no file \texttt{exahype.log-filter} is found in the working directory, a
default configuration is chosen by the code.
Please note that a some performance analyses requires several log statements to
be switched on.
We refer to Peano's handbook, the usage descriptions of the analysis
postprocessing scripts and the respective guidebook sections for details.

By default, Peano pipes all output messages to the terminal, and many developers
thus pipe such output into a report file. 
Alternatively, you can add a 

\begin{code}
  log-file = mylogfile.log
\end{code}

\noindent
statement to your specification file right after the \texttt{architecture}
flag.
If such a statement exists \exahype\ pipes the output into the respective log
files.
Furthermore, one log file is used per grid sweep if you translate
with assertions or in debug mode where lots of information is
dumped.
This enables you to search for particular information of a particular grid
sweep.
Once you translate your code with MPI support, each rank writes a
log file of its own.
This way, we avoid garbage in the output files if multiple MPI ranks write
concurrently as well as congestion on the IO terminal.


\section{Grid statistics}

By default, \exahype\ does not plot statistics about the grid such as used mesh
widths.
There are multiple reasons why we refrain from this:

\begin{itemize}
  \item \exahype\ makes the compute grid host the solution but often chooses on
  purpose finer grids for efficiency reasons.
  \item Most applications can derive information about the actual compute grids
  used from the output files and a generic plot thus would be redundant.
  \item In MPI, \exahype\ tries to avoid global communication whereever
  possible, i.e.~any global grid statistics written out can be corrupted. If you
  use plot routines, all data is consistent, but this comes at the price of some
  additional synchronisation.
\end{itemize}

\noindent
For many codes it makes, at least throughout the development phase, however
sense to track grid statistics such as used mesh sizes and vertex counts. 
To enable it, please translate your code with the option
\texttt{-DTrackGridStatistics}.
For this, we propose to add the line

\begin{code}
PROJECT_CFLAGS+=-DTrackGridStatistics
\end{code}

\noindent
to you makefile. Following a recompile, you obtain data similar to

\begin{code}
 48.5407      info         step 13      t_min          =0.0229621
 48.5407      info              dt_min         =0.00176632
 48.5408      info              memoryUsage    =639 MB
 48.5408      info              inner cells/inner unrefined cells=125479/59049
 48.5408      info              inner max/min mesh width=[5,5]/[0.0617284,0.0617284]
 48.5408      info              max level=6
\end{code}
 