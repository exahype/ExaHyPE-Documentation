\chapter{Adaptive mesh refinement}
\label{chapter:adaptive-mesh-refinement}
In this chapter, we demonstrate the steps to perform for using ExaHyPE's
adaptive mesh refinement.
To this end, we consider an ADER-DG solver for the compressible Euler
equations (Ch.~\ref{chapter:aderdg-user-defined-fluxes}).
Note that \exahype\ does currently only support adaptive mesh refinement
for the ADER-DG solver and the limiting ADER-DG solver.
We currently do not support adaptive mesh refinement for the FV solver.

\section{Prerequisites}
In the following sections, we assume that the reader has performed all steps
in Ch.~\ref{chapter:aderdg-user-defined-fluxes} and
is able to run the example given there on a uniform mesh.

In order to enable adaptive mesh refinement up to a certain maximum depth,
we have to add a parameter \texttt{maximum-mesh-depth} just below the
mandatory parameter \\
\texttt{maximum-mesh-size}:
\begin{code}
exahype-project  Euler2d

  ..
  ..
  
  solver ADER-DG MyEulerSolver
    variables const          = 5
    order const              = 3
    maximum-mesh-size        = 0.1
    maximum-mesh-depth       = 2
    time-stepping            = global
    type const               = nonlinear
    terms const              = flux
    optimisation const       = generic
    language const           = C
    ..
    ..
  end solver
end exahype-project  
\end{code}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Static mesh adaptivity: A geometry-based refinement criterion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Let us start with a simple geometry-based refinement criterion.
In our example, we consider a uniform mesh of $9\times9$ cells
on the unit cube\\ 
(use e.g. \texttt{maximum-mesh-size=0.1}). We additionally
want to perform one level of adaptive refinement in the region
$[0.666,1]\times[0.666,1]$ and two levels 
of adaptive refinements in region $[0.333,0.666]\times[0.333,0.666]$, cf. figure \ref{fig:static-mesh}.
\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.45\textwidth]{screenshots/13_static_adaptive_grid.png}    
\end{center}
\caption[Paraview screenshot: 2D AMR grid]{Exemplary static mesh refinement}\label{fig:static-mesh}
\end{figure}
To facilitate the desired adaptive mesh refinement, we augment the function \texttt{refinementCriterion(...)} in our \texttt{MyEulerSolver.cpp} file as
given below: (No modification of the project's specification file is
necessary.)
\begin{code}
exahype::solvers::Solver::RefinementControl
Euler::MyEulerSolver::refinementCriterion(
    const double* luh, const tarch::la::Vector<DIMENSIONS, double>& center,
    const tarch::la::Vector<DIMENSIONS, double>& dx, double t,
    const int level) {
  if (level == getCoarsestMeshLevel()) 
    if (center[0] > 0.666)
      if (center[1] > 0.666)
      return exahype::solvers::Solver::RefinementControl::Refine;
  
  if (center[0] > 0.333 && center[0] < 0.666)
    if (center[1] > 0.333 && center[1] < 0.666)
      return exahype::solvers::Solver::RefinementControl::Refine;
      
  return exahype::solvers::Solver::RefinementControl::Keep;
}
\end{code}
Please be aware that we do not enforce a 2:1 balancing of the mesh
in \exahype. The generic kernels we provide are able to 
restrict and prolongate over an arbitrary number of levels.
If a 2:1 balance is important for your application, you
have to enforce it on your own by carefully
choosing the mesh refinement regions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Abbreviation for section for TOC where it is too long
\section[Dynamic adaptive mesh refinement: A density-based ref. criterion]{Dynamic adaptive mesh refinement: A density-based refinement criterion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ExaHyPE also allows you to dynamically refine and recoarsen the mesh.
A simple refinement criterion that takes the density amplitude
into account might look as follows:
\begin{code}
exahype::solvers::Solver::RefinementControl
Euler::MyEulerSolver::refinementCriterion(
    const double* luh, const tarch::la::Vector<DIMENSIONS, double>& center,
    const tarch::la::Vector<DIMENSIONS, double>& dx, double t,
    const int level) {
  double largestRho  = -std::numeric_limits<double>::max();

  kernels::idx3 idx_luh(Order+1,Order+1,NumberOfVariables);
  dfor(i,Order+1) {
    ReadOnlyVariables vars(luh + idx_luh(i(1),i(0),0));

    largestRho = std::max (largestRho,  vars.rho());
  }

  if (largestRho > 0.65) {
    return exahype::solvers::Solver::RefinementControl::Refine;
  }

  if (level > getCoarsestMeshLevel())
    return exahype::solvers::Solver::RefinementControl::Erase;

  return exahype::solvers::Solver::RefinementControl::Keep;
}
\end{code}
In order to use the above $d$--dimensional for loop, make sure
to include the file\\
\texttt{peano/utils/Loop.h}.
The index function \texttt{kernels::idx3} is included
via \\
\texttt{kernels/KernelUtils.h}
%If we run the simulation, we should get plots similar to the ones given below:
% \section{Dynamic mesh adaptivity: A density-based refinement criterion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limiter-guided static adaptive mesh refinement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{info}
Limiter-based refinement works only for solvers of type \\
\texttt{Limiting-ADER-DG} (see section
\ref{chapter:coupling-limiter}). They utilise the troubled cell indicator
routines which are ignored for the pure \texttt{ADER-DG} solver.
\end{info}
We start here again with prescribing a static refinement criterion.
However, this time we will not utilise the \texttt{refinementCriterion} routine
but we will (ab)use the physical admissibility detection (PAD)criterion
\texttt{isPhysicallyAdmissible}.

\begin{design}
In \exahype\,, we enforce the constraint that limiting is only performed on the
finest level of the adaptive mesh.
Should a cell be marked as troubled on the coarsest mesh level,
it will be refined down to the finest user-defined mesh level.
However, not just this cell is refined. Also a certain number
of neighbouring cells.
This is necessary since we need to place also the FV and DG helper
cells on the finest level of the mesh.
\end{design}
If we now mark a certain area as troubled by means of the PAD criterion,
this will result in mesh refinement as well as in placing FV volume patches
in the marked areas.

In our example, we mark all cells on the circle or sphere $\|\textbf{x}-0.5\|_2
= 0.25$ as troubled. We detect these cells by counting vertices inside and outside of the circle. Cells which are cut by the circle have 
inside (and outside) vertex counts which are larger than zero but smaller
than $2^d$.
\begin{code}
MySolver_ADERDG.cpp:
[..]
bool MyProject::MySolver_ADERDG::isPhysicallyAdmissible(
    const double* const solution,
    const double* const observablesMin,const double* const observablesMax,
    const int numberOfObservables,
    const tarch::la::Vector<DIMENSIONS,double>& center,
    const tarch::la::Vector<DIMENSIONS,double>& dx,
    const double t, const double dt) const {
  int outsidePoints = 0;

  tarch::la::Vector<DIMENSIONS,double> offset = center-0.5*dx;
  dfor2(p)
    tarch::la::Vector<DIMENSIONS,double> corner=offset;
    #if DIMENSIONS==3
    corner[2]+=p[2]*dx[2];
    #endif
    corner[1]+=p[1]*dx[1];
    corner[0]+=p[0]*dx[0];

    outsidePoints += (tarch::la::norm2(corner)>rad) ? 1 : 0;
  enddforx

  if (outsidePoints>0 && outsidePoints<TWO_POWER_D)  {
    return false;
  }
  else {
  return true;
  }
}
[..]  
\end{code}
This yields the mesh and limiter domain depicted in figure \ref{fig:13_static_adaptive_limiter_grid}.

\begin{figure}

\makebox[\textwidth][c]{ % centering overwidth table
\subfloat[One helper layer]{\includegraphics[width=0.45\textwidth]{screenshots/13_static_adaptive_limiter_grid}}
\hspace*{1cm}
\subfloat[Two helper layers]{\includegraphics[width=0.45\textwidth]{screenshots/13_static_adaptive_limiter_grid_2_layers}}
} % overwidth table centering end
\caption[Screenshot (Paraview): 2D Limiter example]{Actual troubled cells are marked in red, pure ADER-DG cells 
are marked in gray, the DG helper cells are marked in light-blue, and the FV helper cells are marked in orange.}\label{fig:13_static_adaptive_limiter_grid}
\end{figure}

\begin{design}
We distinguish between limiter-based refinement during the imposition of the
initial solution and limiter-based refinement during the time stepping.
For the former, we evaluate again after each refinement if the new cells'
ADER-DG (initial) solution is troubled. 
For the latter, this check is not performed. Here, all children of a refined
cell are marked as troubled and thus compute with FV.
\end{design}

%\begin{info}
The selection of a value of the optional
\texttt{Limiting-ADER-DG} solver parameter \\
\texttt{helper-layers}
larger than 1 results in more halo refinement around the marked troubled cells.
This is necessary for placing the additional helper cells around the
troubled cells.
In order to produce the picture below, we have set \texttt{helper-layers} to 2, cf. figure \ref{fig:13_static_adaptive_limiter_grid}b.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limiter-guided dynamic adaptive mesh refinement}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
We start again with the \texttt{Limiting-ADER-DG} solver application from Ch.
\ref{chapter:coupling-limiter}. Here, we have already implemented the routines
\texttt{isPhysicallyAdmissible} and \\
\texttt{mapDiscreteMaximumPrincipleObservables}.
Both routines have a say in the process of indicating troubled cells.

For running the simulation with dynamic AMR, the only thing that is left is to
switch on \exahype's adaptive mesh refinement 
by providing a $\texttt{maxium-mesh-depth}>0$. We thus augment the original
solver definition as follows:
\begin{code}
  [..]
  solver Limiting-ADER-DG MySolver
    variables          = rho:1,j:3,E:1
    primitives         = 0
    parameters         = 0
    order              = 3
    maximum-mesh-size  = 0.05
    maximum-mesh-depth = 2 // maximum depth of the adaptive mesh 
    time-stepping      = global
    kernel             = generic::fluxes::nonlinear
    language           = C
    limiter-kernel     = generic::godunov
    limiter-language   = C
    dmp-relaxation-parameter = 1e-4
    dmp-difference-scaling = 1e-3
    [..]
  end solver
  [..]
\end{code}

