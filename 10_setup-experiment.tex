\chapter{A new \exahype\,project}
\label{chapter:new-development-project}



\begin{design}
\exahype\ users typically write one code specification per
file experiment plus machine plus setup combination.  
\end{design}

An \exahype\ specification file acts on the one hand as classic
configuration files passed into the executable for a run.
It holds all the constants (otherwise often passed via command lines),
output file names, core numbers, and so forth.
On the other hand, a specification file defines the solver's characteristics
such as numerical scheme used, polynomial order, used postprocessing steps, and
so forth.
Specification files translate the \exahype\ engine into an \exahype\
application.
As they realise a single point-of-contact policy, \exahype\ fosters
reproducability. 
We minimise the number of files required to uniquely define and launch an
application.
As the specification files also describe the used machine (configurations), they
enable \exahype\ to work with machine-tailored engine instances.
The engine can, at hands of the specification, optimise aggressively.
If in doubt, \exahype\ prefers to run the compiler rather than to work with few generic executables.



An \exahype\ specification is a text file where you
specify exactly what you want to solve in which way.
This file\footnote{Some examples can be found in the Toolkit directory or
on the \exahype\ webpage. They have the extension \texttt{.exahype}.}
is handed over to our \exahype\ toolkit, a small python 3 application generating all
required code.
This code (also linking to all other required resources such as parameter
sets or input files) then is handed over to a compiler. 
Depending on the option used, see \ref{chapter:a-toolkit}, you end up with an executable that
 may run without the python toolkit or any additional sources from \exahype.
It however rereads the specification file again for input files or some
parameters, e.g.
We could have worked with two different files, a specification file used to
generate code and a config file, but decided to have everything in one place.


\begin{design}
The specification file parameters that are interpreted by the toolkit are marked
as \texttt{const}. If constant parameters or the structure of the file
(all regions terminated by an \texttt{end}) change, you have to rerun the
toolkit. All other variables are read at runtime, i.e.~you may alter them
without a rerun of the toolkit or a re-compile.
\end{design}

\noindent
If you run various experiments, you start from one specification file and 
build up your application from hereon.
Once the executable is available, it can either be passed the original
specification file or variants of it, i.e.~you may work with sets of
specification files all feeding into one executable.
However, the specification files have to match each other in terms of const
variables and their structure.
\exahype\ itself checks for consistent files in many places and, for many
invalid combinations, complains.


\section{A trivial project}

A trivial project in \exahype\ is described by the following specification file:

\begin{code}
exahype-project  TrivialProject
  peano-kernel-path const    = ./Peano
  exahype-path const         = ./ExaHyPE
  output-directory const     = ./Demonstrators/TrivialProject
  
  computational-domain
    dimension const          = 2
    width                    = 1.0, 1.0
    offset                   = 0.0, 0.0
    end-time                 = 10.0
  end computational-domain
end exahype-project  
\end{code}

\noindent
Most parameters should be rather self-explanatory. You might have to adopt 
the paths\footnote{
You may specify pathes of individual components 
in more detail (cmp.~Section \ref{chapter:finetuning}), but for most
applications working with the three pathes above should be sufficient.}.
Note that \exahype\ supports both two- and three-dimensional
setups.

We hand the file over to the toolkit
\begin{code}
./Toolkit/toolkit.sh Demonstrators/TrivialProject.exahype
\end{code}

\noindent
Finally, we change into this project's directory and type in
\begin{code}
make
\end{code}

\noindent
which gives us an executable. Depending on your system, you might have to change
some environment variables. \exahype\ by default for examples wants to
use an Intel compiler and builds a release mode, i.e.~a highly optimised code
variant. To change this, type in before \texttt{make}:
\begin{code}
export MODE=Asserts
export COMPILER=GNU 
\end{code}

\noindent
All these environment variables are
enlisted by the toolkit, together with recommended settings for your system.
We finally run this first \exahype\ application with 
\begin{code}
./ExaHyPE-TrivialProject TrivialProject.exahype
\end{code} 

\noindent
As clarified before, the specification file co-determines which variant of
the \exahype\ engine is actually built. 
You can always check/reconstruct these settings by passing in \texttt{--version}
instead of the configuration file:
\begin{code}
./ExaHyPE-TrivialProject --version
\end{code}


\section{Solver configuration---an overview}
\label{sec:solver-configuration}

Before we run through particular solvers as life examples, we quickly summarise
\exahype's main solver concepts.  
\exahype\ solves equations of the form 

\begin{equation}\label{eq:general-equation}
  \underbrace{
    \textbf{P}
  }
  _{
    \texttt{materialmatrix}
  }
  \frac{\partial}{\partial t} \textbf{Q}
  +
  \boldsymbol{\nabla}\cdot
  \underbrace{
    \textbf{F}
  }
  _{
    \texttt{fluxes}
  }
  (\textbf{Q})
  +
  \underbrace{
  \sum_{i=1}^{d} \textbf{B}_i (\textbf{Q})\,\frac{\partial \textbf{Q}}{\partial
  x_i} }
  _{\texttt{ncp}}
  = 
  \underbrace{
    \textbf{\textbf{S}(\textbf{Q})}
  }
  _{
    \texttt{sources}
  }
  +
  \underbrace{
    \sum \delta
  }
  _{
    \texttt{pointsources}
  },
\end{equation}

\noindent
and it allows the user to overwrite any solution (locally) and to
invalidate any solution (locally) after each time step.
As most users do not require all terms of the equation, \exahype\ allows you to
switch on/off certain terms. 
This also allows the engine to optimise more aggressively.
Before you start to write your own solver, you have to clarify and specify in
the specification file 
\begin{enumerate}
  \item which terms will be used in your formulation? By default, \exahype\ uses
  $P=id$ and removes all other terms, i.e.~it solves the trivial PDE 
  \[ \frac{\partial}{\partial t} \textbf{Q} = 0. \] This is, most likely, not
  what you want.
  \item which type of solver and integration scheme do you need? At the moment,
  we offer a solver for linear equations and a non-linear one based upon Picard
  iterations. Furthermore, we do
  support Gau\ss-Legendre and Gau\ss-Lobatto integration points for the ADER-DG
  scheme. Gau\ss-Legendre allow exact integration in our code and are the
  default, while Gau\ss-Lobatto nodes are subject to mass lumping yet are useful
  sometimes for imposing boundary conditions. The choice of the shape functions
  is not of interest for Finite Volume schemes.
  \item whether you want to employ particular optimisation methods or you prefer
  a \texttt{generic} solver.
\end{enumerate}


\noindent
While we detail particular solvers in the subsequent chapters, any solver is
embedded into a \texttt{solver} \ldots  \texttt{end solver} environment and has
a unique name. 
Both ADER-DG and Finite Volume solvers always have three main properties
\begin{code}
    type const         = nonlinear
    terms const        = flux
    optimisation const = generic
\end{code}

\noindent
which give answers to the questions raised above.

\subsection{Solver type}
For \texttt{type}, the supported values are given in table \ref{table:solver-types}.
\begin{table}[h]
\begin{center}
 \begin{tabular}{lp{10cm}}
  \hline
  ADER-DG & \\
  \hline
  \texttt{linear} & Is a shortcut for \texttt{linear, Legendre}. \\
  \texttt{nonlinear} & Is a shortcut for \texttt{nonlinear, Legendre}. \\
  \texttt{linear, Legendre} & Kernel for linear PDEs solved on Gauss-Legendre-Nodes    \\
  \texttt{nonlinear, Legendre} & Kernel for nonlinear PDEs, solved on Gauss-Legendre-Nodes\\
  \texttt{linear, Lobatto} & Kernel for linear PDEs solved on Gauss-Lobatto-Nodes \\
  \texttt{nonlinear, Lobatto} & Kernel for nonlinear PDEs solved on Gauss-Lobatto-Nodes \\
  \hline
  Finite Volumes & \\
  \hline
  \texttt{musclhancock} & Use a MUSCL-Hancock Riemann solver. \\
  \texttt{robustmusclhancock} & Use a slightly more robust version of the MUSCL-Hancock Riemann solver. \\
  \texttt{godunov} & Use a standard Godunov Riemann solver.  \\
  \hline
 \end{tabular}
\end{center}
\caption[Parameter table: Solver types]{Solver types. See also figure  \ref{fig:FVPatchGrids} for a comparison of the different nodal basis grids.}\label{table:solver-types}
\end{table}

\noindent
The order of the optional features is irrelevant.
Technically, \exahype\ allows you to pass non-linear PDEs
over to a linear solver, as you define used PDE terms independent of the
solver type, though this does not really make sense.
  
\subsection{Solver PDE terms}\label{sec:solver-pde-terms}
For \texttt{terms}, the supported values are given in table \ref{table:solver-pde-terms}.
\begin{table}[h]
\begin{center}
 \begin{tabular}{lp{10cm}}
 \hline
  \texttt{materialparameters} & If this PDE term is present, \exahype\ allows the
  user to specify a spd matrix $P$. If it is not present, \exahype\ uses $P=id$.
  \\
  \texttt{fluxes} & Informs \exahype\ that you want to use a standard,
  conservative first-order flux formulation.
  \\
  \texttt{ncp} & Informs \exahype\ that you plan to use non-conservative
  formulations.
  \\
  \texttt{sources} & Informs \exahype\ that you plan to use algebraic sources,
  i.e.~right-hand side terms.
  \\
  \texttt{pointSources} & Informs \exahype\ that you plan to use point sources,
  i.e.~Dirac distribution right-hand side terms.
  \\
  \hline
 \end{tabular}
\end{center}
\caption[Parameter table: PDE terms]{Supported solver PDE terms}\label{table:solver-pde-terms}
\end{table}

\noindent
The names correspond to the identifiers in (\ref{eq:general-equation}).
The order of the optional features is irrelevant.
Depending on the chosen terms, the \exahype\ toolkit will create C++/FORTRAN
user classes that have to provide implementations for these terms.

\begin{remark}
  The $\textbf{B}_i$, $\delta $ and $S$ term all may depend on the solution
  $Q$, i.e.~we do not restrict to particular linear term flavours.
\end{remark}

\begin{remark}
  The \texttt{ncp} signatures of \exahype\ allows users to write code that
  technically adds a term $\mathbf{B} \cdot \nabla \mathbf{Q}$, i.e.~a source term to
  the PDE as part of the non-conservative term. While this formally seems to be
  possible, it is usually not a good idea to incorporate sources into the ncp
  formulation. Sources are volume terms, i.e.~do influence ADER-DG's space-time
  predictors but not any Riemann solvers. The ncp term however enters the
  Riemann solve. If you incorporate algebraic sources into ncp, they thus enter
  the Riemann solve and thus yield (usually) invalid solution jump
  contributions.
\end{remark}

Note that we discuss advanced solver features in section
\ref{sec:advanced-solver-features}, notably there is a subsection
about non-conservative systems (section \ref{section:advanted:non-conservative-formulations}) and point sources (section
\ref{section:advanted:non-conservative-formulations}).

\subsection{Solver optimisations}
For \texttt{optimisation}, the supported values are given in table \ref{table:solver-optimisations}.
%
\begin{table}[h]
\begin{center}
 \begin{tabular}{lp{10cm}}
  \hline
  ADER-DG \\
  \hline
  \texttt{generic} & This is our generic implementation of Finite Volumes. This
  is the default if no optimisation is specified.
  \\
  \texttt{optimised} & todo JM \\
  \texttt{usestack} & Put temporary variables such as the space-time predictor
  array on the stack. This might require you to adjust the stack size per
  thread; see Table \ref{03_shared-memory:parameters:configure} for more
  details. \\
  \texttt{maxpicarditer:<int>} & Fix the number of Picard iterations
  the space-time predictor kernel performs.
  \\
  \hline
  Finite Volumes \\
  \hline
  \texttt{generic} & This is our generic implementation of Finite Volumes. This
  is the default if no optimisation is specified.
  \\
  \texttt{optimised} & todo JM \\
  \texttt{usestack} & Put temporary variables such as arrays
  required for imposing boundary conditions on the stack.
  array on the stack. This might require you to adjust the stack size per
  thread; see Table \ref{03_shared-memory:parameters:configure} for more details.
  \\
  \hline
 \end{tabular}
\end{center}
\caption[Parameter table: Solver optimisations]{Solver optimisations}\label{table:solver-optimisations}
\end{table}

\noindent
If you switch to Finite Volumes, not all of the optional features might be
available. 
