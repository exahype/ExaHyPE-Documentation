\chapter{Running \exahype\ on some supercomputers}
\label{sec:apx-supercomputers}


In this section, we collect some remarks on experiences how to use \exahype\ on
particular supercomputers. 

\section{Hamilton (Durham's local supercomputer)}
\label{section:supercomputers:Hamilton}


We have successfully tested \exahype\ with the following modules on Hamilton 7:
\begin{code}
module load intel/xe_2018.2 
module load intelmpi/intel/2017.2
module load gcc/7.4.0
module load python/3.6.8 # for toolkit
\end{code}

\noindent
Given \exahype's size, it is reasonable to use \texttt{/ddn/data/username} as
work directory instead of the home. SLURM is used as batch system and
appropriate SLURM scripts resemble
\begin{code}
#!/bin/bash
#SBATCH --job-name="ExaHyPE"
#SBATCH -o ExaHyPE.%A.out
#SBATCH -e ExaHyPE.%A.err
#SBATCH -t 01:00:00
#SBATCH --exclusive
#SBATCH -p par7.q
#SBATCH --nodes=24
#SBATCH --cpus-per-task=6
#SBATCH --mail-user=tobias.weinzierl@durham.ac.uk
#SBATCH --mail-type=ALL
source /etc/profile.d/modules.sh

module load intel/xe_2018.2 
module load intelmpi/intel/2017.2
module load gcc/7.4.0

setenv I_MPI_FABRICS "tmi"


export I_MPI_FABRICS="tmi"

mpirun ./ExaHyPE-Euler  EulerFlow.exahype
\end{code}

\noindent
For the Euler equations (five unknowns) on the unit square with polynomial order
$p=3$, $h=0.001$ is a reasonable start grid as it yields a tree of depth 8.


Hamilton relies on Omnipath.
Unfortunately, the default fabric configuration of Intel MPI seems not to work
properly for \exahype\ once the problem sizes become big. 
You have to tell MPI explicitly which driver/fabric to use.
Otherwise, your code might deadlock.
One version that seems to work is \texttt{dapl} chosen by
\begin{code}
export I_MPI_FABRICS="dapl"
\end{code}

\noindent
While \texttt{dapl} seems to be very robust, we found it slightly slower than
\texttt{tmi} as used in the script above.
Furthermore, it needs significantly more memory per MPI rank. 
Therefore, we typically use \texttt{tmi} which however has to be set explicitly
via \texttt{export} on Hamilton.


One of the big selling points of Omnipath is that it is well-suited for small
messages sizes.
Compared to other (Infiniband-based) systems, it thus seems to be wise to reduce
the package sizes in your \exahype\ specification file. 
Notably, we often get improved performance once we start to decrease
\texttt{buffer-size}.


\section{SuperMUC (Munich's petascale machine)}
\label{section:supercomputers:SuperMUC}

There are very few pitfalls on SuperMUC that mainly arise from the interplay 
of IBM's MPI with Intel TBBs as well as new compiler versions. Please load 
a recently new GCC version (Intel by default uses a too old version) as well 
as TBBs manually before you compile
\begin{code}
module switch intel/18.0
module switch tbb/2018
module switch gcc/5
\end{code}

\noindent
and remember to do so in your job scripts, too:
\begin{code}
#!/bin/bash
#@ job_type = parallel
##@ job_type = MPICH
#@ class = micro
#@ node = 1
#@ tasks_per_node = 1
#@ island_count = 1
#@ wall_clock_limit = 24:00:00
#@ energy_policy_tag = ExaHyPE_rulez
#@ minimize_time_to_solution = yes
#@ job_name = LRZ-test
#@ network.MPI = sn_all,not_shared,us
#@ output = LRZ-test.out
#@ error =  LRZ-test.err
#@ notification=complete
#@ notify_user=tobias.weinzierl@durham.ac.uk
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh
module switch intel/18.0
module switch tbb/2018
module switch gcc/5
\end{code}

\noindent
If you use Intel's TBB in combination with \texttt{poe} or MPI, please ensure
that you set
\begin{code}
export OMP_NUM_THREADS=28
export MP_TASK_AFFINITY=core:28
\end{code}

\noindent
manually, explicitly and correctly before you launch your application.
If you forget to do so, \exahype's TBB launches the correct number of TBB
threads as specified in your \exahype\ specification file, but it pins all of
these threads to one single core.
You will get at most a speedup of two (from the core plus its hyperthread) in
this case\footnote{Thanks to Nicolay Hammer from LRZ for identifying this
issue.}.

 


%\section{Tornado KNL (RSC group prototype)}



\section{Archer's KNL partition (EPCC supercomputer)}
\label{section:supercomputers:archer}

Archer's default Java version does not meet \exahype's requirements and the Java
configuration does not provide the toolkit with enough heap memory (cf.~Section
\ref{section:appendix-toolkit:troubleshooting}).
Furthermore, we haven't used the Cray tools yet but stick to Intel and there
have to load a well-suited GCC version manually:

\begin{code}
module load java/jdk1.8.0_51
module swap PrgEnv-cray PrgEnv-intel
module load gcc
\end{code}

\noindent
To accomodate the toolkit, we use the modified Java invocation:
\begin{code}
java -XX:MaxHeapSize=512m -jar Toolkit/dist/ExaHyPE.jar
\end{code}


\noindent
For shared memory support, we encountered three issues:
\begin{enumerate}
  \item We should use EPCC's compiler macro \texttt{CC} instead of manually
  invocations of the compilers.
  \item The module does not initialise the \texttt{TBB\_SHLIB} variable that we
  use in our script. So we have to set it manually.
  \item The default compiler behaviour links all libraries static into the
  executable. However, the TBB libs are not available in their static variant.
  To change this behaviour, we had to instruct the linker explicitly to link
  against shared memory library variants.
\end{enumerate}

\noindent
Overall, these three lines fix the behaviour:
\begin{code}
  export EXAHYPE_CC=CC
  export TBB_SHLIB="-L/opt/intel/compilers_and_" \
    "libraries_2017.0.098/linux/tbb/lib/intel64/gcc4.7 -ltbb"
  export CRAYPE_LINK_TYPE=dynamic
\end{code}


\noindent
Similar to SuperMUC, we observe that a plain launch of executables through
\texttt{aprun} {\em does not allow the codes to exploit shared memory
parallelism}. We explicitly have to unlock the cores for the scripts in the run
command through
\begin{code}
  aprun -n ... -d coresPerTask ... -cc depth
\end{code}
where \texttt{-cc} configures the pinning. According to the Archer
documentation, this configuration still does not enable hyperthreading. 
If hyperthreading is required, we have to append \texttt{-j 4} to the
invocation, too.

\section{RWTH Aachen Cluster}

We have successfully tested \exahype\ on RWTH Aachen's RZ clusters using MUST.
Here, it is important to switch the GCC implementation before you compile, as
GCC is by default version 4.8.5 which does not fully implement C++11.

\begin{code}
module load UNITE must


#module unload gcc
module unload openmpi
module switch intel gcc/5
module load intel openmpi

	
export SHAREDMEM=none
export COMPILER=manual
export EXAHYPE_CC="mpiCC -std=c++11 -g3"
export COMPILER_CFLAGS="$FLAGS_FAST"
\end{code}

\noindent
The above setups use the compiler variant \texttt{manual} as RWTH has installed
MUST such thatight \texttt{mustrun} automatically throws the executable onto the
right cluster. 
To create a binary that is compatible with this cluster, the flags from
\texttt{FLAST\_FAST} are to be used. 


\section{CoolMUC 3}

LRZ's KNL system CoolMUC 3 drives Omnipath as well. Therefore, ensure that you
set the MPI fabric properly as soon as you use more than one node. Otherwise,
\exahype\ will deadlock:

\begin{code}
export I_MPI_FABRICS="tmi"
\end{code}



\section{Hazelhen (Cray)} 

Cray may configure the intel compiler to link in all libraries statically
but TBB by default is not built statically so add the following to the
\texttt{TBB\_SHLIB}
\begin{code}
-dynamic -ltbb
\end{code} 
i.e. before the link command. The environment used is as follows

\begin{code}

module switch PrgEnv-cray PrgEnv-intel 

export COMPILER="Intel"
export DISTRIBUTEDMEM=MPI
export EXAHYPE_CC="CC"
export EXAHYPE_FC="ftn"
export MODE="Release"
export SHAREDMEM=TBB
export TBB_PATH=$INTEL_PATH/tbb
export TBB_INC="-I${TBB_PATH}/include"
export TBB_SHLIB="-L${TBB_PATH}/lib/intel64/gcc4.7 -ltbb" 
export CRAYPE_LINK_TYPE=dynamic

\end{code}

To run a simulation on Hazelhen, the following are required for the
submit script, in addition to the normal variables
\begin{code}
#PBS -m abe
#PBS -l nodes=5:ppn=24
#PBS -q test


module switch PrgEnv-cray PrgEnv-intel 
module swap intel intel/18.0.1.163


export TBB_PATH=$INTEL_PATH/tbb
export TBB_INC="-I${TBB_PATH}/include"
export TBB_SHLIB="-L${TBB_PATH}/lib/intel64/gcc4.7 -dynamic -ltbb" 
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$TBB_PATH/lib/intel64/gcc4.7

export KMP_AFFINITY=disabled

aprun -cc none -n 30 -N 6 -d 4 ./ExaHyPE $SPECFILE
\end{code}

where $n$ is the number of MPI ranks, $N$ is the number of
MPI ranks-per-node, $d$ is the number of threads. $n$ should be equal 
to $N\times\text{number of nodes}$ while $N*d$ should be 24 (or whatever 
ppn is).




\section{Frankfurt machines}
For the machines

\begin{itemize}
  \item Generic Ubuntu laptop
  \item Iboga
  \item LOEWE
  \item FUCHS
  \item Hamilton
  \item SuperMUC
\end{itemize}

please see the configuration settings in the \texttt{ClusterConfigs}
directory in the \texttt{Miscellaneous} in the main repository.

\section{git on clusters}

Some compute clusters, such as SuperMUC or Hazelhen, do not allow access
to the outside world but instead require sshing in through known
machines. To access git through these machines do the following.
On your local machine add the following to the \texttt{.ssh/config} file
\begin{code}
RemoteForward $PORT_NUM gitlab.lrz.de:22
\end{code}
where you specify \texttt{PORT\_NUM} as a port number. Then on the compute
machine, create a new ssh key and add it to your profile on Gitlab. Then
add the following to the \texttt{.ssh/config} file
\begin{code} 
IdentitiesOnly yes 

Host gitlab.lrz.de
  Hostname localhost
  Port $PORT_NUMBER
  User $USERNAME
  IdentityFile ~/.ssh/$PRIVATE_KEY
\end{code}
where \texttt{USERNAME} and \texttt{PRIVATE\_KEY} are your username
required to log into the gitlab and private key file respectively.

