\chapter{Postprocessing}
\label{chapter:postprocessing}


\section{On-the-fly computation of global metrics such as integrals}
\label{section:global-integrals-computing}

The plotter sections in the specification file allow you to write
\texttt{unknowns=0}.
For this exercise use for example 
In this case, the toolkit creates a plotter and hands over to this plotter all
discretisation points.
However, it neglects all return data, i.e.~nothing is plotted.


To compute the global integral of a quantity, you might want to use the
\linebreak 
\texttt{vtk::Cartesian::ascii} plotter but set the unknowns to zero. 
Whenever your plotter becomes active, \exahype\ calls your
\texttt{startPlotting} operation.
Add a local attribute $m$ to your class and set it to zero in the start
function.
In your conversation routines, you can now accumulate the L2 integral in
$m$, while you use \texttt{finishPlotting} write the result to the terminal or into a
file of your choice, e.g.
If you alter the start and finish routine, please continue to call the parent
operation, too.

We illustrate the realisation at hands of a simple computation of the global
$L_2$ norm over all quantities of a simulation. 
For this, we first add a new variable to the class:
\begin{code}
class MyEulerSolver_Plotter0: public ...
  private:
    // We add a new attribute to the plotter. 
    double _globalL2Accumulated;
  public:
    MyEulerSolver_Plotter0();
    virtual ~MyEulerSolver_Plotter0();
...
\end{code}

\noindent
Next, we set this quantity to zero in the plotter initialisation, we accumulate
it in the mapping operation and we write the result to the terminal when the
plotting finishes.
\begin{code}
void MyEulerSolver_Plotter0::startPlotting(double time) {
  // Reset global measurements
  _globalL2Accumulated = 0.0;
}


void MyEulerSolver_Plotter0::finishPlotting() {
  _globalL2Accumulated = std::sqrt(_globalL2Accumulated);
  std::cout << "my global value = " << _globalL2Accumulated << std::endl;
}


void MyEulerSolver_Plotter0::mapQuantities(
    const tarch::la::Vector<DIMENSIONS, double>& offsetOfPatch,
    const tarch::la::Vector<DIMENSIONS, double>& sizeOfPatch,
    const tarch::la::Vector<DIMENSIONS, double>& x,
    double* Q,
    double* outputQuantities,
    double timeStamp
) {
  // There are no output quantities
  assertion( outputQuantities==nullptr );
  // Now we do the global computation on Q but we do not write anything 
  // into outputQuantities
  const NumberOfLagrangePointsPerAxis = 4; // Please adopt w.r.t. order 
  const NumberOfUnknownsPerGridPoint  = 5; // Please adopt

  // This is the correct scaling for FV, but not DG.
  double scaling = tarch::la::volume( 
    sizeOfPatch* (1.0/NumberOfLagrangePointsPerAxis) 
  ); 
  for (int iQ=0; iQ<NumberOfUnknownsPerGridPoint; iQ++) {
    _globalL2Accumulated += Q[iQ] * Q[iQ] * scaling;
  }
}
\end{code}

\noindent
As we have set the number of plotted unknowns to zero, no output files are
written at all.
However, all routines of \texttt{MyEulerSolver\_Plotter0} are invoked, i.e.~we
can compute global quantities, e.g.
Some remarks on the implementation:
\begin{itemize}
  \item It would be nicer to use the plotter routines of Peano when we write
  data to the terminal. This way, we can filter outputs via a filter file,
  i.e.~at startup, and Peano takes care that data from different ranks is piped
  into different log file and does not mess up the terminal through concurrent
  writes.
  \item Most $L_2$ computations do scale the accumulated quantity with
  $\frac{1}{N}$ where $N$ is the number of data points. Such an approach however
  fails for adaptive grids. If we scale each point with the mesh size, we
  automatically get a discrete equivalent to the $L_2$ norm that works for any
  adaptivity pattern. The \texttt{volume} function computes $h^d$ for a
  vectorial $h$. See the documentation in \texttt{tarch::la}.
  \item The abovementioned version works if you use a Cartesian plotter where
  \exahype's solution already is projected onto regular patches within each cell
  (subsampling). There are other plotters that allow you to evaluated the
  unknowns directly in the Lagrange points. The scaling of the weights then
  however has to be chosen differently.
  \item Plotting is expensive as \exahype\ switches off multithreading for
  plotting always.
  It thus makes sense not to invoke a plotter too often---even if you can handle
  the produced data of a simulation.
\end{itemize}




\section{Reduction of global quantities over all MPI ranks}

The code snippets so far are unaware of any MPI parallelisation.
\exahype\ does disable any shared memory parallelisation if you 
decide to plot---so ensure that there is a reasonable time in-between any two
plots, even if they do only derive a single scalar---but it does not
automatically synchronise the plotters between any two MPI ranks at any time.

We try to make \exahype\ minimalistic and easy to handle, maintain and learn. 
Predefined reduction routines---reduction means all MPI ranks combine their
results into one final piece of data on one rank---would contradict this
objective and require us to come up with a comprehensive list of possibly
required reductions. So we decided to make the programming of reductions simple
rather than offering as many as possible reductions out-of-the-box.

Reducing data from all MPI ranks is a popular task in distributed memory
programming.
Therefore, MPI offers a collective operation to do this.
The term collective here means that all MPI ranks are involved.
The term collective also reasons why we may not use MPI collectives in \exahype. 
Our dynamic load balancing, notably in combination with dynamic mesh refinement,
may remove MPI ranks from the computation at any time; just to use them for
other tasks later on that are urgent.
You can never rely that really all MPI ranks do work (though most of the time,
all of them will do).
As a result, we have to program the reduction manually.
We discuss how to do this by means of the summation of values from all 
plotters running on MPI ranks at a certain time.

\begin{itemize}
  \item If you have code parts that you want
   not to be translated if no MPI is activated, protect it via
   \begin{code}
#ifdef Parallel
...
#endif
   \end{code}  
  \item \exahype\ runs plotters only on ``active'' MPI ranks. If an MPI rank
   currently is not used by the (dynamic) load balancing, no plotter ever is
   started on this rank. At the same time, \exahype's Peano code base identifies
   a {\em global master} rank. This rank is always working. If you have to
   reduce data, it is convenient always to reduce all data on the global master.
   At any point, you can find out whether you are on the global master via
   \begin{code}
#include "tarch/parallel/Node.h"
...
if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
  ...
}
   \end{code}  
   Please note that \texttt{isGlobalMaster()} is always defined. If you run
   without MPI, it always returns \texttt{true} as there is only one instance of
   \exahype\ running.
  \item Peano, which is \exahype's grid management engine, uses literally
   hundreds of different message types running through the network at any time
   simultaneously. It is important that we do not interfer with any \exahype\
   core messages if we postprocess data. To ensure this, we propose to {\em tag}
   messages, i.e.~to give them a label to say ``these are for my
   postprocessing'' only. Peano offers a tagging mechanism that we use here.
\end{itemize}

\noindent
Our proposed reduction now realises the following three ideas:
\begin{enumerate}
  \item We do all the reduction at the end of \texttt{finishPlotting} where we
   can assume that the reduced data per rank is available.
  \item If we run on any rank that is not the global master, 
   \texttt{finishPlotting} sends its data to the global master.
  \item The global master's \texttt{finishPlotting} runs over all ranks that are
   not idling around and collects the data from them.
\end{enumerate}


\begin{code}
#include "tarch/parallel/Node.h"
#include "tarch/parallel/NodePool.h"

...

void MyEulerSolver_Plotter0::finishPlotting() {
 // All the stuff we have done before comes here
 // ...
 const int myMessageTagUseForTheReduction = 
  tarch::parallel::Node::getInstance().reserveFreeTag(
   "MyEulerSolver_Plotter0::finishPlotting()" );

 double myValue = ...;

 if (tarch::parallel::Node::getInstance().isGlobalMaster()) {
  double receivedValue;
  for (
   int rank=1; 
   rank<tarch::parallel::Node::getInstance().getNumberOfNodes(); rank++
  ) {
   if ( !tarch::parallel::NodePool::getInstance().isIdleNode(rank) ) {
    MPI_Recv( &receivedValue, 1, MPI_DOUBLE, rank,
     myMessageTagUseForTheReduction,
     tarch::parallel::Node::getInstance().getCommunicator(), 
     MPI_STATUS_IGNORE );
    myValue += receivedValue; 
   }
  }
 }
 else {
  MPI_Send( &myValue, 1, MPI_DOUBLE,
   tarch::parallel::Node::getGlobalMasterRank(),
   myMessageTagUseForTheReduction,
   tarch::parallel::Node::getInstance().getCommunicator()
  );
 }

 tarch::parallel::Node::getInstance().releaseTag(
  myMessageTagUseForTheReduction);
}
\end{code}

The \texttt{reserveFreeTag} operation stems from Peano. We are supposed to tell
the function which operation has invoked it, as this is a quite useful piece of 
information for MPI debugging. 
The last line of the code snippet returns this tag again, so it might be reused
later on.
Our snippet demonstrates the reduction at hands of the double variable
\texttt{double}. 
An extension to non-double and vector types should be straightforward for
developers having rudimentary knowledge of MPI.

The class \texttt{Node} is a singleton---it exists only once---representing an
MPI rank.
The class \texttt{NodePool} is a singleton, too, and holds all of Peano's 
load balancing information.
Notably is knows which ranks are idling.
We use those two classes here.

Our code snippet sends the value of \texttt{myValue} to the global master if we
run into the routine on a rank which is not the global master itself.
If the plotter is invoked on the global master, we run over all the ranks that
do exist.
Per rank, we ask the node pool whether the rank is idling. 
If it is not, we know that it will send data to the global master and we receive
these data.



% 
% However, in many applications it is neccessary to programmatically access the data. As VTK defines not only a file format but also a programming standard (class interface), there is a straight forward way to process the data generated by \exahype. It is that the VTK files produced by \exahype contain an \emph{Unstructured Grid} which is basically a set of points with scalar fields on each point. Each conserved quantity (\texttt{numberOfVariables} and \texttt{numberOfParameters} in \exahype) corresponds to one scalar field in the output. It is thus possible to display the output as a table with a huge number of row entries, with columns
% \begin{code}
% t x y z Q0 Q1 Q2 Q3 Q4 ...
% \end{code}
% 
% The next section will contain a primer how to access and inspect the VTK files.
% 
% 
% This section shall contain a small C++ or Python hands on how to read VTK files and plot a 1D slice with some trivial postprocessing, as applying a constant shift and applying a time rescaling, using Matplotlib or gnuplot. This can be done in less than 20 lines of code.
% 
