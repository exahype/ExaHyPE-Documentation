\chapter{Distributed memory parallelisation}

\exahype's distributed memory parallelisation is done with plain MPI.
We rely on the 1.3 standard with the pure C bindings through there are MPI-2
variants available (cmp.~Peano's documentation).
This chapter provides an MPI hitchhiker's guide through MPI, before we detail
\exahype's MPI specifica.

\section{An hitchhiker's guide through MPI}


\subsection*{Prepare build}
To make an \exahype\ project
use MPI, please add a \texttt{distributed-memory} configuration block to your
specification file:

\begin{code}
  distributed-memory
    identifier               = static_load_balancing
    configure                = {greedy-naive,FCFS}
    buffer-size              = 64
    timeout                  = 120
  end distributed-memory
\end{code}

\noindent
The above fragment is minimal and thus well-suited for a first try.
Now either rerun the \exahype\ toolkit and recompile your code, or 
\begin{code}
  export DISTRIBUTEDMEM=mpi
\end{code}
and recompile. 
There's always the opportunity to switch MPI on/off through \texttt{DISTRIBUTEDMEM}.
Depending on whether the \texttt{distributed-memory} segment in the
specification file is available, MPI on or off is the default.
For the compilation, we assume that a proper MPI compiler is available.
You can reconfigure it through \texttt{EXAHYPE\_CC}.


% Whenever you configure your project with distributed memory support, the {\bf
% default} build variant is a distributed memory build. 
% However, you always are able to rebuild without distributed memory support by manually redefining environment variables
% and by rerunning the makefile.
% There is no need to rerun the toolkit again.
% Also note that all arguments within the \texttt{distributed-memory} environment
% are read at startup time. 


\pagebreak

\subsection*{Prepare a benchmark simulation setup}

Before you start to do any MPI experiments, we strongly recommend that
\begin{enumerate}
  \item you switch off any output unless it is the very primitive VTK/VTU
  output stuff we ship with \exahype.
  Adding output is a second thing you can do once your application scales to a certain number of nodes/cores. But starting
  with IO enabled right from the start makes, in our experience, things way more
  complicated---notably the identification and isolation of issues. The
  straightforward way to disable any output is to set the value of the
  \texttt{time} attribute, i.e.~the first time a file should be written, to
  something bigger than the total simulation time.
  \item you change your grid resolutions such that your simulation fits onto
  exactly one node of your cluster without exceeding the memory. It is very
  handy to have a reasonable baseline that still runs serially while it is as
  large as possible to see scalability issues early. To obtain information about
  your code's memory consumption, recompile with \texttt{-DTrackGridStatistics},
  i.e.~add it to \texttt{PROJECT\_CFLAGS} of your application's makefile.
  Furthermore, ensure that your log filter file (if you use one) does not filter
  out the messages and contains
  \begin{code}
info  exahype::runners::Runner::runAsWorker      -1  white
info  exahype::runners::Runner::createRepository -1 white
  \end{code}
  \item you start with a regular grid setup. This way, you can first eliminate
  technical difficulties before adaptivity and load balancing kick in.
  \item you stick to two dimensions if possible. It just makes many things
  (including runtimes) easier to handle.
\end{enumerate}

\noindent
Fixing MPI codes can be tedious. 
It is thus advantageous to have one minimal setup available to study \exahype's
behaviour.


\subsection*{First run \& quick checks}

I strongly recommend to do three runs first before you start any MPI
experiments:

\begin{enumerate}
  \item {\bf Run with \texttt{mpirun -n 1}}. Just to ensure that the sole
  compilation has not ruined anything in your code. Please ensure: Does the
  simulation fit into the memory? MPI will blow up your memory footprint already
  in this stage.
  \item {\bf Run with \texttt{mpirun -n 2}}. \exahype\ follows Peano's MPI
  paradigm where the first rank is exclusively reserved for algorithm control
  and load balancing. This implies that a two-rank run should behave exactly as
  a run with only one rank: the first rank does all the admin work, the second
  rank does the real computation. If you don't want to sacrifice a complete
  rank, almost all MPI installations give you the opportunity to overbook one
  node, i.e.~to launch more than one rank on the first node. It your code fails
  already in this stage, often people have written bugs into their global
  reduction.
  \item {\bf Run with \texttt{mpirun -n K}} where \texttt{K} is either ten for
  two-dimensional setups or 28 for three-dimensional problems. \exahype\ is
  based upon three-partitioning of grids. With one rank reserved for load
  balancing and administration $1+3^d$ is a core count that makes it very easy
  for any \exahype\ configuration to distribute the grid already at startup.
  If the simulation hangs, doublecheck the memory requirements of the individual
  ranks, i.e.~whether they can be accommodated by your cluster. If possible, it
  makes sense to issue the first run with 10 or 28 ranks on one node. In this
  case, MPI falls back to shared memory data exchange, i.e.~buffer locking
  effects are avoided. 
\end{enumerate}


\noindent
From hereon, \exahype\ is prepared to scale up the problem size as well as the
grid resolution.



\subsection*{The upscaling cycle}

We recommend to scaleup \exahype\ in an iterative scheme that reads as follows:
\begin{enumerate}
  \item Increase the problem size {\bf or} increase the core count {\bf or}
  change the \exahype\ specification settings/problem characteristics.
  \item Conduct a first test. If the scalability is satisfying, continue with 1.
  Otherwise proceed as follows.
  \item Follow Chapter \ref{chapter:profiling} and notably Sections
  \ref{section:profiling:Peano-plain} and
  \ref{section:profiling:Peano-advanced}.
  \item Search for inefficiency patterns in the output (cmp.~Section
  \ref{section:mpi:inefficiency-patterns}), alter your code settings and return
  to 1.
\end{enumerate}


\section{Buffer and timeout settings}
\label{section:mpi:buffer-timeout-settings}

The parameter \texttt{timeout} specifies how long a node shall wait (in seconds)
before it triggers a time out if no message has arrived.
If half of the time has elapsed, it furthermore writes a warning. 
Set the value to zero if you don't want to use the time out detection.

\begin{design}
Peano, \exahype's AMR code base, introduces timeouts to allow you to 
identifiy deadlocks without you burning too many CPUhs as well as communication
inefficiencies.
We recommend to start with rather restrictive timeout settings and to scale the
timeout with the grid size.
\end{design}

\noindent
The parameter \texttt{buffer-size} specifies how many messages Peano shall
internally bundle into one MPI message.
This operation allows you to tailor your application behaviour w.r.t.~latency
and bandwidth requirements. 
The fewer messages you bundle, i.e. the small this value, the faster messages
are sent out and other ranks might not have to wait for them.
The more messages you bundle the lower the total communication overhead.
Please note that some Infiniband implementations tend to deadlock, if this value
is too small as they are swamped with lots of tiny messages. We found 64 to be a
reasonable first try. To bundle all messages into one MPI message use a 
comparably high buffer size (e.g. 16000).



\section{Configuring the load balancing}


%\texttt{identifier} and \texttt{configure} settings
Through \texttt{identifier}, \exahype\ users set the load balancing paradigm
used. 
We support
\begin{center}
 \begin{tabular}{|lp{8cm}|}
  \hline
  \texttt{identifier} & {\bf Semantics} \\
  \hline
  \texttt{static\_load\_balancing}
    & A static load balancing that decomposes \exahype's underlying spacetree
    at construction time. The grid decomposition is not altered afterwards, even
    if the grid's structure changes.
    \\
  \hline
 \end{tabular}
\end{center}


\noindent
Besides the identifier, \exahype\ needs entries in the \texttt{configure}
field.
A configuration is simply a comma-separated set of identifiers---written down
with curly brackets---that control how the chosen load balancing strategy is
behaving.
There are to be two mandatory entries in the configuration: the load
balancing analysis (Table \ref{table:mpi-static:analysis}) and the load
assignment strategy (Table \ref{table:node-pool-strategy}).
So you need exactly one entry from each of the two tables. 
Further optional configuration entries might complete the \texttt{configure}
set.


\begin{table}[htb]
\caption[MPI load balancing metrics]{
  \exahype\ supports various metrics, i.e.~ways the load balancing is
  guided. We recommend to start with \texttt{greedy-naive} always.
}
\label{table:mpi-static:analysis}
\begin{center}
\begin{tabular}{|lp{10cm}|}
  \hline
  {\bf Argument}  & {\bf Semantics} \\
  \hline
  \texttt{greedy-naive} & Uses a greedy split strategy to decompose the
  underlying spacetree. 
  The
  individual ranks do not synchronise with each other, i.e.~one rank is
  unaware of load imbalanced on another rank.
  This scheme is fast and naive but might yield non-optimal partitions right
  from the start.
  \\
  \texttt{greedy-regular} & Extension of \texttt{greedy-naive} that takes into
  account that grids are (rather) regular. These regular grid levels then are
  distributed aggressively. Very fast.
  \\
  \texttt{hotspot} & This variant is the most conservative one and introduces quite some grid
  construction overhead as the load balancing has to plug into the grid
  construction. It however yields better partitions than the greedy
  partitioning scheme if you use AMR or your rank count is not an exact match
  to the regular grid (something alike $3^{3kd},\ k \in \mathbb{N}^+$), as it
  searches for refined mesh regions within the grid and splits those up more
  aggressively than the remainder. Please note that this strategy works best if
  combined with \texttt{fair} or the SFC-based diffusion plus additional
  information how the ranks are distributed among the nodes (cmp.~Table \ref{table:node-pool-strategy}).
  \\
  \hline
\end{tabular}
\end{center}
\end{table}





\begin{table}[htb]
\caption[MPI global node pool configuration]{Configuration of the global node pool, i.e.~how \exahype\ decides
which ranks to use next.}
\label{table:node-pool-strategy}
\begin{center}
\begin{tabular}{|lp{11.5cm}|}
  \hline
  {\bf Argument}  & {\bf Semantics} \\
  \hline
  \texttt{FCFS} & If an MPI rank want to split its domain, it sends a
  request for an additional MPI rank to rank 0. All requests on rank 0 are
  answered in FCFS fashion which minimises the answer latency but might yield
  unfair decompositions: ranks with a very low latency towards rank 0 are more
  likely to be served than others. 
  You may not combine \texttt{FCFS} and \texttt{fair}.
  \\
  \texttt{fair} & All requests sent to rank 0 are collected for a couple
  of ms and then answered such that those ranks with the lowest number of
  workers so far get new workers first. The answering latency is slightly higher
  than for \texttt{FCFS} but the distributions tend to be fairer. This variant 
  expects the user to specify \texttt{ranks-per-node:X}, too, such that the
  load balancing knows how the ranks are distributed first. If $n$ ranks request for one worker each for a level $\ell $, the load
  balancing tries to make each node, i.e.~every \texttt{ranks-per-node}th rank,
  serve this request. Notably, the ranks responsible for the coarsest tree
  levels are assigned to the ranks \texttt{ranks-per-node},
  $2 \cdot $ \texttt{ranks-per-node}, $3 \cdot $ \texttt{ranks-per-node}, and so
  forth. The idea is that jobs are homogeneously distributed among the nodes and
  no node runs risk to run out of memory. Furthermore, if the grid is
  reasonably regular, also the load should be reasonably distributed among the
  nodes.
  The fair strategy can be tailored further by a flag
  \texttt{max-node-pool-answering-time:x} which tells the strategy how many
  seconds to wait until any answer is given, i.e.~how long to wait for someone
  else to raise demands, too. If it is not specified, \exahype\ falls back to a
  default value.\footnotemark
  \\
  \texttt{sfc-diffusion} & This is an extension of the \texttt{fair} strategy
  where we try to align all ranks along the Peano space-filling curve. To allow
  us to do this, we have to know the \texttt{ranks-per-node:X} settings.
  \texttt{ranks-per-node} and the total rank count have to match, i.e.~the
  total count modulo ranks per node has to be zero.
  Further, the strategy also needs \texttt{primary-ranks-per-node:Y} with
  $X\geq Y$.
  All requests sent to rank 0 are collected for a couple
  of ms whenever they drop in and then answered such that the load is
  distributed evenly among the nodes.
  Hereby, only \texttt{primary-ranks-per-node} ranks on each single node are
  used. 
  Once all primary ranks are distributed, the strategy switches into another
  state and now uses the remaining ranks so make the SFC be split such that each
  node receives roughly the same amount of work.
  The strategy has been developed to be used in combination with shared memory
  parallelisation and the \texttt{hotspot} balancing.
  The diffusion strategy can be tailored further by a flag
  \texttt{max-node-pool-answering-time:x}, too. See the comments for the
  \texttt{fair} strategy.
  \\
  \hline
\end{tabular}
\end{center}
\end{table}


\footnotetext{
  If the fair or SFC-based load balancing does not yield proper domain
  decompositions, please try first to increase the 
  \texttt{max-node-pool-answering-time:x} value. Also, please note that all
  flags are written with a - rather than an underscore.
}






\section{Hybrid parallelisation}

Please see Section \ref{section:20:hybrid} for details how MPI and the shared
memory parallelisation interplay.

If you use the strategy \texttt{sfc-diffusion}, we recommend that you use $p$
primary ranks per node.
If a node has $c$ cores, ensure $c \mod p = 0$.
For $c=24$ (and our first KNL tests), we have made good experience with
$p=\frac{c}{t}$ and $t=4$.
In this case, we will obtain $p \leq r \leq c$ working MPI ranks $r$.
Some of these will, through the meander pattern of the SFC, become responsible
for larger cubic regions of the domain, while others only are responsible for
tiny tails of the curve.
If thus makes sense to configure the code to use $t$ threads per rank.



\section{MPI grid modifications}
\label{section:mpi:grid-modifications}


MPI communication is very sensitive to the grid layout. \exahype\ can minimise
data exchange with the critical first rank which controls the whole simulation
workflow (and thus quickly becomes a bottleneck) by slightly stretching and
realigning the grid. To switch on this feature, add
\texttt{virtually-expand-domain} to your configuration:
\begin{code}
configure = {...,virtually-expand-domain}
\end{code}

\begin{figure}
\begin{center}
  \includegraphics[width=0.33\textwidth]{sketches/scale-bounding-box-1.pdf}
  \includegraphics[width=0.33\textwidth]{sketches/scale-bounding-box-1_3.pdf}
  \includegraphics[width=0.33\textwidth]{sketches/scale-bounding-box-1_9.pdf}
\end{center}
  \caption[Scaling the bounding box]{The bounding box is scaled such that one cell lies outside of the 
  computational domain $\Omega$. The bounding box is partitioned into $3\times3$ equal parts. 
  Left to Right: The subdomain sizes become more equal for increasing mesh resolution.}\label{fig:expand-domain}
\end{figure}
 
\noindent
If you have a grid that is very regular along the bounding box (sketch above,
left), all ranks synchronise with rank 0 each and every time step as rank 0
holds knowledge about changes along the global domain boundary.
This renders rank 0, who otherwise holds no compute data at all, a bottleneck.
With the additional flag, the compute grid is slightly enlarged and thus the
ranks responsible for boundary cells have all boundary knowledge local---if a
cell overlaps the boundary, then all boundary conditions are evaluated locally;
problems arise, if the computational boundary coincides with the grid
boundaries.


While a virtual expansion speeds up some codes dramatically, its impact on the
chosen mesh size and load balancing has to be studied carefully.
Mesh sizes are modified, global cell counts may change as refinement criteria
yield slightly different grids, and the load balancing becomes different and
harder:
Grids that did fit to a certain rank count brilliantly before might now be grids
that are very hard to decompose reasonably.
In general, we thus recommend to use this feature for large MPI rank
counts only where the high number of ranks gives \exahype\ the freedom to find
proper load balancings. 
Furthermore, we recommend that you use the a non-greedy load
metric if you switch this feature on (\texttt{hotspot}, e.g.).

\subsection{Meshes for weak and strong scaling}

\exahype\ distributes work by decomposing the tripartitioned spacetree into
subtrees that are deployed to worker processes.
Only subtrees that overlap with the computational domain are deployed.
This constraint can be used to steer work distribution.

\exahype\ can scale the bounding box such that \texttt{outside\_cells\_left} and/or \\
\texttt{outside\_cells\_right} cells are placed outside of the 
domain while the latter is still resolved as accurately as specified in the spec file.

Furthermore, there is an option to place exactly \texttt{ranks\_per\_dimension} on the coarse grid. 
(Note: This overrules \texttt{outside\_cells\_right} but not \texttt{outside\_cells\_left}.) 
This feature is particular interesting for weak scaling experiments as it can scale the number of cells per dimension
of a mesh by arbitrary integers.
An example is given in Fig. \ref{fig:weak-scaling}.

\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.4\textwidth]{sketches/weak-scaling-1.pdf}\hspace{0.1cm}
  \includegraphics[width=0.4\textwidth]{sketches/weak-scaling-2.pdf}
  \\
  \vspace{0.1cm}
  \includegraphics[width=0.4\textwidth]{sketches/weak-scaling-4.pdf}\hspace{0.1cm}
  \includegraphics[width=0.4\textwidth]{sketches/weak-scaling-8.pdf}
\end{center} 
\caption{Weak scaling experiment growing the domain by factors $1$, $2^d$, $4^d$, and $8^d$. \texttt{ranks\_per\_dimension} is chosen 1,2,4,8.
The majority of cells lying outside of the domain will be erased.}
\label{fig:weak-scaling}
\end{figure}

The bounding box scaling controls can be used to create interesting meshes like
the $36^d$ cell mesh shown in Fig. \ref{fig:strong-scaling}. This mesh was created with 9 outside cells
on the left side and 12 ranks per dimension. The mesh
is interesting since it can be perfectly distributed among
$2^d$, $4^d$, and $12^d$ processes.

The same mesh refined by a factor $3^i$, $i \leq 0$, is constructed by placing $3^i \times 9$ bounding box 
cells left to the domain. The $36^d$ mesh requires a maximum-mesh-size of $\frac{1}{36} \approx 0.03$.
For further uniform refinement, this mesh size must be scaled by a factor $3^{-i}$.

Another family of interesting uniform meshes are the ones with $3^i \times 48^d$ cells that are shifted
by $3^i \times 3$ cells. They can be created by choosing $\texttt{ranks\_per\_dimension}=16$
and $\texttt{outside\_cells\_left}=1$. The latter must be multiplied by a factor $3^i$ for any
additional refinement level to preserve the positioning.
The $48^d$ mesh requires a maximum-mesh-size of $\frac{1}{48} \approx 0.021$.
For further uniform refinement, the mesh size must be scaled with a factor $3^{-i}$.

The code used to draw all grids is given below:
\begin{code}
\documentclass[tikz,preview,crop,11pt]{standalone}
\begin{document}
\begin{tikzpicture}
%
% settings 
%
\def\bbsize{9}
\def\h{\bbsize/27}
\def\o{1*\h}
\def\w{16*\h}
%
% white background; scales the plot to a certain size
%
\draw[draw=none,fill=white] (0,0) rectangle (\bbsize,\bbsize);
%
% draw domain
%
\draw[gray!80,fill=gray!80] (\o,\o) rectangle (\o+\w,\o+\w);
%
% draw mesh with different line thickness per level
%
\draw[help lines,thin,step=\bbsize/81] (0,0) grid (\bbsize,\bbsize);
\draw[black,step=\bbsize/27]            (0,0) grid (\bbsize,\bbsize);
\draw[black,thick,step=\bbsize/9]       (0,0) grid (\bbsize,\bbsize); 
\draw[ultra thick,black,step=\bbsize/3] (0,0) grid (\bbsize,\bbsize);
%
\end{tikzpicture}
\end{document}
\end{code}

\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.4\textwidth]{sketches/strong-scaling-grid-223.pdf}\hspace{0.1cm}
  \includegraphics[width=0.4\textwidth]{sketches/strong-scaling-grid-233.pdf}
\end{center} 
\caption{Left: A grid with $36^d$ cells on the coarse grid which allows to use $1$, $2^d$, $4^d$, and $12^d$ ranks
to uniformly distribute the load.
Right: A grid with $48^d$ cells on the coarse grid which allows to use $1$, $2^d$ and $16^d$ ranks
to uniformly distribute the load. A quasi-optimal distribution can be established with $4^d$ ranks.
The majority of cells lying outside of the domain will be erased.}
\label{fig:strong-scaling}
\end{figure}




\section{MPI troubleshooting and inefficiency patterns}
\label{section:mpi:inefficiency-patterns} 

For many of the smells below, it is quite useful to translate the code with
\linebreak \texttt{-DTrackGridStatistics}. The flag makes your code slightly
slower helps to identify many smells quickly.
Furthermore, please ensure that you run the unit tests and the ping pong test
before you continue. To run these tests, invoke \exahype\ without arguments and
read how you run the tests instead of the simulation.

%
% Smell
% How to find this smell
% What to do (short with references if possible)
%
\begin{description}
  \item [ I have increased my problem size and my code crashes now ] Often,
  scheduling systems return signal 9 but the reason might be different from
  case to case.
  Validate that you are not running out of
  memory! \exahype\ does not add, for performance reasons, checks around all
  memory allocations. It might be that you have spread your simulation over more
  cores or that you have to decrease your mesh resolution. 
  
  If you see, for a smaller-scale run, each rank require
  around $160 MB$ and if
  you run your code with dimensions $d\in\{2,3\}$ and if you have a rather
  regular (startup) grid and if you run $6$ MPI ranks per node, then a mesh of
  one additional refinement level (decrease your mesh to $1/3h$) already will
  require at least $6 \cdot 3^d \cdot 160 MB$ per node.
  
  Please note furthermore that \exahype\ first builds up the grid up to a
  certain (regular) level before it befills the grid with content, i.e.~once
  this initial grid is set up, you will require in one rush a massive amount of
  memory. 

  \item [My code crashes] Compile with \texttt{MODE=asserts} and rerun.
  Noone working with \exahype\ will be able to give you useful support without
  an assertion run. Please increase your timeouts significantly before you
  launch a simulation with assertions. Your code will be significantly slower.
  
  
  \item [ The ping pong test fails ] This is a known bug with MPI/MPI-2 and
  \exahype's user-defined data types. Please consult Chapter \ref{sec:bugs}.
  
  \item [ I have increased my problem size and now run into a time out ]
  Increase time out values. See Section
  \ref{section:mpi:buffer-timeout-settings}. Before you do so, please ensure:
  If the load balancing seems to be ok, add the load balancing flag \linebreak \texttt{virtually-expand-domain} to
  your configuration but carefully study the implications (Section
  \ref{section:mpi:grid-modifications}).

  \item [ I have an (almost) regular grid but the load balancing partitions
  unfavourable ]
  
  This smell \linebreak manifests typically in unbalanced logical topology trees
  if you run Peano's Python scripts.
  Please consult Section \ref{section:profiling:Peano-plain}.
  Consult the section on load balancing and try different variants.
  Usually, the \texttt{hotspot} strategy from Table
  \ref{table:mpi-static:analysis} is our method of choice. If your grid is
  pretty regular, \texttt{greedy-regular} might be the better choice. It is
  definitely faster throughout the grid construction.

  \item [ One of my nodes runs out of memory ]  \texttt{sfc-diffusion} yields
  close-to-perfect load distribution among the nodes (not ranks) which might fix
  your problem. Also ensure that you supplement your load balancing strategy
  with appropriate information on the number of ranks per node. Please consult
  the tables in the respective sections.

%   \item [ The load balancing seems to tackle the ranks that have too much
%   load but the domain splitting is not good ] 
%   It might be worth using the SFC-based load balancing from Table
%   \ref{table:node-pool-strategy}.

  \item [ I run out of memory on rank 0 or into a timeout because of rank 0 ]
  Add the load balancing flag \linebreak \texttt{virtually-expand-domain} to
  your configuration but carefully study the implications (Section
  \ref{section:mpi:grid-modifications}).
%   \item {\bf \exahype\ successively puts load on all nodes but as it fills
%   them one after another and as I have multiple ranks per node, my first few
%   nodes are heavily booked while the other ranks are assigned a smaller amount
%   of work}.
%   Switch the node pool strategy to \texttt{fair} (Table
%   \ref{table:node-pool-strategy}).
%   \item {\bf I do not have success at all to parallelize my application}.
%   You might want to start with a simpler PDE with less unknowns. The lowest
%   common denominator is an advection equation $\partial_t U + \partial_x U = 0$
%   with one unknown or the classical hydrodynamics as in section
%   \ref{sec:euler-starting} (5 unknowns). Also you should start with
%   the simplest scheme possible, i.e. start with a Finite Volume scheme (section
%   \ref{sec:FV-intro}) and try it to parallelize with MPI. If this works, use
%   instead the ADER-DG scheme and try again. Only if both schemes work on their own,
%   you are ready to try to paralallelize the coupled ADER-DG scheme with limiting
%   (section \ref{chapter:coupling-limiter}). Also, only change one thing at a
% time   (ie. do not change the PDE and the scheme at the same time) in order to determine
%   what scenario can be run with your setup and which not.
%   
  \item [ To be continued \ldots ]
\end{description}



\vspace{1cm}
\noindent
For further helpful hints to get MPI running, take a look into the developer FAQ
section in appendix \ref{sec:dev-faq}, the user FAQ section in appendix
\ref{sec:user-faq} and the known bugs and limitations in appendix \ref{sec:bugs}.
If you try to get MPI working on a well-known supercomputer, probably appendix
\ref{sec:apx-supercomputers} can give you an advice.


