#!/usr/bin/env python
# Python2
#
# This file generates the sketchy plots displaying ADER-DG polynomials
# in a cell and the meaning of the grid points. Run it interactively
# in an iPython shell and use savefig() to update the PDFs in figures/.
# Also play around with deterministic_points=True|False to switch
# between ...-cont.pdf and ...-discont.pdf
#
# PD 2016, Sven for ExaHyPE
#

from pylab import *
ion(); clf()

# helper functions
distribute = lambda Q: array([ Q[c*(nP-1) : (c+1)*(nP-1)+1] for c in cells])
heaviside = lambda x: 0.5 * (sign(x) + 1)

# settings:

domain = [0., 1.]
width = domain[1]-domain[0]

# number of cells and number of points in each cell

nCells = 4
nP = 3

# big flag whether to plot a continous solution or several random ones
deterministic_points = True

# derived basic stuff, Y values:
plotResolution = 20
cells, points = range(nCells), range(nP)
nGridpoints = nCells * (nP - 1) + 1
allXpoints = linspace(domain[0], domain[1], nGridpoints)
randomYpoints = random(nGridpoints)

# cellwise polynomial points
Xpoints = distribute(allXpoints)

if deterministic_points:
	# generate a continous solution 
	x = allXpoints # abbreviate
	allYpoints = exp(- x / (nGridpoints / 5)) * sin(x*2*pi/(1.*nGridpoints/11.))
	Ypoints = distribute(allYpoints)
else:
	# generate several independent random solutions
	offset, scale = random(2)
	Ypoints = array([(random(nP)-offset)/scale for c in cells])
	allYpoints = list(flatten([cp[:-1] for cp in Ypoints])) + [Ypoints[-1,-1]]

# cell points (cell edges)
cellX = allXpoints[::nP-1] # == linspace(domain[0], domain[1], nCells+1)
cellY = allYpoints[::nP-1]

highlight_cell_points = False
if highlight_cell_points:
	plot(cellX, cellY, "s", ms=15, color="green")

# reconstruction of exact data
polys = [poly1d(polyfit(x, y, nP-1)) for x,y in zip(Xpoints,Ypoints)]
Xinterp = [linspace(min(x), max(x), plotResolution) for x in Xpoints]
Yinterp = [ poly(Xi) for poly, Xi in zip(polys, Xinterp)]

# vertical connecting lines indicating a disconiuity
# Each X,Y in zip(Xconnect, Yconnect) is suitable for plot(X,Y).
connCells = cells[:-1]
Xconnect = [ [(c+1)/(nCells*width)]*2 for c in connCells ]
Yconnect = [ [ fun(Yinterp[c][-1], Yinterp[c+1][0]) for fun in [min,max] ] for c in connCells ]

# cell averages
bar(Xpoints[:,0], average(Ypoints, axis=1), width=width/nCells, \
	color='grey', linewidth=0)

for i in cells:
	plot(Xinterp[i], Yinterp[i], "-", color="blue")
	plot(Xpoints[i], Ypoints[i], "o", color="red", markersize=15)

# add vertical lines seperating cells, has to be at end for ylim infos
yminmax = ylim()
Xseper = Xconnect
Yseper = [list(yminmax)] * len(connCells)
for i in connCells:
	plot(Xseper[i], Yseper[i], "-", color="black")
ylim(yminmax) # ensure that ylim doesn't change

# show connecting cells
for i in connCells:
	plot(Xconnect[i], Yconnect[i], "-", color="red", linewidth=2.5)

# ensure 0 is visible
axhline(0, color='black')

# custom ticks
celllabels = ["Cell %d" % c  for c in cells]
xticks(cellX + cellX[1]/2, celllabels)

# ensure x domain
xlim(domain[0], domain[1])

# save the picture, if you like it, with
# savefig("../figures/bla.pdf")

# we also increased the font size in order to
# embed the figure quite small:
matplotlib.rcParams.update({'font.size': 22})




