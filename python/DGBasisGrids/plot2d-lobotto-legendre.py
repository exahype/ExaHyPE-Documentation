#!/usr/bin/python

from pylab import *
import matplotlib.patches as patches
from itertools import product
from collections import OrderedDict, namedtuple
from subprocess import call
ion()
clf()

fig = Figure(figsize=4*figaspect(1.0), facecolor="white", tight_layout=True)
# this is just for display, not for saving
fig.set_facecolor("white")

gausspoints=genfromtxt("../gausspoints.txt", names=True)
datacols = ['gaussLegendreNodes', 'gaussLobottoNodes']
orders = [ 2, 3, 4, 5, 7 ]

def plotGrid(order, datacol):
	ax = figure().add_subplot(1, 1, 1, adjustable="box", aspect=1.0)
	tight_layout()
	#ax.set_axis_bgcolor("gray")
	ax.set_aspect('equal', 'datalim')

	# label on the left

	ax = gca()

	points = gausspoints[ gausspoints['order'] == order ][datacol]

	X,Y = meshgrid(points,points)

	## this plot is interesting as it shows the weights when
	## interpolating. However, for the grid it is not that helpful
	#scatter(X,Y, gplweights * 2000)

	markersize = 120
	scatter(X,Y, markersize, clip_on=False, zorder=10, marker="o", color="black")
	xlim(0,1)
	ylim(0,1)

	ax.set(adjustable="datalim")
	ax.axis('off')

	# plot the actual grid lines between the points
	gridoptions = { 'color':"black",'lw': 2, "clip_on": False, "zorder": 8 }
	first, last = points[0], points[-1]
	for p in points:
		plot( [first, last], [p, p], **gridoptions) # rows
		plot( [p, p], [first, last], **gridoptions) # cols


	# shades of gray
	bgcolor = '0.6666'

	# Display the polynomial order as text in background
	numbercolor = '0.75'
	beauty_offset_y = -0.09
	text(0.5, 0.5 + beauty_offset_y, str(order),
		verticalalignment='center', horizontalalignment='center',
		transform=gca().transAxes, zorder=5,
		color=numbercolor,
		fontsize=200)

	# plot the background, ie. the patch size
	ax.add_patch(patches.Rectangle((0,0),1.0,1.0, edgecolor="none",
		zorder=0, facecolor=bgcolor))

	fname = "2d-p%d-%s-visualization.pdf" % (order, datacol)
	print "Plotting %s" % fname
	savefig(fname, bbox_inches = 'tight', pad_inches = 0)

	# try to get rid of the whitespace by calling pdfcrop.
	# if you don't have this installed and you are on Ubuntu, this might help:
	#  sudo apt-get install texlive-extra-utils
	call(["pdfcrop", fname, fname])


# for plotGrid(a,b), gives a function plotGridPrime(tupleOfAandB)
unpack = lambda fun: (lambda tpl: fun(*tpl))

# call all options
map(unpack(plotGrid), product(orders, datacols))

#row0 = setupax(1).twinx()
#for plotnum, (irow, icol) in enumerate(ndindex(nrows, ncols)):
#	cell = table[irow][icol]
#	setupax(plotnum+1)
#	plotgrid(cell)

