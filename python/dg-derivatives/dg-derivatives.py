#!/usr/bin/python
#
# A 1D gauss point function and derivative example.
# Written by Sven at 2017-04-03 for Garching ExaHyPE RSC presentation.
# May be used for different stuff, thought.
#

from pylab import *
import sympy
from sympy import var, lambdify
from aderdg.lagrangeInterpolation import * # aderdg package in engine/misc
ion()

gausspoints=genfromtxt("gausspoints.txt", names=True)
outdir=lambda fname: "output/"+fname

p = 5
mask = gausspoints['order'] == p
gwp = gausspoints[mask]['gaussLegendreNodes']

domain = linspace(0,1,100)

# style
lw = 2.5
ms = 18

def set_style(titlestr=""):
	fig, ax = gcf(), gca()
	fig.patch.set_facecolor('white')
	matplotlib.rcParams.update({'font.size': 25})

	# to avoid jumping of plots
	fig.set_size_inches( 8.15  ,  5.1875)
	ylim(-1.45, 1.6)

	def adjust_spines(ax, spines):
	    for loc, spine in ax.spines.items():
		if loc in spines:
		    spine.set_position(('outward', 10))  # outward by 10 points
		    spine.set_smart_bounds(True)
                    spine.set_linewidth(lw)
		else:
		    spine.set_color('none')  # don't draw spine

	    # turn off ticks where there is no spine
	    if 'left' in spines:
		ax.yaxis.set_ticks_position('left')
	    else:
		# no yaxis ticks
		ax.yaxis.set_ticks([])

	    if 'bottom' in spines:
		ax.xaxis.set_ticks_position('bottom')
	    else:
		# no xaxis ticks
		ax.xaxis.set_ticks([])

	adjust_spines(ax, ['left', 'bottom'])
	ax.get_yaxis().set_ticks([]) # hide y ticks
	ax.get_xaxis().set_ticks([0,1])
	ylabel("Field")#, rotation=0)
	xlabel("Single cell (patch)")
	if(titlestr): title(titlestr)
	fig.tight_layout() # actually brings texts back in

	# legendre grid lines
	[ax.axvline(x=xi, ls='--', color='lightgrey') for xi in gwp]

# test function
clf(); set_style("Interpolated initial data")
funccolor = "red"
#func = lambda x: sin(x**2 *8) /exp(x**2)*x**0.8
func = lambda x: sin(x*2*pi*2)
plot(domain, func(domain), color=funccolor, linewidth=lw)
plot(gwp, func(gwp), "o", color=funccolor, clip_on=False, markersize=ms)
savefig(outdir("01_initial.png"))

# projection onto DG polynomial
#clf();
set_style("Projection on DG polynomial")
projcolor = "forestgreen"
x = var("x")
projection = LagrangePoly(x, gwp, func(gwp))
numproj = lambdify(x, projection)
plot(domain, numproj(domain), color=projcolor, linewidth=lw)
plot(gwp, numproj(gwp), "o", color=projcolor, clip_on=False, markersize=ms)
savefig(outdir("02_projection.png"))

# derivative, faked
#clf();
set_style("DG derivative")
derivcolor = "blue"
deriv = sympy.diff(projection, x)
numderiv = lambdify(x, 0.04 * deriv)
plot(domain, numderiv(domain), color=derivcolor, linewidth=lw)
plot(gwp, numderiv(gwp), "o", color=derivcolor, clip_on=False, markersize=ms)
savefig(outdir("03_derivative.png"))

# evaluation points
#plot(gwp, func(gwp), "D", color=funccolor, markersize=15.0)

# plot faked gradient
#gradcolor = "red"
#grad = 2*pi * gradient(func(domain))
#plot(domain, grad, color=gradcolor, linewidth=1.5)
#
#gwpgrad = interp(gwp, domain, grad)
#plot(gwp, gwpgrad, "o", color=gradcolor, markersize=15.)



