/**
 * A standalone C++ program to extract the nodes and weights from
 * the ExaHyPE kernel structures. It dumps the values to stdout as
 * a CSV table.
 * The program is quite trivial and was justed just to quickly get
 * a table of nodes and weights instead of programming one from the
 * algebraic definitions.
 *
 * Written at 2017-03-31 by SvenK.
 *
 * In order to copmile, you should have the "kernels" directory
 * linked in the current folder. See extract-kernels.sh for a
 * compilation and usage example.
 **/

#include <stdio.h>

#include "kernels/GaussLobattoQuadrature.h"
#include "kernels/GaussLegendreQuadrature.h"

#include "kernels/DGMatrices.h"

int main() {

	const int omin=2, omax=8;

	// orders for the init...NodesAndWeights, it is not used anyway.
	std::set<int> orders;
	for(int o=omin; o<omax; o++)
		orders.insert(o);

	kernels::initGaussLobattoNodesAndWeights(orders);
	kernels::initGaussLegendreNodesAndWeights(orders);

	printf("%s\t%s\t%s\t%s\t%s\t%s\n", "order", "i", 
		"gaussLegendreNodes", "gaussLobottoNodes",
		"gaussLegendreWeights", "gaussLobottoWeights");

	for(int o=omin; o<omax; o++) {
	for(int i=0; i<=o; i++) {
		printf("%d\t%d\t%.10f\t%.10f\t%.10f\t%.10f\n", o, i,
			kernels::gaussLegendreNodes[o][i], kernels::gaussLobattoNodes[o][i],
			kernels::gaussLegendreWeights[o][i], kernels::gaussLobattoWeights[o][i]
		);
	}
	}
}
