#!/bin/sh
#
# This script compiles and runs the Extract-Kernel.cc program.
# See the c++ for further documentation.
# SvenK 2017.

verbose() { echo -e $@; $@; }

verbose g++ Extract-Kernel.cc kernels/GaussLobattoQuadrature.o kernels/GaussLegendreQuadrature.o

./a.out | tee ../gausspoints.txt

# softlinked files before
#for cpp in kernel-src/*.cpp;
#	do verbose g++ -c -std=c++0x $cpp
#done;
